ZCTEDIT
Created by Alan Gordon

Okay, this isn't a very polished program, because I have few intentions of releasing it as an official part of the game. It is a dev tool only.

Controls:
WASD.........Move camera
Ctrl+Mouse...Rotate camera
Arrow Keys...Change map size
~-0..........Change unit type being built
[G]..........Switch from placing units to placing generators*
F1-F7........Change map objective
[Q]/[E]......Rotate unit being placed left/right
Space........Add/remove walls
Tab..........Add/remove territory
[X]..........Add/remove destination squares (Seize/Escort)
Enter........Save map locally as "map.dat"
Escape.......Quit

*For example, to make a reaper generator, press [8] to select reapers, then [G] to place reaper generators.

When the program is loaded up, any local file named "map.dat" will be loaded to edit.