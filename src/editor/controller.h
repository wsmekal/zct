#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "include.h"
#include "tile.h"
#include "location.h"
#include "being.h"
#include "display.h"
#include "map.h"
#include "pathnode.h"
#include <vector>

class Being;
class Display;

class Controller {
public:
	int stringToInt(char *str);
	int loadNumberFromFile(ifstream &file);

	Controller(Display * = NULL);

	Map getMap();
	Being getBeing(int id);
	float getXCursor();
	float getYCursor();
	int getSelect();
	float getSelectRot();
	int getObjective();
	int getTime();

	void setMap(int x, int y, Tile n_map);
	void setWallDraw(bool n_wallDraw);
	void setTerritoryDraw(bool n_wallDraw);
	void setSelect(int n_select);
	void setSelectRot(float n_selectRot);
	void setObjective(int n_objective);
	void makeGenerators();

	void shiftRot(float amount);
	void changeXSize(int amount);
	void changeYSize(int amount);
	void changeTime(int amount);
	void generate();
	void moveCursor(int, int);		//Find Cursor position based on mouse position
	void spawnBeing(int x, int y, float rot, int type);
	void updateOccupation();
	void placeUnit();
	void placeTargetSquare();
	void killUnit();

	void save();
	void killMap();

private:
	Map map;
	Being *b;
	Display *display;

	GLfloat xCursor, yCursor;
	int select;			//Which being type is the current brush
	float selectRot;	//Direction of placing unit
	int wallDraw;		//Are we painting walls (-1 for no, 0 for floor, 1 for wall)
	int territoryDraw;	//Are we painting territory (-1 for no, 0 for enemy, 1 for player)

	int objective;		//Objective type
	int time;			//Time limit (If applicable
};

#endif
