//Point.h
//Definition for class Point.

#include "include.h"

#ifndef POINT_H
#define POINT_H

class Point {
	public:
		Point();							//Constructors
		Point(GLfloat, GLfloat);

		GLfloat getX();						//Accessors
		GLfloat getY();

		int getintX();						//integer Accessors
		int getintY();

		void setX(GLfloat);
		void setY(GLfloat);
		void setPoint(GLfloat, GLfloat);	//Mutators

		bool equals(Point);
		void pivot(Point, GLfloat);			//Rotate around point
		bool intersectPoint(Point, GLfloat);	//Is Point within [distance] of other Point?
	protected:
		GLfloat x,y;
};

#endif
