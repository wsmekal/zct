//Point.cpp
//Function declarations for class Point.

#include "include.h"
#include "point.h"

Point::Point() {
	x = 0.0f;
	y = 0.0f;
}

Point::Point(GLfloat n_x, GLfloat n_y) {
	x = n_x;
	y = n_y;
}

void Point::setX(GLfloat n_x) {
	x = n_x;
}

void Point::setY(GLfloat n_y) {
	y = n_y;
}

void Point::setPoint(GLfloat n_x, GLfloat n_y) {
	setX(n_x);
	setY(n_y);
}

GLfloat Point::getX() {
	return x;
}

GLfloat Point::getY() {
	return y;
}

int Point::getintX() {
	return static_cast<int>(x);
}

int Point::getintY() {
	return static_cast<int>(y);
}

bool Point::equals(Point pt) {
	if(pt.getX() == x && pt.getY() == y)
		return true;
	else
		return false;
}

void Point::pivot(Point pt, GLfloat degrees) {
	if(!equals(pt) && degrees != 0.0f) {				//If our points aren't the same
		GLfloat xNew, yNew;
		xNew = x - pt.getX();
		yNew = y - pt.getY();

		double rot = -atan(GLfloat(yNew) / GLfloat(xNew));	//Get the Angle & distance
		double dist = sqrt(pow(double(xNew),2) + 
							pow(double(yNew),2));
		rot -= PI / 2.0f;
		if(x < 0.0f) rot += PI;
		if(rot < 0.0) rot += PI * 2.0f;

		rot += degrees;

		xNew = -GLfloat(sin(rot) * dist);			//Get the new relative coords...
		yNew = -GLfloat(cos(rot) * dist);
		x = xNew + pt.getX();						//...and make them absolute
		y = yNew + pt.getY();
	}
}

bool Point::intersectPoint(Point pt, GLfloat r) {
	double dist = sqrt(pow(double(x - pt.getX()),2) + pow(double(y - pt.getY()),2));
	if(GLfloat(dist) > r)
		return false;
	else
		return true;
}
