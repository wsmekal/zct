#include "include.h"
#include "controller.h"


int Controller::stringToInt(char *str) {
	int result = 0;
	int negative = 0;
	if (str[0] == '-') {
		negative = 1;
		++str;
	} else if(str[1]=='-') {
		negative = 1;
		str += 2;
	}
	for (size_t i = 0; i < strlen(str); ++i) {
		if(str[i] != '\n' && str[i] != '.') {
			result *= 10;
			result += int(str[i] - 48);
		}
	}
	if (negative) {
		result *= -1;
		--str;
	}
	return result;
}

int Controller::loadNumberFromFile(ifstream &file) {
	char buffer[32];
	file.getline(buffer, 32);
	return stringToInt(buffer);
}

Controller::Controller(Display *n_display) {
	display = n_display;
	b = new Being[MAX_BEING];
	map = Map();

	xCursor = 0.0f;
	yCursor = 0.0f;
	select = -1;
	selectRot = 0;
	wallDraw = -1;
	territoryDraw = -1;
	objective = ROUT;
	time = 1;
}

Map Controller::getMap() {
	return map;
}

Being Controller::getBeing(int id) {
	return b[id];
}

float Controller::getXCursor() {
	return xCursor;
}

float Controller::getYCursor() {
	return yCursor;
}

int Controller::getSelect() {
	return select;
}

float Controller::getSelectRot() {
	return selectRot;
}

int Controller::getObjective() {
	return objective;
}

int Controller::getTime() {
	return time;
}


void Controller::setMap(int x, int y, Tile n_map) {
	map.setTile(x, y, n_map.getTerrain());
}

void Controller::setWallDraw(bool n_wallDraw) {
	if(!n_wallDraw) {
		wallDraw = -1;
	} else {
		Tile t = map.getTile(int(xCursor+0.5), int(yCursor+0.5));
		if(t.getTerrain() == FLOOR) {
			t.setTerrain(WALL);
			wallDraw = WALL;
		} else {
			t.setTerrain(FLOOR);
			wallDraw = FLOOR;
		}
		map.setTile(int(xCursor+0.5), int(yCursor+0.5), t);
	}
}

void Controller::setTerritoryDraw(bool n_territoryDraw) {
	if(!n_territoryDraw) {
		territoryDraw = -1;
	} else {
		Tile t = map.getTile(int(xCursor+0.5), int(yCursor+0.5));
		if(t.getPlayer() == ENEMY) {
			t.setPlayer(PLAYER1);
			territoryDraw = PLAYER1;
		} else if(t.getPlayer() == PLAYER1) {
			t.setPlayer(ENEMY);
			territoryDraw = ENEMY;
		}
		map.setTile(int(xCursor+0.5), int(yCursor+0.5), t);
	}
}

void Controller::setSelect(int n_select) {
	if(select == n_select) {
		select = -1;
	} else {
		select = n_select;
		selectRot = 0;
	}
}

void Controller::setSelectRot(float n_selectRot) {
	selectRot = n_selectRot;
}

void Controller::setObjective(int n_objective) {
	objective = n_objective;
}

void Controller::makeGenerators() {
	if((select >= ZOMBIE && select != ZOMBIELORD) || select < -1) {
		select *= -1;
	}
}


void Controller::shiftRot(float amount) {
	selectRot += amount;
}

void Controller::changeXSize(int amount) {
	map.setMaxX(max(min(map.getMaxX() + amount, MAX_SIZE), 3));
}

void Controller::changeYSize(int amount) {
	map.setMaxY(max(min(map.getMaxY() + amount, MAX_SIZE), 3));
}

void Controller::changeTime(int amount) {
	time += amount;
	if(time < 1) time = 1;
}

void Controller::generate() {
	//Try to open map.dat
	ifstream file;
	char buffer;
	file.open("map.dat");
	if(file) {
		//Load the terrain data
		map.setMaxX(loadNumberFromFile(file));
		map.setMaxY(loadNumberFromFile(file));
		objective = loadNumberFromFile(file);
		if(objective >= TIMED_ROUT) {
			time = loadNumberFromFile(file);
		}
		for(int i = 0; i < map.getMaxX(); ++i) {
			for(int d = 0; d < map.getMaxY(); ++d) {
				file.get(buffer);
				if(buffer == '0') {
					map.setTile(i,d,Tile(FLOOR));
				} else if(buffer == '1') {
					map.setTile(i,d,Tile(WALL));
				} else if(buffer == '2') {
					map.setTile(i,d,Tile(GENERATOR));
				} else if(buffer == '3') {
					map.setTile(i,d,Tile(GENERATOR_SMART));
				} else if(buffer == '4') {
					map.setTile(i,d,Tile(GENERATOR_FAST));
				} else if(buffer == '5') {
					map.setTile(i,d,Tile(GENERATOR_BIG));
				} else if(buffer == '6') {
					map.setTile(i,d,Tile(GENERATOR_REAPER));
				} else if(buffer == '7') {
					map.setTile(i,d,Tile(GENERATOR_STEALTH));
				}
			}
			file.get(buffer);
		}
		Tile t;
		for(int i = 0; i < map.getMaxX(); ++i) {
			for(int d = 0; d < map.getMaxY(); ++d) {
				file.get(buffer);
				if(buffer == '0') {
					t = map.getTile(i,d);
					t.setPlayer(ENEMY);
					map.setTile(i,d,t);
				} else if(buffer == '1') {
					t = map.getTile(i,d);
					t.setPlayer(PLAYER1);
					map.setTile(i,d,t);
				} else if(buffer == '2') {
					t = map.getTile(i,d);
					t.setPlayer(PLAYER2);
					map.setTile(i,d,t);
				}
			}
			file.get(buffer);
		}
		if(objective == SEIZE || objective == ESCORT || objective == TIMED_SEIZE || objective == TIMED_ESCORT) {
			for(int i = 0; i < map.getMaxX(); ++i) {
				for(int d = 0; d < map.getMaxY(); ++d) {
					file.get(buffer);
					if(buffer == '0') {
						t = map.getTile(i,d);
						t.setTarget(false);
						map.setTile(i,d,t);
					} else if(buffer == '1') {
						t = map.getTile(i,d);
						t.setTarget(true);
						map.setTile(i,d,t);
					}
				}
				file.get(buffer);
			}
		}
		//Load the being data
		int numBeings = min(loadNumberFromFile(file), MAX_BEING);
		Location temp = Location();
		b = new Being[MAX_BEING];
		for(int i = 0; i < MAX_BEING; ++i) {
			b[i] = Being(this);
			if(i < numBeings) {
				b[i].setAlive(true);
				temp.setX(float(loadNumberFromFile(file)));
				temp.setY(float(loadNumberFromFile(file)));
				temp.setRot(float(loadNumberFromFile(file)) * 45);
				b[i].setPos(temp);
				b[i].setSeenPos(temp);
				b[i].setType(loadNumberFromFile(file));
				t = map.getTile(int(b[i].getPos().getX()), int(b[i].getPos().getY()));
				t.setOccupied(b[i].getType());
				map.setTile(int(b[i].getPos().getX()), int(b[i].getPos().getY()), t);
			}
		}
	} else {
		//If it's not there, create a blank map
		map = Map();

		map.setMaxX(24);
		map.setMaxY(24);
		for(int i = 0; i < map.getMaxX(); ++i) {
			for(int d = 0; d < map.getMaxY(); ++d) {
				map.setTile(i,d,Tile(FLOOR));
			}
		}
		for(int i = 0; i < MAX_BEING; ++i) {
			b[i] = Being(this);
		}
	}
}

void Controller::moveCursor(int x, int y) {
	float oldX = xCursor;
	float oldY = yCursor;
	xCursor = display->getXCamera() + ( (x - 320) * cos(display->getRotate() * PI / 180) - (y-290) * sin(display->getRotate() * PI / 180)) / 18.0f;
	yCursor = display->getYCamera() + (-(x - 320) * sin(display->getRotate() * PI / 180) - (y-290) * cos(display->getRotate() * PI / 180)) / 18.0f;
	if(wallDraw > -1 && (int(oldX + 0.5f) != int(xCursor + 0.5f) || int(oldY + 0.5f) != int(yCursor + 0.5f))) {
		Tile t = map.getTile(int(xCursor+0.5), int(yCursor+0.5));
		t.setTerrain(wallDraw);
		map.setTile(int(xCursor+0.5), int(yCursor+0.5), t);
	}
	if(territoryDraw > -1 && (int(oldX + 0.5f) != int(xCursor + 0.5f) || int(oldY + 0.5f) != int(yCursor + 0.5f))) {
		Tile t = map.getTile(int(xCursor+0.5), int(yCursor+0.5));
		t.setPlayer(territoryDraw);
		map.setTile(int(xCursor+0.5), int(yCursor+0.5), t);
	}
}

void Controller::updateOccupation() {
	Tile t;
	for(int i = 0; i < map.getMaxX(); ++i) {
		for(int d = 0; d < map.getMaxY(); ++d) {
			t = map.getTile(i, d);
			t.setOccupied(-1);
			map.setTile(i, d, t);
		}
	}

	for(int i = 0; i < MAX_BEING; ++i) {
		if(b[i].getAlive()) {
			t = map.getTile(int(b[i].getPos().getX()), int(b[i].getPos().getY()));
			t.setOccupied(b[i].getType());
			map.setTile(int(b[i].getPos().getX()), int(b[i].getPos().getY()), t);
		}
	}
}

void Controller::placeUnit() {
	if(select > -1 && map.getTile(int(xCursor+0.5), int(yCursor+0.5)).getOccupied() == -1 &&
		!map.getTile(int(xCursor+0.5), int(yCursor+0.5)).getWall()) {
		//Create a new being at this space
		for(int i = 0; i < MAX_BEING; ++i) {
			if(!b[i].getAlive()) {
				//We use this being
				b[i].setAlive(true);
				b[i].setPos(Location(float(int(xCursor+0.5)), float(int(yCursor+0.5)), selectRot+180));
				b[i].setSeenPos(b[i].getPos());
				b[i].setType(select);
				b[i].setTurns(0);
				break;
			}
		}
	} else if(select < -1 && map.getTile(int(xCursor+0.5), int(yCursor+0.5)).getOccupied() == -1) {
		Tile t = map.getTile(int(xCursor+0.5), int(yCursor+0.5));
		switch(select) {
		case -ZOMBIE:
			t.setTerrain(GENERATOR);
			break;
		case -SMARTZOMBIE:
			t.setTerrain(GENERATOR_SMART);
			break;
		case -FASTZOMBIE:
			t.setTerrain(GENERATOR_FAST);
			break;
		case -BIGZOMBIE:
			t.setTerrain(GENERATOR_BIG);
			break;
		case -REAPER:
			t.setTerrain(GENERATOR_REAPER);
			break;
		case -STEALTH:
			t.setTerrain(GENERATOR_STEALTH);
			break;
		}
		map.setTile(int(xCursor+0.5), int(yCursor+0.5), t);
	}
}

void Controller::placeTargetSquare() {
	Tile t;
	t = map.getTile(int(xCursor+0.5), int(yCursor+0.5));
	t.setTarget(!t.getTarget());
	map.setTile(int(xCursor+0.5), int(yCursor+0.5), t);
}

void Controller::killUnit() {
	for(int i = 0; i < MAX_BEING; ++i) {
		if(b[i].getPos().getX() == float(int(xCursor+0.5)) && b[i].getPos().getY() == float(int(yCursor+0.5))) {
			b[i].setAlive(false);
		}
	}
	updateOccupation();
}

void Controller::spawnBeing(int x, int y, float rot, int type) {
	for(int i = 0; i < MAX_BEING; ++i) {
		if(!b[i].getAlive()) {
			b[i].setAlive(true);
			b[i].setPos(Location(float(x), float(y), rand() % 8 * 45));
			b[i].setSeenPos(b[i].getPos());
			b[i].setType(type);
			b[i].setTurns(1);
			break;
		}
	}
	updateOccupation();
}

void Controller::save() {
	ofstream file;
	file.open("map.dat");

	file << map.getMaxX() << endl;
	file << map.getMaxY() << endl;
	file << objective << endl;
	if(objective >= TIMED_ROUT)
		file << time << endl;
	for(int i = 0; i < map.getMaxX(); ++i) {
		for(int d = 0; d < map.getMaxY(); ++d) {
			switch(map.getTile(i,d).getTerrain()) {
			case FLOOR:
				file << '0';
				break;
			case WALL:
				file << '1';
				break;
			case GENERATOR:
				file << '2';
				break;
			case GENERATOR_SMART:
				file << '3';
				break;
			case GENERATOR_FAST:
				file << '4';
				break;
			case GENERATOR_BIG:
				file << '5';
				break;
			case GENERATOR_REAPER:
				file << '6';
				break;
			case GENERATOR_STEALTH:
				file << '7';
				break;
			}
		}
		file << endl;
	}
	for( int i = 0; i < map.getMaxX(); ++i) {
		for(int d = 0; d < map.getMaxY(); ++d) {
			switch(map.getTile(i,d).getPlayer()) {
			case PLAYER1:
				file << '1';
				break;
			case ENEMY:
				file << '0';
				break;
			}
		}
		file << endl;
	}
	if(objective == SEIZE || objective == ESCORT || objective == TIMED_SEIZE || objective == TIMED_ESCORT) {
		for( int i = 0; i < map.getMaxX(); ++i) {
			for(int d = 0; d < map.getMaxY(); ++d) {
				switch(map.getTile(i,d).getTarget()) {
				case PLAYER1:
					file << '1';
					break;
				case ENEMY:
					file << '0';
					break;
				}
			}
			file << endl;
		}
	}
	file << endl;

	//Count beings
	int numBeings = 0;
	for(int i = 0; i < MAX_BEING; ++i) {
		if(b[i].getAlive()) {
			++numBeings;
		}
	}
	file << numBeings << endl;
	for(int i = 0; i < MAX_BEING; ++i) {
		if(b[i].getAlive()) {
			file << int(b[i].getPos().getX()) << endl;
			file << int(b[i].getPos().getY()) << endl;
			file << int(b[i].getPos().getRot() / 45) << endl;
			file << b[i].getType() << endl;
		}
	}
}

void Controller::killMap() {
	delete b;
}
