#ifndef LOCATION_H
#define LOCATION_H

#include "point.h"

class Location : public Point {
	public:
		Location();				//Constructors
		Location(Point);
		Location(GLfloat, GLfloat, GLfloat);

		GLfloat getRot();		//Accessors

		void setRot(GLfloat);	//Mutators

		void pivot(Point, GLfloat, bool);		//Rotate around point
		void facePoint(Point);					//Rot faces point
	private:
		GLfloat rot;
};

#endif
