//Map class - used to store the data on the different maps loaded before one is chosen to play on.

#ifndef MAP_H
#define MAP_H

#include "include.h"
#include "tile.h"

class Map {
public:
	int stringToInt(char *str);
	int loadNumberFromFile(ifstream &file);

	Map();
//	~Map();

	Tile getTile(int x, int y);
	int getMaxX();
	int getMaxY();

	void setTile(int x, int y, Tile n_tile);
	void setMaxY(int n_maxY);
	void setMaxX(int n_maxX);

	int moveDistance(int x1, int y1, int x2, int y2);
	bool legalMove(int x1, int y1, int x2, int y2, int player);

	void clearHighlights();
	void highlightMoves(int x, int y, int turns);
private:
	Tile *t;
	int maxX, maxY;
};

#endif
