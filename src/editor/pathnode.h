#ifndef PATHNODE_H
#define PATHNODE_H

#include "point.h"

class PathNode : public Point {
public:
	PathNode();
	PathNode(Point pt);
	PathNode(float x2, float y2, PathNode *n_parent);

	PathNode *getParent();
	float getG();
	
	void setParent(PathNode *n_parent);
	void setG(float n_g);
	
	float getF(float x2, float y2);
private:
	PathNode *parent;
	float g;
};

#endif
