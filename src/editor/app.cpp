
/**************************************
*                                     *
*   Jeff Molofee's Basecode Example   *
*   SDL porting by Fabio Franchello   *
*          nehe.gamedev.net           *
*                2001                 *
*                                     *
**************************************/

// Includes
#ifdef WIN32													// If We're Under MSVC
	#include <windows.h>											// Include The Windows Header
#else															// Otherwise
	#include <cstdio>												// Include The Standar IO Header
	#include <cstdlib>												// And The Standard Lib (for exit())
#endif															// Then...

#include <cmath>												// We Require The Math Lib For Sin and Cos
#include <GL/gl.h>												// And Obviously The OpenGL Header
#include <GL/glu.h>												// And The GLu Heander

#include <SDL.h>												// Finally: The SDL Header!
#include <assert.h>

#include "main.h"												// We're Including theHeader Where Defs And Prototypes Are

extern S_AppStatus AppStatus;									// We're Using This Struct As A Repository For The Application State (Visible, Focus, ecc)

// User Defined Variables
bool keyPressed[256];
bool clicked;
float oldMousex;

//Game-related variables
Controller control;
Display display;
Map map;

// Code

bool InitGL(SDL_Surface *S)										// Any OpenGL Initialization Code Goes Here
{
	return display.InitGL(S);
}

bool Initialize(void)											// Any Application & User Initialization Code Goes Here
{
	AppStatus.Visible		= true;								// At The Beginning, Our App Is Visible
	AppStatus.MouseFocus	= true;								// And Have Both Mouse
	AppStatus.KeyboardFocus = true;								// And Input Focus
	SDL_ShowCursor( false );								// cursor isn't shown

	// Start Of User Initialization
	control = Controller(&display);
	control.generate();

	return true;												// Return TRUE (Initialization Successful)
}


void Deinitialize(void)											// Any User Deinitialization Goes Here
{
	return;														// We Have Nothing To Deinit Now
}

char getLetter(Uint8 *keys) {
	for(int i = int('a'); i <= int('z'); ++i) {
		if(keys) {
			if(keys[i]) {
				if(!keyPressed[i]) {
					keyPressed[i] = true;
					return char(i);
				}
			} else {
				keyPressed[i] = false;
			}
		}
	}
	return ' ';
}


void Update(Uint32 Milliseconds, Uint8 *Keys, int mousex, int mousey, bool lclick, bool rclick, bool rdown, bool ldown)	// Perform Motion Updates Here
{
	if(Keys)													// If We're Sent A Key Event With The Update
	{
		if(Keys['w']) {
			display.moveCamera( CAMERA_SPEED, CAMERA_SPEED);
		}
		if(Keys['s']) {
			display.moveCamera(-CAMERA_SPEED,-CAMERA_SPEED);
		}
		if(Keys['a']) {
			display.moveCamera(-CAMERA_SPEED, CAMERA_SPEED);
		}
		if(Keys['d']) {
			display.moveCamera( CAMERA_SPEED,-CAMERA_SPEED);
		}
		if(Keys[SDLK_ESCAPE]) {
			TerminateApplication();
		}
		if(Keys['1']) {
			if(!keyPressed[int('1')]) {
				keyPressed[int('1')] = true;
				control.setSelect(1);
			}
		} else {
				keyPressed[int('1')] = false;
		}
		if(Keys['2']) {
			if(!keyPressed[int('2')]) {
				keyPressed[int('2')] = true;
				control.setSelect(2);
			}
		} else {
				keyPressed[int('2')] = false;
		}
		if(Keys['3']) {
			if(!keyPressed[int('3')]) {
				keyPressed[int('3')] = true;
				control.setSelect(3);
			}
		} else {
				keyPressed[int('3')] = false;
		}
		if(Keys['4']) {
			if(!keyPressed[int('4')]) {
				keyPressed[int('4')] = true;
				control.setSelect(4);
			}
		} else {
				keyPressed[int('4')] = false;
		}
		if(Keys['5']) {
			if(!keyPressed[int('5')]) {
				keyPressed[int('5')] = true;
				control.setSelect(5);
			}
		} else {
				keyPressed[int('5')] = false;
		}
		if(Keys['6']) {
			if(!keyPressed[int('6')]) {
				keyPressed[int('6')] = true;
				control.setSelect(6);
			}
		} else {
				keyPressed[int('6')] = false;
		}
		if(Keys['7']) {
			if(!keyPressed[int('7')]) {
				keyPressed[int('7')] = true;
				control.setSelect(7);
			}
		} else {
				keyPressed[int('7')] = false;
		}
		if(Keys['8']) {
			if(!keyPressed[int('8')]) {
				keyPressed[int('8')] = true;
				control.setSelect(8);
			}
		} else {
				keyPressed[int('8')] = false;
		}
		if(Keys['9']) {
			if(!keyPressed[int('9')]) {
				keyPressed[int('9')] = true;
				control.setSelect(9);
			}
		} else {
				keyPressed[int('9')] = false;
		}
		if(Keys['0']) {
			if(!keyPressed[int('0')]) {
				keyPressed[int('0')] = true;
				control.setSelect(10);
			}
		} else {
				keyPressed[int('0')] = false;
		}
		if(Keys['`']) {
			if(!keyPressed[int('`')]) {
				keyPressed[int('`')] = true;
				control.setSelect(0);
			}
		} else {
				keyPressed[int('`')] = false;
		}
		if(Keys['g']) {
			if(!keyPressed[int('g')]) {
				keyPressed[int('g')] = true;
				control.makeGenerators();
			}
		} else {
				keyPressed[int('g')] = false;
		}
		if(Keys['q']) {
			if(!keyPressed[int('q')]) {
				keyPressed[int('q')] = true;
				control.shiftRot( 45);
			}
		} else {
				keyPressed[int('q')] = false;
		}
		if(Keys['e']) {
			if(!keyPressed[int('e')]) {
				keyPressed[int('e')] = true;
				control.shiftRot(-45);
			}
		} else {
				keyPressed[int('e')] = false;
		}
		if(Keys[SDLK_UP]) {
			if(!keyPressed[SDLK_UP]) {
				keyPressed[SDLK_UP] = true;
				control.changeYSize( 1);
			}
		} else {
				keyPressed[SDLK_UP] = false;
		}
		if(Keys[SDLK_DOWN]) {
			if(!keyPressed[SDLK_DOWN]) {
				keyPressed[SDLK_DOWN] = true;
				control.changeYSize(-1);
			}
		} else {
				keyPressed[SDLK_DOWN] = false;
		}
		if(Keys[SDLK_LEFT]) {
			if(!keyPressed[SDLK_LEFT]) {
				keyPressed[SDLK_LEFT] = true;
				control.changeXSize(-1);
			}
		} else {
				keyPressed[SDLK_LEFT] = false;
		}
		if(Keys[SDLK_RIGHT]) {
			if(!keyPressed[SDLK_RIGHT]) {
				keyPressed[SDLK_RIGHT] = true;
				control.changeXSize( 1);
			}
		} else {
				keyPressed[SDLK_RIGHT] = false;
		}
		if(Keys[' ']) {
			if(!keyPressed[int(' ')]) {
				keyPressed[int(' ')] = true;
				control.setWallDraw(true);
			}
		} else {
			keyPressed[int(' ')] = false;
			control.setWallDraw(false);
		}
		if(Keys[SDLK_TAB]) {
			if(!keyPressed[SDLK_TAB]) {
				keyPressed[SDLK_TAB] = true;
				control.setTerritoryDraw(true);
			}
		} else {
			keyPressed[SDLK_TAB] = false;
			control.setTerritoryDraw(false);
		}
		if(Keys['x']) {
			if(!keyPressed[int('x')]) {
				keyPressed[int('x')] = true;
				control.placeTargetSquare();
			}
		} else {
			keyPressed[int('x')] = false;
		}
		if(Keys[SDLK_F1]) {
			if(!keyPressed[SDLK_F1]) {
				keyPressed[SDLK_F1] = true;
				control.setObjective(0);
			}
		} else {
				keyPressed[SDLK_F1] = false;
		}
		if(Keys[SDLK_F2]) {
			if(!keyPressed[SDLK_F2]) {
				keyPressed[SDLK_F2] = true;
				control.setObjective(1);
			}
		} else {
				keyPressed[SDLK_F2] = false;
		}
		if(Keys[SDLK_F3]) {
			if(!keyPressed[SDLK_F3]) {
				keyPressed[SDLK_F3] = true;
				control.setObjective(2);
			}
		} else {
				keyPressed[SDLK_F3] = false;
		}
		if(Keys[SDLK_F4]) {
			if(!keyPressed[SDLK_F4]) {
				keyPressed[SDLK_F4] = true;
				control.setObjective(3);
			}
		} else {
				keyPressed[SDLK_F4] = false;
		}
		if(Keys[SDLK_F5]) {
			if(!keyPressed[SDLK_F5]) {
				keyPressed[SDLK_F5] = true;
				control.setObjective(4);
			}
		} else {
				keyPressed[SDLK_F5] = false;
		}
		if(Keys[SDLK_F6]) {
			if(!keyPressed[SDLK_F6]) {
				keyPressed[SDLK_F6] = true;
				control.setObjective(5);
			}
		} else {
				keyPressed[SDLK_F6] = false;
		}
		if(Keys[SDLK_F7]) {
			if(!keyPressed[SDLK_F7]) {
				keyPressed[SDLK_F7] = true;
				control.setObjective(6);
			}
		} else {
				keyPressed[SDLK_F7] = false;
		}
		if(Keys['[']) {
			if(!keyPressed[int('[')]) {
				keyPressed[int('[')] = true;
				control.changeTime(-1);
			}
		} else {
				keyPressed[int('[')] = false;
		}
		if(Keys[']']) {
			if(!keyPressed[int(']')]) {
				keyPressed[int(']')] = true;
				control.changeTime(1);
			}
		} else {
				keyPressed[int(']')] = false;
		}
		if(Keys[SDLK_RETURN]) {
			if(!keyPressed[SDLK_RETURN]) {
				keyPressed[SDLK_RETURN] = true;
				control.save();
			}
		} else {
				keyPressed[SDLK_RETURN] = false;
		}
	}

	if((Keys && Keys[SDLK_LCTRL])) {
		display.pivotCamera(mousex - oldMousex);
	}

	if(lclick) {
		control.placeUnit();
	}
	if(rclick) {
		control.killUnit();
	}
	control.moveCursor(mousex, mousey);

	oldMousex = mousex;
	return;
}

void Draw( SDL_Surface *Screen )									// Our Drawing Code
{
	assert(Screen);												// We won't go on if the Screen pointer is invalid

	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );		// Clear Screen And Depth Buffer
	Draw3D( Screen );												// We split our drawing code into two pieces:
	Draw2D( Screen );												// First draw the 3d stuff, then blit 2d surfaces onto it
	glFlush();													// Flush The GL Rendering Pipelines

	return;
}

void Draw3D( SDL_Surface *S )										// OpenGL drawing code here
{
	display.draw( control );
	return;
}

void Draw2D( SDL_Surface *S )										// SDL drawing code here
{
	static SDL_Rect src1={ 0, 0, 0, 0 };

	SDL_FillRect(S, &src1, SDL_MapRGBA(S->format,0,0,0,0));

	glColor3f(1,1,1);
	glEnable(GL_BLEND);
	glDisable(GL_LIGHTING);
	glEnable(GL_TEXTURE_2D);

	glDisable(GL_TEXTURE_2D);
	glEnable(GL_LIGHTING);
	glDisable(GL_BLEND);

	return;														// We're Always Making Functions Return
}
