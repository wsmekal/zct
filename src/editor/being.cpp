#include "being.h"

Being::Being(Controller *n_control) {
	control = n_control;
	type = ZOMBIE;
	color[0] = 0;
	color[1] = 1;
	color[2] = 0;
	health = 1;
	turns = 1;
	pos = Location(0,0,0);
	seenPos = pos;
	alive = false;
}


Location Being::getPos() {
	return pos;
}

Location Being::getSeenPos() {
	return seenPos;
}

float Being::getColor(int id) {
	return color[id];
}

int Being::getTurns() {
	return turns;
}

int Being::getMaxTurns() {
	switch(type) {
	case VIP:
	case DEFENDER:
	case SOLDIER:
	case ZOMBIE:
	case SMARTZOMBIE:
	case BIGZOMBIE:
		return 1;
		break;
	case SLAYER:
	case FASTZOMBIE:
	case REAPER:
		return 2;
		break;
	case ZOMBIELORD:
		return 3;
		break;
	}
	return 0;
}

int Being::getType() {
	return type;
}

bool Being::getAlive() {
	return alive;
}

int Being::getValue() {
	switch(type) {
	case DEFENDER:
		return 1;
		break;
	case SOLDIER:
		return 2;
		break;
	case SLAYER:
		return 3;
		break;
	default:
		return -1;
		break;
	}
}


void Being::setPos(Location n_pos) {
	pos = n_pos;
}

void Being::setSeenPos(Location n_seenPos) {
	seenPos = n_seenPos;
}

void Being::setTurns(int n_turns) {
	turns = n_turns;
}

void Being::setType(int n_type) {
	type = n_type;
	switch(type) {
	case VIP:
		color[0] = 1;
		color[1] = 0;
		color[2] = 0;
		break;
	case DEFENDER:
		color[0] = 0;
		color[1] = 0;
		color[2] = 1;
		break;
	case SOLDIER:
		color[0] = 0.3f;
		color[1] = 0.3f;
		color[2] = 0.7f;
		break;
	case SLAYER:
		color[0] = 0;
		color[1] = 0;
		color[2] = 0;
		break;
	case ZOMBIE:
		color[0] = 0;
		color[1] = 0.7f;
		color[2] = 0;
		break;
	case SMARTZOMBIE:
		color[0] = 0;
		color[1] = 1;
		color[2] = 0;
		break;
	case FASTZOMBIE:
		color[0] = 0.5f;
		color[1] = 1;
		color[2] = 0.5f;
		break;
	case BIGZOMBIE:
		color[0] = 0;
		color[1] = 0.5f;
		color[2] = 0;
		break;
	case REAPER:
		color[0] = 0.7f;
		color[1] = 0;
		color[2] = 0.7f;
		break;
	case STEALTH:
		color[0] = 0.3f;
		color[1] = 0;
		color[2] = 0.3f;
		break;
	case ZOMBIELORD:
		color[0] = 1;
		color[1] = 0.8f;
		color[2] = 0;
		break;
	}
}

void Being::setAlive(bool n_alive) {
	alive = n_alive;
}

void Being::adjustPos() {
	seenPos.setX(seenPos.getX() + (pos.getX() - seenPos.getX()) / SLIDE_DELAY);
	seenPos.setY(seenPos.getY() + (pos.getY() - seenPos.getY()) / SLIDE_DELAY);
	seenPos.setRot(pos.getRot());
}
