#ifndef BEING_H
#define BEING_H

#include "include.h"
#include "location.h"
#include "controller.h"

class Controller;

class Being {
public:
	Being(Controller *n_control = NULL);

	Location getPos();
	Location getSeenPos();
	int getType();
	int getHealth();
	int getTurns();
	int getMaxHealth();
	int getMaxTurns();
	float getColor(int id);
	bool getAlive();
	int getValue();

	void setPos(Location n_pos);
	void setSeenPos(Location n_seenPos);
	void setType(int n_type);
	void setTurns(int n_turns);
	void setAlive(bool n_alive);

	void adjustPos();
private:
	Location pos;		//The game-mechanics-wise position of the piece
	Location seenPos;	//The displayed position of the piece
	int type;

	int health;
	int turns;			//How many spaces this piece can still move
	bool alive;			//Is this being alive?

	float color[3];

	Controller *control;	//Controller associated with this piece
};

#endif
