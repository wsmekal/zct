#include "location.h"

Location::Location() {
	Point::Point();
	rot = 0.0f;
}

Location::Location(Point pt) {
	x = pt.getX();
	y = pt.getY();
	rot = 0.0f;
}

Location::Location(GLfloat n_x, GLfloat n_y, GLfloat n_rot) {
	x = n_x;
	y = n_y;
	rot = n_rot;
}

GLfloat Location::getRot() {
	return rot;
}

void Location::setRot(GLfloat n_rot) {
	rot = n_rot;
}

void Location::pivot(Point pt, GLfloat degrees, bool adjustRot) {
	Point::pivot(pt, degrees);
	if(adjustRot)
		rot += degrees;
}

void Location::facePoint(Point pt) {
	if(!(equals(pt))) {
		GLfloat xNew, yNew;
		xNew = x - pt.getX();
		yNew = -(y - pt.getY());

		double n_rot = -atan(GLfloat(yNew) / GLfloat(xNew));	//Get the Angle & distance
		n_rot -= PI / 2.0f;
		if(xNew < 0.0f) n_rot += PI;
		if(n_rot < 0.0) n_rot += PI * 2.0f;

		rot = GLfloat(n_rot);
		rot *= 180.0f / PI;
	}
}
