/*
include.h
Contains the #include commands for all the necessary external files,
along with constants.
*/

#ifndef INCLUDE_H
#define INCLUDE_H

#ifdef WIN32													// If We're Under MSVC
	#include <windows.h>											// Include The Windows Header
#else															// Otherwise
	#include <cstdio>												// Include The Standar IO Header
	#include <cstdlib>												// And The Standard Lib (for exit())
#endif															// Then...
#include <cmath>
#include <SDL.h>
#include <GL/gl.h>			// Header File For The OpenGL32 Library
#include <GL/glu.h>			// Header File For The GLu32 Library
#include <fstream>		// File read/write
#include <string>
#include <list>
#include <iterator>

using namespace std;

#define PI			3.14159f

//Maximums
#define MAX_BEING		256
#define MAX_SIZE		60
#define MAX_ENERGY		100
#define NUM_TEXTURES	1

//Display technicalities
#define VIEW_RADIUS		30
#define	ZOOM			30
#define CAMERA_SPEED	0.3f
#define SLIDE_DELAY		5
#define AI_DELAY		3

//Being types
#define VIP			0
#define DEFENDER	1
#define SOLDIER		2
#define SLAYER		3
#define ZOMBIE		4
#define SMARTZOMBIE	5
#define FASTZOMBIE	6
#define BIGZOMBIE	7
#define REAPER		8
#define	ZOMBIELORD	9
#define	STEALTH		10
#define CORPSE		11

//Player ownership
#define ENEMY		0
#define PLAYER1		1
#define PLAYER2		2

//Terrain types
#define FLOOR			0
#define WALL			1
#define GENERATOR		2
#define GENERATOR_SMART	3
#define GENERATOR_FAST	4
#define GENERATOR_BIG	5
#define GENERATOR_REAPER 6
#define GENERATOR_STEALTH 7

//Objectives
#define ROUT			0
#define SEIZE			1
#define ESCORT			2
#define TIMED_ROUT		3
#define TIMED_SEIZE		4
#define TIMED_ESCORT	5
#define SURVIVAL		6

#endif
