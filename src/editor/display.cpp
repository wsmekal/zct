#include "include.h"
#include "display.h"

Display::Display() {
	rotate = 0;
	cursorSpin = 0;
	subCursorHeight = 0;
	xCamera = 11;
	yCamera = 10;
	lightposition[0] = -1.0f;
	lightposition[1] =  0.0f;
	lightposition[2] = -5.0f;
	lightposition[3] =  1.0f;
	lightDiffuse[0] = 1.0f;
	lightDiffuse[1] = 1.0f;
	lightDiffuse[2] = 1.0f;
	lightDiffuse[3] = 1.0f;
}

void Display::BuildLists() {
	list=glGenLists(8);
	glNewList(list, GL_COMPILE);
		glBegin(GL_QUADS);
			glNormal3f(0.0f, 0.0f, 1.0f);			//List = Floor
			glVertex3f(-0.5f,-0.5f, 0.5f);
			glVertex3f(-0.5f, 0.5f, 0.5f);
			glVertex3f( 0.5f, 0.5f, 0.5f);
			glVertex3f( 0.5f,-0.5f, 0.5f);
		glEnd();
	glEndList();
	glNewList(list+1, GL_COMPILE);					//List+1 = Wall
		glBegin(GL_QUADS);
			glNormal3f(0.0f, 0.0f, 1.0f);
			glVertex3f(-0.5f,-0.5f, 0.5f);
			glVertex3f(-0.5f, 1.5f, 0.5f);
			glVertex3f( 0.5f, 1.5f, 0.5f);
			glVertex3f( 0.5f,-0.5f, 0.5f);
		glEnd();
	glEndList();
	glNewList(list+2, GL_COMPILE);
		glBindTexture(GL_TEXTURE_2D, texture[1]);	//List+2 = Particle
		glBegin(GL_TRIANGLE_STRIP);
			glNormal3f(0.0f, 0.0f, 1.0f);
			glTexCoord2f(0,1);
			glVertex3f(-0.7f, 0.7f, 0.0f);
			glTexCoord2f(1,1);
			glVertex3f( 0.7f, 0.7f, 0.0f);
			glTexCoord2f(0,0);
			glVertex3f(-0.7f,-0.7f, 0.0f);
			glTexCoord2f(1,0);
			glVertex3f( 0.7f,-0.7f, 0.0f);
		glEnd();
	glEndList();
	glNewList(list+3, GL_COMPILE);
		glBegin(GL_TRIANGLES);				//Being
			glNormal3f( 0.577f, 0.577f, 0.577f);
			glVertex3f( 0.0f, 0.0f, 1.2f);
			glVertex3f( 0.3f, 0.0f, 0.9f);
			glVertex3f( 0.0f, 0.3f, 0.9f);

			glNormal3f(-0.577f, 0.577f, 0.577f);
			glVertex3f( 0.0f, 0.0f, 1.2f);
			glVertex3f(-0.3f, 0.0f, 0.9f);
			glVertex3f( 0.0f, 0.3f, 0.9f);

			glNormal3f( 0.577f, 0.577f,-0.577f);
			glVertex3f( 0.0f, 0.0f, 0.6f);
			glVertex3f( 0.3f, 0.0f, 0.9f);
			glVertex3f( 0.0f, 0.3f, 0.9f);

			glNormal3f(-0.577f, 0.577f,-0.577f);
			glVertex3f( 0.0f, 0.0f, 0.6f);
			glVertex3f(-0.3f, 0.0f, 0.9f);
			glVertex3f( 0.0f, 0.3f, 0.9f);

			glNormal3f( 0.0f,-1.0f, 0.0f);
			glVertex3f( 0.0f, 0.0f, 1.2f);
			glVertex3f( 0.3f, 0.0f, 0.9f);
			glVertex3f( 0.0f, 0.0f, 0.6f);

			glNormal3f( 0.0f,-1.0f, 0.0f);
			glVertex3f( 0.0f, 0.0f, 1.2f);
			glVertex3f(-0.3f, 0.0f, 0.9f);
			glVertex3f( 0.0f, 0.0f, 0.6f);

			glNormal3f( 0.667f, 0.667f, 0.333f);
			glVertex3f( 0.0f, 0.0f, 0.9f);
			glVertex3f( 0.3f, 0.0f, 0.0f);
			glVertex3f( 0.0f, 0.3f, 0.0f);

			glNormal3f( 0.667f,-0.667f, 0.333f);
			glVertex3f( 0.0f, 0.0f, 0.9f);
			glVertex3f( 0.3f, 0.0f, 0.0f);
			glVertex3f( 0.0f,-0.3f, 0.0f);

			glNormal3f(-0.667f, 0.667f, 0.333f);
			glVertex3f( 0.0f, 0.0f, 0.9f);
			glVertex3f(-0.3f, 0.0f, 0.0f);
			glVertex3f( 0.0f, 0.3f, 0.0f);

			glNormal3f(-0.667f,-0.667f, 0.333f);
			glVertex3f( 0.0f, 0.0f, 0.9f);
			glVertex3f(-0.3f, 0.0f, 0.0f);
			glVertex3f( 0.0f,-0.3f, 0.0f);

			glNormal3f( 0.0f, 0.0f,-1.0f);
			glVertex3f( 0.0f, 0.3f, 0.0f);
			glVertex3f( 0.3f, 0.0f, 0.0f);
			glVertex3f(-0.3f, 0.0f, 0.0f);

			glNormal3f( 0.0f, 0.0f,-1.0f);
			glVertex3f( 0.0f,-0.3f, 0.0f);
			glVertex3f( 0.3f, 0.0f, 0.0f);
			glVertex3f(-0.3f, 0.0f, 0.0f);
		glEnd();
	glEndList();
	glNewList(list+4, GL_COMPILE);
		glColor3f(1.0f, 1.0f, 1.0f);
		glBegin(GL_LINES);				//List+4 = cursor
			glVertex3f( 0.0f, 0.2f, 0.0f);
			glVertex3f( 0.0f, 0.4f, 0.0f);
			glVertex3f( 0.0f,-0.2f, 0.0f);
			glVertex3f( 0.0f,-0.4f, 0.0f);
			glVertex3f( 0.2f, 0.0f, 0.0f);
			glVertex3f( 0.4f, 0.0f, 0.0f);
			glVertex3f(-0.2f, 0.0f, 0.0f);
			glVertex3f(-0.4f, 0.0f, 0.0f);
		glEnd();
		gluDisk(quadratic, 0.27f, 0.3f, 12, 1);
	glEndList();
	glNewList(list+5, GL_COMPILE);
		glBegin(GL_LINES);
			glNormal3f(0.0f, 0.0f, 1.0f);			//List+5 = subCursor
			glVertex3f(-0.5f,-0.5f, 0.5f);
			glVertex3f(-0.5f, 0.5f, 0.5f);

			glVertex3f(-0.5f, 0.5f, 0.5f);
			glVertex3f( 0.5f, 0.5f, 0.5f);

			glVertex3f( 0.5f, 0.5f, 0.5f);
			glVertex3f( 0.5f,-0.5f, 0.5f);

			glVertex3f( 0.5f,-0.5f, 0.5f);
			glVertex3f(-0.5f,-0.5f, 0.5f);
		glEnd();
	glEndList();
	glNewList(list+6, GL_COMPILE);					//List+6 = unmoved cursor
		glBegin(GL_LINES);
			glVertex3f( 0.0f, 0.0f, 1.4f);
			glVertex3f( 0.2f, 0.0f, 1.6f);

			glVertex3f( 0.2f, 0.0f, 1.6f);
			glVertex3f(-0.2f, 0.0f, 1.6f);

			glVertex3f(-0.2f, 0.0f, 1.6f);
			glVertex3f( 0.0f, 0.0f, 1.4f);
		glEnd();
	glEndList();
	glNewList(list+7, GL_COMPILE);					//List+7 = Generator
		glBegin(GL_QUADS);
			glNormal3f( 0.0f, 0.7f, 0.3f);	//North face
			glVertex3f( 0.4f, 0.4f, 0.0f);
			glVertex3f( 0.2f, 0.2f, 2.8f);
			glVertex3f(-0.2f, 0.2f, 2.8f);
			glVertex3f(-0.4f, 0.4f, 0.0f);

			glNormal3f( 0.0f,-0.7f, 0.3f);	//South face
			glVertex3f( 0.4f,-0.4f, 0.0f);
			glVertex3f( 0.2f,-0.2f, 2.8f);
			glVertex3f(-0.2f,-0.2f, 2.8f);
			glVertex3f(-0.4f,-0.4f, 0.0f);

			glNormal3f( 0.7f, 0.0f, 0.3f);	//East face
			glVertex3f( 0.4f,-0.4f, 0.0f);
			glVertex3f( 0.2f,-0.2f, 2.8f);
			glVertex3f( 0.2f, 0.2f, 2.8f);
			glVertex3f( 0.4f, 0.4f, 0.0f);

			glNormal3f(-0.7f, 0.0f, 0.3f);	//West face
			glVertex3f(-0.4f,-0.4f, 0.0f);
			glVertex3f(-0.2f,-0.2f, 2.8f);
			glVertex3f(-0.2f, 0.2f, 2.8f);
			glVertex3f(-0.4f, 0.4f, 0.0f);

			glNormal3f( 0.0f, 0.0f, 1.0f);	//Top
			glVertex3f( 0.2f,-0.2f, 2.8f);
			glVertex3f(-0.2f,-0.2f, 2.8f);
			glVertex3f(-0.2f, 0.2f, 2.8f);
			glVertex3f( 0.2f, 0.2f, 2.8f);
		glEnd();
	glEndList();
}

SDL_Surface* Display::LoadBMP( const char* Filename )				// Loads A Bitmap Image
{
	FILE *File=NULL;									// File Handle

	if (!Filename)										// Make Sure A Filename Was Given
		return NULL;									// If Not Return NULL

	File=fopen(Filename,"r");							// Check To See If The File Exists

	if (File)											// Does The File Exist?
	{
		fclose(File);									// Close The Handle
		return SDL_LoadBMP(Filename);		
	}

	return NULL;										// If Load Failed Return NULL
}

int Display::LoadGLTextures()									// Load Bitmaps And Convert To Textures
{
	bool Status=false;									// Status Indicator

	SDL_Surface* TextureImage[NUM_TEXTURES];					// Create Storage Space For The Texture
	memset( TextureImage, 0, sizeof(void *)*NUM_TEXTURES );           	// Set The Pointer To NULL

	// Load The Bitmap, Check For Errors, If Bitmap's Not Found Quit
	if( (TextureImage[0]=LoadBMP("data/font.bmp")) )			// Load Block Texture
	{
		Status=true;                                    // Set The Status To TRUE
		glGenTextures(NUM_TEXTURES, &texture[0]);                  // Create One Textures

		for (int loop1=0; loop1<NUM_TEXTURES; loop1++)			// Loop Through 5 Textures
		{
			glBindTexture(GL_TEXTURE_2D, texture[loop1]);
			glTexImage2D(GL_TEXTURE_2D, 0, 3, TextureImage[loop1]->w, TextureImage[loop1]->h, 0,
				GL_RGB, GL_UNSIGNED_BYTE, TextureImage[loop1]->pixels);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
			//change to texture matrix and do the trick
			glMatrixMode(GL_TEXTURE);
			glRotatef(180.0f,0.0f,0.0f,1.0f);
			glScalef(-1.0f,1.0f,1.0f);
			
			//back to normal
			glMatrixMode(GL_MODELVIEW);
		}
		for (int loop1=0; loop1<NUM_TEXTURES; loop1++)				// Loop Through 5 Textures
			if (TextureImage[loop1])			// If Texture Exists
				SDL_FreeSurface(TextureImage[loop1]);		// Free The Image Structure
	}
	return Status;										// Return The Status
}

void Display::BuildFont()								// Build Our Font Display List
{
	float	cx;											// Holds Our X Character Coord
	float	cy;											// Holds Our Y Character Coord

	base=glGenLists(256);								// Creating 256 Display Lists
	glBindTexture(GL_TEXTURE_2D, texture[0]);			// Select Our Font Texture
	for (int loop=0; loop<256; loop++)						// Loop Through All 256 Lists
	{
		cx=float(loop%16)/16.0f;						// X Position Of Current Character
		cy=float(loop/16)/16.0f;						// Y Position Of Current Character

		glNewList(base+loop,GL_COMPILE);				// Start Building A List
			glBegin(GL_QUADS);							// Use A Quad For Each Character
				glTexCoord2f(cx,1-cy-0.0625f);	// Texture Coord (Bottom Left)
				glVertex2i(0,0);						// Vertex Coord (Bottom Left)
				glTexCoord2f(cx+0.065f,1-cy-0.0625f);	// Texture Coord (Bottom Right)
				glVertex2i(16,0);						// Vertex Coord (Bottom Right)
				glTexCoord2f(cx+0.065f,1-cy);			// Texture Coord (Top Right)
				glVertex2i(16,16);						// Vertex Coord (Top Right)
				glTexCoord2f(cx,1-cy);			// Texture Coord (Top Left)
				glVertex2i(0,16);						// Vertex Coord (Top Left)
			glEnd();									// Done Building Our Quad (Character)
			glTranslated(10,0,0);						// Move To The Right Of The Character
		glEndList();									// Done Building The Display List
	}													// Loop Until All 256 Are Built
}

void Display::KillFont()									// Delete The Font From Memory
{
	glDeleteLists(base,256);							// Delete All 256 Display Lists
}

void Display::glPrint(GLint x, GLint y, float scale, int set, bool center, const char *string, ...)	// Where The Printing Happens
{
	char		text[256];				// Holds Our String
	if (set>1)
	{
		set=1;
	}
	va_list	ap;										// Pointer To List Of Arguments
	va_start(ap, string);							// Parses The String For Variables
	    vsprintf(text, string, ap);					// And Converts Symbols To Actual Numbers
	va_end(ap);										// Results Are Stored In Text

	glBindTexture(GL_TEXTURE_2D, texture[0]);			// Select Our Font Texture
	glDisable(GL_DEPTH_TEST);							// Disables Depth Testing
	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glPushMatrix();										// Store The Projection Matrix
	glLoadIdentity();									// Reset The Projection Matrix
	glOrtho(0,640,0,480,-1,1);							// Set Up An Ortho Screen
	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glPushMatrix();										// Store The Modelview Matrix
	glLoadIdentity();									// Reset The Modelview Matrix
	if(center) {
		glTranslated(x - strlen(text) * 5 * scale,y,0);	// Position The Text (0,0 - Bottom Left)
	} else {
		glTranslated(x,y,0);							// Position The Text (0,0 - Bottom Left)
	}
	glScalef(scale, scale, scale);
	glListBase(base-32+(128*set));						// Choose The Font Set (0 or 1)
	glCallLists(strlen(text),GL_BYTE,text);				// Write The Text To The Screen
	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glPopMatrix();										// Restore The Old Projection Matrix
	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glPopMatrix();										// Restore The Old Projection Matrix
	glEnable(GL_DEPTH_TEST);
}

bool Display::InitGL(SDL_Surface *S)										// Any OpenGL Initialization Code Goes Here
{
	srand(SDL_GetTicks());

	if(!LoadGLTextures())
		return false;
	BuildFont();
	glClearColor(0.0f, 0.0f, 0.0f, 0.5f);				// Black Background
	glClearDepth(1.0f);									// Depth Buffer Setup
	quadratic=gluNewQuadric();							// Create A Pointer To The Quadric Object
	gluQuadricNormals(quadratic, GLU_SMOOTH);			// Create Smooth Normals
	gluQuadricTexture(quadratic, GL_TRUE);				// Create Texture Coords
	gluQuadricOrientation(quadratic, GLU_INSIDE);		
	glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do
	glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
	glEnable(GL_COLOR_MATERIAL);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE);					// Select The Type Of Blending
	glEnable(GL_BLEND);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, lightDiffuse);
	glLightfv(GL_LIGHT1, GL_POSITION, lightposition);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHTING);
	glShadeModel(GL_SMOOTH);							// Enables Smooth Color Shading
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations
	glDisable(GL_TEXTURE_2D);
 	BuildLists();
	return true;										// Initialization Went OK
}


float Display::getRotate() {
	return rotate;
}

float Display::getXCamera() {
	return xCamera;
}

float Display::getYCamera() {
	return yCamera;
}


void Display::moveCamera(float x, float y) {
	Point temp = Point(x,y);
	temp.pivot(Point(0,0), ((rotate - 45) / 180 * PI));
	xCamera += temp.getX();
	yCamera += temp.getY();
}

void Display::pivotCamera(float rot) {
	rotate += rot;
}

void Display::drawTile(Controller *c, int x, int y) {
	Map m = c->getMap();
	if(m.getTile(x,y).getTerrain() == WALL) { 
		glTranslatef(0.0f, 0.0f, 1.5f);
		glColor3f(0.7f, 0.7f, 0.7f);
	} else if(m.getTile(x,y).getTerrain() == FLOOR){
		glTranslatef(0.0f, 0.0f,-0.5f);
		switch(m.getTile(x,y).getPlayer()) {
		case ENEMY:
			if(m.getTile(x,y).getTarget()) {
				glColor3f(0.5f + 0.25f * sin(cursorSpin * 0.03f), 0.5f + 0.25f * sin(cursorSpin * 0.03f), 0.5f + 0.25f * sin(cursorSpin * 0.03f));
			} else {
				glColor3f(0.5f, 0.5f, 0.5f);
			}
			break;
		case PLAYER1:
			if(m.getTile(x,y).getTarget()) {
				glColor3f(0.3f + 0.15f * sin(cursorSpin * 0.03f), 0.3f + 0.15f * sin(cursorSpin * 0.03f), 0.8f + 0.4f * sin(cursorSpin * 0.03f));
			} else {
				glColor3f(0.3f, 0.3f, 0.8f);
			}
			break;
		}
	} else if(m.getTile(x,y).getTerrain() >= GENERATOR) {
		glTranslatef(0.0f, 0.0f,-0.5f);
		//Switch to color for generator
		if(m.getTile(x,y).getPlayer() == PLAYER1) {
			if(m.getTile(x,y).getHighlight()) {
				glColor3f(0.0f, 0.0f, 1.0f);
			} else {
				glColor3f(0.0f, 0.0f, 0.5f);
			}
		} else {
			switch(m.getTile(x,y).getTerrain()) {
			case GENERATOR:
				if(m.getTile(x,y).getHighlight()) {
					glColor3f(0.0f, 0.7f, 0.0f);
				} else {
					glColor3f(0.0f, 0.35f, 0.0f);
				}
				break;
			case GENERATOR_SMART:
				if(m.getTile(x,y).getHighlight()) {
					glColor3f(0.0f, 1.0f, 0.0f);
				} else {
					glColor3f(0.0f, 0.5f, 0.0f);
				}
				break;
			case GENERATOR_FAST:
				if(m.getTile(x,y).getHighlight()) {
					glColor3f(0.5f, 1.0f, 0.5f);
				} else {
					glColor3f(0.25f, 0.5f, 0.25f);
				}
				break;
			case GENERATOR_BIG:
				if(m.getTile(x,y).getHighlight()) {
					glColor3f(0.0f, 0.5f, 0.0f);
				} else {
					glColor3f(0.0f, 0.25f, 0.0f);
				}
				break;
			case GENERATOR_REAPER:
				if(m.getTile(x,y).getHighlight()) {
					glColor3f(1.0f, 0.0f, 1.0f);
				} else {
					glColor3f(0.5f, 0.0f, 0.5f);
				}
				break;
			case GENERATOR_STEALTH:
				if(m.getTile(x,y).getHighlight()) {
					glColor3f(0.3f, 0.0f, 0.3f);
				} else {
					glColor3f(0.15f, 0.0f, 0.15f);
				}
				break;
			}
		}
		glCallList(list+7);
		//Then, switch to color for floor
		switch(m.getTile(x,y).getPlayer()) {
		case ENEMY:
			if(m.getTile(x,y).getTarget()) {
				glColor3f(0.5f + 0.25f * sin(cursorSpin * 0.03f), 0.5f + 0.25f * sin(cursorSpin * 0.03f), 0.5f + 0.25f * sin(cursorSpin * 0.03f));
			} else {
				glColor3f(0.5f, 0.5f, 0.5f);
			}
			break;
		case PLAYER1:
			if(m.getTile(x,y).getTarget()) {
				glColor3f(0.3f + 0.15f * sin(cursorSpin * 0.03f), 0.3f + 0.15f * sin(cursorSpin * 0.03f), 0.8f + 0.4f * sin(cursorSpin * 0.03f));
			} else {
				glColor3f(0.3f, 0.3f, 0.8f);
			}
			break;
		}
	}
	glCallList(list);		//Render floor/cieling
	glColor3f(0.4f, 0.4f, 0.4f);
	if(m.getTile(x,y).getWall()) {
		glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
		if(y == MAX_SIZE-1 || !m.getTile(x,min(y+1, MAX_SIZE-1)).getWall()) {
			glCallList(list+1);					//Draw the north wall
		}
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		if(x == MAX_SIZE-1 || !m.getTile(min(x+1, MAX_SIZE-1), y).getWall()) {
			glCallList(list+1);					//Draw the west wall
		}
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		if(y == 0 || !m.getTile(x, max(y-1,0)).getWall()) {
			glCallList(list+1);					//Draw the south wall
		}
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		if(x == 0 || !m.getTile(max(x-1,0),y).getWall()) {
			glCallList(list+1);					//Draw the east wall
		}
	}
}

void Display::draw(Being b) {
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f,-ZOOM);
	glRotatef(-30.0f, 1.0f, 0.0f, 0.0f);
	glRotatef( rotate, 0.0f, 0.0f, 1.0f);
	glTranslatef(b.getSeenPos().getX() - xCamera, b.getSeenPos().getY() - yCamera, 0);
	if(b.getType() == FASTZOMBIE) {
		glScalef(0.8f, 0.8f, 0.8f);
	} else if(b.getType() == BIGZOMBIE) {
		glScalef(1.2f, 1.2f, 1.2f);
	} else if(b.getType() == ZOMBIELORD) {
		glScalef(1.2f, 1.2f, 1.4f);
	}
	glRotatef(b.getSeenPos().getRot() + 180, 0.0f, 0.0f, 1.0f);
	glColor3f(b.getColor(0), b.getColor(1), b.getColor(2));
	glCallList(list+3);
}

void Display::draw(Controller c) {
	int radius = int(ZOOM) + 1;
	glClearColor(0.0f, 0.0f, 0.0f, 0.5f);
	int centerX = int(xCamera);
	int centerY = int(yCamera);

	//Loop through every tile
	for(int i = max(centerX - radius + 2, 0); i < min(centerX + radius + 2, c.getMap().getMaxX()); ++i) {
		for(int d = max(centerY - radius + 2, 0); d < min(centerY + radius + 2, c.getMap().getMaxY()); ++d) {
			//Render the tile
			glLoadIdentity();
			glTranslatef(0.0f, 0.0f,-ZOOM);
			glRotatef(-30.0f, 1.0f, 0.0f, 0.0f);
			glRotatef( rotate, 0.0f, 0.0f, 1.0f);
			glTranslatef(float(i) - xCamera, float(d) - yCamera, 0.0f);
			drawTile(&c, i, d);
			if(int(c.getXCursor()+0.5) == i && int(c.getYCursor()+0.5) == d) {
				//Draw the subcursor
				glLoadIdentity();
				glTranslatef(0.0f, 0.0f,-ZOOM);
				glRotatef(-30.0f, 1.0f, 0.0f, 0.0f);
				glRotatef( rotate, 0.0f, 0.0f, 1.0f);
				glTranslatef(float(i) - xCamera, float(d) - yCamera, 0.0f);
				glColor3f(1,1,1);
				glTranslatef(0,0,-0.49f);
				if(c.getMap().getTile(i,d).getWall()) {
					glTranslatef(0,0,2);
				}
				glCallList(list+5);
			}
		}
	}

	//Draw the people
	for(int i = 0; i < MAX_BEING; ++i) {
		if(c.getBeing(i).getAlive()) {
			draw(c.getBeing(i));
		}
	}

	//Draw the cursor
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f,-ZOOM);
	glRotatef(-30.0f, 1.0f, 0.0f, 0.0f);
	glRotatef( rotate, 0.0f, 0.0f, 1.0f);
	glTranslatef(c.getXCursor() - xCamera, c.getYCursor() - yCamera, 0);
	if(c.getMap().getTile(int(c.getXCursor() + 0.5), int(c.getYCursor()+0.5)).getWall() ||
			c.getXCursor() <-0.5f || c.getYCursor() <-0.5f ||
			int(c.getXCursor() + 0.5) > c.getMap().getMaxX()-1 || int(c.getYCursor() + 0.5) > c.getMap().getMaxY()-1) {
		glTranslatef(0,0,2);
	}
	if(c.getSelect() > -1) {
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_LIGHTING);
		switch(c.getSelect()) {
		case VIP:
			glColor3f(1,0,0);
			break;
		case DEFENDER:
			glColor3f(0,0,1);
			break;
		case SOLDIER:
			glColor3f(0.3f, 0.3f, 0.7f);
			break;
		case SLAYER:
			glColor3f(0,0,0);
			break;
		case ZOMBIE:
			glColor3f(0, 0.7f, 0);
			break;
		case SMARTZOMBIE:
			glColor3f(0,1,0);
			break;
		case FASTZOMBIE:
			glColor3f(0.5f, 1.0f, 0.5f);
			glScalef(0.8f, 0.8f, 0.8f);
			break;
		case BIGZOMBIE:
			glColor3f(0.0f, 0.5f, 0.0f);
			glScalef(1.2f, 1.2f, 1.2f);
			break;
		case REAPER:
			glColor3f(0.7f, 0.0f, 0.7f);
			break;
		case STEALTH:
			glColor3f(0.3f, 0.0f, 0.3f);
			break;
		case ZOMBIELORD:
			glColor3f(1.0f, 0.8f, 0.0f);
			glScalef(1.2f, 1.2f, 1.4f);
			break;
		}
		glRotatef(c.getSelectRot(), 0,0,1);
		glCallList(list+3);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);
		glColor3f(1,1,1);
	}
	glRotatef( cursorSpin, 0.0f, 0.0f, 1.0f);
	glCallList(list+4);
	glEnable(GL_DEPTH_TEST);

	//Draw the HUD
	glDisable(GL_LIGHTING);
	glEnable(GL_BLEND);
	glEnable(GL_TEXTURE_2D);
	glColor3f(1,1,1);
	glPrint(5,460,0.8f,false,false,"~-V.I.P");
	glPrint(5,445,0.8f,false,false,"1-Militia");
	glPrint(5,430,0.8f,false,false,"2-Soldier");
	glPrint(5,415,0.8f,false,false,"3-Slayer");
	glPrint(5,400,0.8f,false,false,"4-Zombie");
	glPrint(5,385,0.8f,false,false,"5-Smart Zombie");
	glPrint(5,370,0.8f,false,false,"6-Fast Zombie");
	glPrint(5,355,0.8f,false,false,"7-Big Zombie");
	glPrint(5,340,0.8f,false,false,"8-Reaper");
	glPrint(5,325,0.8f,false,false,"9-Zombie Lord");
	glPrint(5,310,0.8f,false,false,"0-Stealth Zombie");
	glColor3f(1,1,1);
	glPrint(505,460,0.8f,false,false,"F1-Rout");
	glPrint(505,445,0.8f,false,false,"F2-Seize");
	glPrint(505,430,0.8f,false,false,"F3-Escort");
	glPrint(505,415,0.8f,false,false,"F4-Timed Rout");
	glPrint(505,400,0.8f,false,false,"F5-Timed Seize");
	glPrint(505,385,0.8f,false,false,"F6-Timed Escort");
	glPrint(505,370,0.8f,false,false,"F7-Survival");
	switch(c.getObjective()) {
	case ROUT:
	case TIMED_ROUT:
		glPrint(320,20,0.8f,false,true,"Goal: Rout");
		break;
	case SEIZE:
	case TIMED_SEIZE:
		glPrint(320,20,0.8f,false,true,"Goal: Seize");
		break;
	case ESCORT:
	case TIMED_ESCORT:
		glPrint(320,20,0.8f,false,true,"Goal: Escort");
		break;
	case SURVIVAL:
		glPrint(320,20,0.8f,false,true,"Goal: Survival");
		break;
	}
	if(c.getObjective() >= TIMED_ROUT) {
		glPrint(320,5,0.8f,false,true,"Turns: %1.1i", c.getTime());
	}
	glPrint(420,20,0.8f,false,true,"%1.1ix%1.1i", c.getMap().getMaxX(), c.getMap().getMaxY());
	switch(c.getSelect()) {
	case VIP:
		glPrint(320,40,0.8f,false,true,"Select location for new V.I.P");
		break;
	case DEFENDER:
		glPrint(320,40,0.8f,false,true,"Select location for new Militia");
		break;
	case SOLDIER:
		glPrint(320,40,0.8f,false,true,"Select location for new Soldier");
		break;
	case SLAYER:
		glPrint(320,40,0.8f,false,true,"Select location for new Slayer");
		break;
	case ZOMBIE:
		glPrint(320,40,0.8f,false,true,"Select location for new Zombie");
		break;
	case SMARTZOMBIE:
		glPrint(320,40,0.8f,false,true,"Select location for new Smart Zombie");
		break;
	case FASTZOMBIE:
		glPrint(320,40,0.8f,false,true,"Select location for new Fast Zombie");
		break;
	case BIGZOMBIE:
		glPrint(320,40,0.8f,false,true,"Select location for new Big Zombie");
		break;
	case REAPER:
		glPrint(320,40,0.8f,false,true,"Select location for new Reaper");
		break;
	case STEALTH:
		glPrint(320,40,0.8f,false,true,"Select location for new Stealth Zombie");
		break;
	case ZOMBIELORD:
		glPrint(320,40,0.8f,false,true,"Select location for new Zombie Lord");
		break;
	case -ZOMBIE:
		glPrint(320,40,0.8f,false,true,"Select location for new Zombie Generator");
		break;
	case -SMARTZOMBIE:
		glPrint(320,40,0.8f,false,true,"Select location for new Smart Zombie Generator");
		break;
	case -FASTZOMBIE:
		glPrint(320,40,0.8f,false,true,"Select location for new Fast Zombie Generator");
		break;
	case -BIGZOMBIE:
		glPrint(320,40,0.8f,false,true,"Select location for new Big Zombie Generator");
		break;
	case -REAPER:
		glPrint(320,40,0.8f,false,true,"Select location for new Reaper Generator");
		break;
	case -STEALTH:
		glPrint(320,40,0.8f,false,true,"Select location for new Stealth Zombie Generator");
		break;
	}
	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);


	glEnable(GL_LIGHTING);
	//Spin the cursor, etc
	cursorSpin += 5;
	subCursorHeight += 0.03f;
	if(subCursorHeight > 1) {
		subCursorHeight -= 1;
	}
}
