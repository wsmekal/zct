#include "map.h"

int Map::stringToInt(char *str) {
	int result = 0;
	int negative = 0;
	if (str[0] == '-') {
		negative = 1;
		++str;
	} else if(str[1]=='-') {
		negative = 1;
		str += 2;
	}
	for (size_t i = 0; i < strlen(str); ++i) {
		if(str[i] != '\n' && str[i] != '.') {
			result *= 10;
			result += int(str[i] - 48);
		}
	}
	if (negative) {
		result *= -1;
		--str;
	}
	return result;
}

int Map::loadNumberFromFile(ifstream &file) {
	char buffer[32];
	file.getline(buffer, 32);
	return stringToInt(buffer);
}

Map::Map() {
	t = new Tile[MAX_SIZE*MAX_SIZE];
	maxX = MAX_SIZE;
	maxY = MAX_SIZE;
}

/*Map::~Map() {
	delete t;
}*/

int Map::getMaxX() {
	return maxX;
}

int Map::getMaxY() {
	return maxY;
}

Tile Map::getTile(int x, int y) {
	return t[x*MAX_SIZE+y];
}

void Map::setTile(int x, int y, Tile n_tile) {
	t[x*MAX_SIZE+y] = n_tile;
}

void Map::setMaxX(int n_maxX) {
	maxX = n_maxX;
}

void Map::setMaxY(int n_maxY) {
	maxY = n_maxY;
}

int Map::moveDistance(int x1, int y1, int x2, int y2) {
	return max(abs(x1-x2), abs(y1-y2));
}

bool Map::legalMove(int x1, int y1, int x2, int y2, int player) {
	if(moveDistance(x1, y1, x2, y2) <= 1) {
		if(getTile(x1, y1).getWall() == getTile(x2, y2).getWall() && 
			(getTile(x2, y2).getTerrain() < GENERATOR || player != ENEMY)) {
			if(getTile(x2, y2).getOccupied() > -1) {
				if(player == PLAYER1 && getTile(x2, y2).getOccupied() < ZOMBIE) {
					return false;
				}
				if(player == ENEMY && getTile(x2, y2).getOccupied() >= ZOMBIE) {
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}

void Map::clearHighlights() {
	for(int i = 0; i < MAX_SIZE * MAX_SIZE; ++i) {
		t[i].setHighlight(true);
	}
}

void Map::highlightMoves(int x, int y, int turns) {
	for(int i = 0; i < MAX_SIZE * MAX_SIZE; ++i) {
		t[i].setHighlight(false);
		t[i].setMarked(false);
	}
	t[x*MAX_SIZE+y].setHighlight(true);
	for(int d = 0; d < turns; ++d) {
		//Mark all places that can be moved to from highlighted squares
		for(int i = 0; i < MAX_SIZE; ++i) {
			for(int j = 0; j < MAX_SIZE; ++j) {
				if((getTile(i+1,j).getHighlight()     && legalMove(i+1,j, i,j, PLAYER1)) ||
					(getTile(i+1,j+1).getHighlight() && legalMove(i+1,j+1, i,j, PLAYER1)) ||
					(getTile(i,j+1).getHighlight()   && legalMove(i,j+1, i,j, PLAYER1)) ||
					(getTile(i-1,j+1).getHighlight() && legalMove(i-1,j+1, i,j, PLAYER1)) ||
					(getTile(i-1,j).getHighlight()   && legalMove(i-1,j, i,j, PLAYER1)) ||
					(getTile(i-1,j-1).getHighlight() && legalMove(i-1,j-1, i,j, PLAYER1)) ||
					(getTile(i,j-1).getHighlight()   && legalMove(i,j-1, i,j, PLAYER1)) ||
					(getTile(i+1,j-1).getHighlight() && legalMove(i+1,j-1, i,j, PLAYER1))) {
					t[i*MAX_SIZE+j].setMarked(true);
				}
			}
		}
		for(int i = 0; i < MAX_SIZE * MAX_SIZE; ++i) {
			if(t[i].getMarked()) {
				t[i].setMarked(false);
				t[i].setHighlight(true);
			}
		}
	}
}
