#ifndef DISPLAY_H
#define DISPLAY_H

#include "include.h"
#include "controller.h"
#include "being.h"

class Controller;
class Being;

class Display {
public:
	Display();

	void BuildLists();
	SDL_Surface* LoadBMP( const char* Filename );
	int LoadGLTextures();
	void BuildFont();
	void KillFont();
	void KillParticles();
	void glPrint(GLint x, GLint y, GLfloat scale, int set, bool center, const char *string, ...);
	bool InitGL(SDL_Surface *S);

	float getRotate();
	float getXCamera();
	float getYCamera();

	void moveCamera(float x, float y);
	void pivotCamera(float rot);
	void addParticle(int x, int y, float fire, bool steam);
	void modifyLight(Controller *c, int x, int y, float amount);

	void drawTile(Controller *c, int x, int y);
	void draw(Controller c);
	void draw(Being b);
private:
	GLUquadricObj *quadratic;

	int list;
	GLuint	texture[NUM_TEXTURES];
	GLuint	base;					// Base Display List For The Font
	float xCamera, yCamera;
	float lightposition[4];
	float lightDiffuse[4];

	float rotate;
	float cursorSpin;
	float subCursorHeight;
	bool showDamage;
};

#endif
