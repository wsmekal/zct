#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <vector>
#include <list>

#include "tile.h"
#include "location.h"
#include "being.h"
#include "display.h"
#include "map.h"
#include "pathnode.h"

class Being;
class Display;

class Controller {
public:
	Controller(Display * = NULL);

	int loadNumberFromFile(ifstream &file);

	Map getMap();
	Being getBeing(int id);
	float getXCursor();
	float getYCursor();
	int getSelect();
	int getTurn();
	int getEnergy();
	int getIncome();
	float getMaxEnergy();
	int getOutcome();
	int getObjective();
	int getTime();
	int getClock();
	int getStats(int id);

	void setMap(int x, int y, Tile n_map);
	void setUndo(Controller *n_undo);

	void generate(const char *f, ...);
	void moveCursor(int, int);		//Find Cursor position based on mouse position
	point2d findPath(point2d, point2d, bool fullPath, bool ignoreWalls, bool resume);
	int getBeingValue(int type);
	point2d findTarget(point2d, bool ignoreWalls);	//Finds the nearest target
	void moveZombieBeing(int id);
	void spawnZombie(int x, int y, int type);
	void process();
	void updateOccupation();
	void requestNewUnit(int num);
	bool generatorsDown();
	void saveZombieLord(int id);
	void issueOrder();
	void cancelOrder();
	void endPlayerTurn();
	void newPlayerTurn();
	void selectNextUnit();
	void duplicate(Controller *source);
	void revert();

	void killMap();

private:
	Map map;
	Being *b;
	Display *display;
	Controller *undo;

	GLfloat xCursor, yCursor;
	int select;		//Which being is selected
	int turn;		//Whose turn it is
	int counter;	//Delay between enemy AI turns
	float energy;	//Amount of Energy player has
	bool gen;		//Are we generating from slow generators this turn?

	int objective;	//Objective type
	int time;		//Time limit (If applicable)
	int clock;		//Time elapsed
	int outcome;	//0 = Undecided, 1 = humans win, -1 = zombies win, -2 = VIP died

	string firstnames[MAX_NAMES];
	string lastnames[MAX_NAMES];
	int maxFirstnames, maxLastnames;

	//Postgame Stats
	int stats[MAX_STATS];

	//Stuff for multi-frame AI decisions
	list<PathNode> openList;
	list<PathNode> closedList;
};

#endif
