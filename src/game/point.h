//Point.h
//Definition for class Point.

#ifndef __POINT_H
#define __POINT_H

#include "SDL_opengl.h"

class point2d {
	public:
		point2d();							//Constructors
		point2d(GLfloat, GLfloat);

		GLfloat getX();						//Accessors
		GLfloat getY();

		int getintX();						//integer Accessors
		int getintY();

		void setX(GLfloat);
		void setY(GLfloat);
		void setPoint(GLfloat, GLfloat);	//Mutators

		bool equals(point2d);
		void pivot(point2d, GLfloat);			//Rotate around point
		bool intersectPoint(point2d, GLfloat);	//Is Point within [distance] of other Point?
	protected:
		GLfloat x,y;
};

#endif  // __POINT_H
