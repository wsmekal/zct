#ifndef ORGANIZER_H
#define ORGANIZER_H

#include <fstream>
#include "main.h"
#include "map.h"

class Organizer {
public:
	Organizer();

	int loadNumberFromFile(std::ifstream &file);
	string getMapName(int id);
	bool getCleared(int id);
	int getTimeRecord(int id);
	int getSelect();
	int getNumMaps();
	int toUnlock(int id);
	float getXCursor();
	float getYCursor();

	void setTimeRecord(int id, int n_timeRecord);
	void setSelect(int n_select);

	void moveCursor(int x, int y);
	void load();
	void save();

private:
	string mapName[MAX_MAP];
	int timeRecord[MAX_MAP];
	int select;
	int numMaps;
	int numCleared;
	int xCursor, yCursor;
};

#endif
