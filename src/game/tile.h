#ifndef TILE_H
#define TILE_H

#include "point.h"

class Tile {
public:
	Tile(int n_terrain = 0);

	bool getWall();
	int getTerrain();
	int getPlayer();
	bool getHighlight();
	bool getMarked();
	bool getTarget();
	int getOccupied();

	void setTerrain(int n_terrain);
	void setPlayer(int n_player);
	void setHighlight(bool n_highlight);
	void setMarked(bool n_marked);
	void setTarget(bool n_target);
	void setOccupied(int n_occupied);
private:
	int terrain;	//1 = wall, 2+ = generators
	int player;		//Who owns this space
	bool marked;	//Marked for highlighting
	bool highlight; //Highlighted for move radii
	bool target;	//Is this one of the objective tiles?
	int occupied;	//Is there a being here? (Type)
};

#endif
