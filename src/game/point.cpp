//Point.cpp
//Function declarations for class Point.

#include <cmath>

#include "point.h"

point2d::point2d() {
	x = 0.0f;
	y = 0.0f;
}

point2d::point2d(GLfloat n_x, GLfloat n_y) {
	x = n_x;
	y = n_y;
}

void point2d::setX(GLfloat n_x) {
	x = n_x;
}

void point2d::setY(GLfloat n_y) {
	y = n_y;
}

void point2d::setPoint(GLfloat n_x, GLfloat n_y) {
	setX(n_x);
	setY(n_y);
}

GLfloat point2d::getX() {
	return x;
}

GLfloat point2d::getY() {
	return y;
}

int point2d::getintX() {
	return static_cast<int>(x);
}

int point2d::getintY() {
	return static_cast<int>(y);
}

bool point2d::equals(point2d pt) {
	if(pt.getX() == x && pt.getY() == y)
		return true;
	else
		return false;
}

void point2d::pivot(point2d pt, GLfloat degrees) {
	if(!equals(pt) && degrees != 0.0f) {				//If our points aren't the same
		GLfloat xNew, yNew;
		xNew = x - pt.getX();
		yNew = y - pt.getY();

		double rot = -atan(GLfloat(yNew) / GLfloat(xNew));	//Get the Angle & distance
		double dist = sqrt(pow(double(xNew),2) + 
							pow(double(yNew),2));
		rot -= M_PI / 2.0f;
		if(x < 0.0f) rot += M_PI;
		if(rot < 0.0) rot += M_PI * 2.0f;

		rot += degrees;

		xNew = -GLfloat(sin(rot) * dist);			//Get the new relative coords...
		yNew = -GLfloat(cos(rot) * dist);
		x = xNew + pt.getX();						//...and make them absolute
		y = yNew + pt.getY();
	}
}

bool point2d::intersectPoint(point2d pt, GLfloat r) {
	double dist = sqrt(pow(double(x - pt.getX()),2) + pow(double(y - pt.getY()),2));
	if(GLfloat(dist) > r)
		return false;
	else
		return true;
}
