
#ifndef _MAIN_H_
#define _MAIN_H_

#include <string>
using namespace std;

#include "SDL.h"										

//Defines
#define APP_NAME	"Zombie City Tactics" // The App Name And Caption
#define APP_VERSION	"1.11.2"

#define SCREEN_W	640	    // Screen Width Of Our App Is 640 Points
#define SCREEN_H	480		  // SCreen Height Of Our App Is 480 Points
#define SCREEN_BPP	16		// Screen Depth Of Our App Is 16 bit (65536 Colors)

#define MILLISECONDS	16

#define LOG_FILE	"zct.log"								// The Name Of The Log File

extern std::string dataPath; 

// Data Types
typedef struct												// We Use A Struct To Hold Application Runtime Data
{
	bool Visible;											// Is The Application Visible? Or Iconified?
	bool MouseFocus;										// Is The Mouse Cursor In The Application Field?
	bool KeyboardFocus;										// Is The Input Focus On Our Application?
}
	S_AppStatus;											// We Call It S_AppStatus

// Prototypes
int main(int, char **);										// The main() Function, Every Program Must Have One!		

bool InitErrorLog(void);									// Initializes The Error Log
void CloseErrorLog(void);									// Closes The Error Log
int  Log(const char *, ...);						// Uses The Error Log :)
char* formatString(const char* format, ...);    // printf-like function

bool InitTimers(Uint32 *);									// Initializes The Timers
bool InitGL(SDL_Surface *);									// Performs OpenGL Scene Initialization
bool CreateWindowGL(SDL_Surface *, int, int, int, Uint32);	// Create The OpenGL Window
SDL_Surface *SetUpIcon(char *);								// Load  A Bitmap And Set It As The Window Icon

void ReshapeGL(int, int);									// Resize The OpenGL Scene
void TerminateApplication(void);							// Send A SDL_QUIT Event To The Queue

bool Initialize(void);										// Performs User Initializations
void Deinitialize(void);									// Performs User De-Initializations
void Update(Uint32, Uint8 *, int, int, bool, bool, bool, bool);			// Update Data
void Draw(SDL_Surface *);									// Do The Drawings


// various defines 

// Maximums
#define MAX_BEING		512
#define MAX_SIZE		256
#define MAX_ENERGY		100
#define MAX_MAP			110
#define MAX_NAMES		512
#define NUM_TEXTURES	1

// Postgame Stats
#define MAX_STATS	  	10
#define MILITIABUILT	0
#define SOLDIERSBUILT	1
#define SLAYERSBUILT	2
#define MILITIADIED		3
#define SOLDIERSDIED	4
#define SLAYERSDIED		5
#define ZOMBIEKILLS		6
#define UNDOS			    7
#define ENERGYGET		  8
#define ENERGYSPENT		9

// Scenes
#define S_MENU			1
#define S_PLAY			2
#define S_STATS			3

// Display technicalities
#define VIEW_RADIUS		30
#define ZOOM_SPEED     0.5f
#define CAMERA_SPEED	0.2f
#define SLIDE_DELAY		5
#define AI_DELAY		3

// Being types
#define VIP			0
#define DEFENDER	1
#define SOLDIER		2
#define SLAYER		3
#define ZOMBIE		4
#define SMARTZOMBIE	5
#define FASTZOMBIE	6
#define BIGZOMBIE	7
#define REAPER		8
#define	ZOMBIELORD	9
#define	STEALTH		10
#define	ZOMBIEKING	11

// Player ownership
#define ENEMY		0
#define PLAYER1		1
#define PLAYER2		2

// Terrain types
#define FLOOR		      	  0
#define WALL			        1
#define GENERATOR         2
#define GENERATOR_SMART  	3
#define GENERATOR_FAST	  4
#define GENERATOR_BIG	    5
#define GENERATOR_REAPER  6
#define GENERATOR_STEALTH 7

// Objectives
#define ROUT			0
#define SEIZE			1
#define ESCORT			2
#define TIMED_ROUT		3
#define TIMED_SEIZE		4
#define TIMED_ESCORT	5
#define SURVIVAL		6

#endif
