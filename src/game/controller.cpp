
#include <cmath>

#include "controller.h"
#include "main.h"


int Controller::loadNumberFromFile(ifstream &file)
{
	char buffer[32];
	file.getline(buffer, 32);
	return atoi(buffer);
}


Controller::Controller(Display *n_display)
{
	display = n_display;
	b = new Being[MAX_BEING];
	map = Map();

	xCursor = 0.0f;
	yCursor = 1.0f;
	select = -1;
	turn = PLAYER1;
	counter = AI_DELAY;
	energy = MAX_ENERGY / 2;
	gen = false;
	undo = NULL;
}


Map Controller::getMap()
{
	return map;
}


Being Controller::getBeing(int id)
{
	return b[id];
}


float Controller::getXCursor()
{
	return xCursor;
}


float Controller::getYCursor()
{
	return yCursor;
}


int Controller::getSelect() {
	return select;
}


int Controller::getTurn() {
	return turn;
}


int Controller::getClock() {
	return clock;
}


int Controller::getEnergy() {
	return int(energy);
}


void Controller::setMap(int x, int y, Tile n_map)
{
	map.setTile(x, y, n_map.getTerrain());
}

void Controller::setUndo(Controller *n_undo)
{
	undo = n_undo;
}


void Controller::generate(const char *f, ...) 
{
	map = Map();
	char text[256];				// Holds Our String
	string middleInitial;
	char buffer;
	char buffer2[32];
	int x, y;

	va_list	ap;										// Pointer To List Of Arguments
	va_start(ap, f);							// Parses The String For Variables
	vsprintf(text, f, ap);					// And Converts Symbols To Actual Numbers
	va_end(ap);										// Results Are Stored In Text

	ifstream file1;
	file1.open(formatString("%s/data/firstnames.txt", dataPath.c_str()));
	maxFirstnames = int(loadNumberFromFile(file1));
	maxFirstnames = min(maxFirstnames, MAX_NAMES);

	for(int i = 0; i < maxFirstnames; ++i) {
		file1.getline(buffer2, 32);
		firstnames[i] = string(buffer2);
	}
	file1.close();

	ifstream file2;
	file2.open(formatString("%s/data/lastnames.txt", dataPath.c_str()));
	maxLastnames = int(loadNumberFromFile(file2));
	maxLastnames = min(maxLastnames, MAX_NAMES);
	for(int i = 0; i < maxLastnames; ++i) {
		file2.getline(buffer2, 32);
		lastnames[i] = string(buffer2);
	}
	file2.close();

	ifstream file;
	file.open(text);
	if(!file) {
#ifdef WIN32
		MessageBox( NULL,"One of the Map files could not be opened.","Initialization Error", MB_OK | MB_ICONINFORMATION );
#else
		fprintf( stderr, "Initialization Error: One of the Map files could not be opened." );
		exit(1);
#endif
	}

	//Load the terrain data
	map.setMaxX(loadNumberFromFile(file)+4);
	map.setMaxY(loadNumberFromFile(file)+4);
	objective = loadNumberFromFile(file);
	if(objective >= TIMED_ROUT) {
		time = loadNumberFromFile(file);
	}
	for(int i = 0; i < map.getMaxX(); ++i) {
		for(int d = 0; d < map.getMaxY(); ++d) {
			map.setTile(i,d,Tile(WALL));
		}
	}
	for(int i = 2; i < map.getMaxX()-2; ++i) {
		for(int d = 2; d < map.getMaxY()-2; ++d) {
			file.get(buffer);
			if(buffer == '0') {
				map.setTile(i,d,Tile(FLOOR));
			} else if(buffer == '1') {
				map.setTile(i,d,Tile(WALL));
			} else if(buffer == '2') {
				map.setTile(i,d,Tile(GENERATOR));
			} else if(buffer == '3') {
				map.setTile(i,d,Tile(GENERATOR_SMART));
			} else if(buffer == '4') {
				map.setTile(i,d,Tile(GENERATOR_FAST));
			} else if(buffer == '5') {
				map.setTile(i,d,Tile(GENERATOR_BIG));
			} else if(buffer == '6') {
				map.setTile(i,d,Tile(GENERATOR_REAPER));
			} else if(buffer == '7') {
				map.setTile(i,d,Tile(GENERATOR_STEALTH));
			}
		}
		file.get(buffer);
	}
	Tile t;
	for(int i = 2; i < map.getMaxX()-2; ++i) {
		for(int d = 2; d < map.getMaxY()-2; ++d) {
			file.get(buffer);
			if(buffer == '0') {
				t = map.getTile(i,d);
				t.setPlayer(ENEMY);
				map.setTile(i,d,t);
			} else if(buffer == '1') {
				t = map.getTile(i,d);
				t.setPlayer(PLAYER1);
				map.setTile(i,d,t);
			} else if(buffer == '2') {
				t = map.getTile(i,d);
				t.setPlayer(PLAYER2);
				map.setTile(i,d,t);
			}
		}
		file.get(buffer);
	}
	if(objective == SEIZE || objective == ESCORT || objective == TIMED_SEIZE || objective == TIMED_ESCORT) {
		for(int i = 2; i < map.getMaxX()-2; ++i) {
			for(int d = 2; d < map.getMaxY()-2; ++d) {
				file.get(buffer);
				if(buffer == '0') {
					t = map.getTile(i,d);
					t.setTarget(false);
					map.setTile(i,d,t);
				} else if(buffer == '1') {
					t = map.getTile(i,d);
					t.setTarget(true);
					map.setTile(i,d,t);
				}
			}
			file.get(buffer);
		}
	}
	//Load the being data
	loadNumberFromFile(file);   // reading empty line
	int numBeings = min(loadNumberFromFile(file), MAX_BEING);
	Location loc = Location();
	b = new Being[MAX_BEING];
	for(int i = 0; i < MAX_BEING; ++i) {
		b[i] = Being(this);
		if(i < numBeings) {
			b[i].setAlive(true);
			x = rand() % maxFirstnames;
			y = rand() % maxLastnames;
			if(rand() % 20 == 0) {
				middleInitial = ". ";
				middleInitial[0] = char(rand() % 26 + 65);
			} else {
				middleInitial = "";
			}
			b[i].setName(firstnames[x] + " " + middleInitial + lastnames[y]);
			loc.setX(float(loadNumberFromFile(file))+2);
			loc.setY(float(loadNumberFromFile(file))+2);
			loc.setRot(float(loadNumberFromFile(file)) * 45);
			b[i].setPos(loc);
			b[i].setSeenPos(loc);
			b[i].setType(loadNumberFromFile(file));
			b[i].setTurns(b[i].getMaxTurns());
			b[i].setHealth(b[i].getMaxHealth());
			if(b[i].getType() == REAPER && map.getTile(loc.getintX(), loc.getintY()).getTerrain() == WALL) {
				b[i].setZ(2);
			} else {
				b[i].setZ(0);
			}
			b[i].setSeenZ(b[i].getZ());
			if(b[i].getType() < ZOMBIE) {
				display->setCamera(b[i].getPos().getX(), b[i].getPos().getY());
				display->setSeenCamera(b[i].getPos().getX(), b[i].getPos().getY());
			}
		}
	}
	updateOccupation();
	outcome = 0;
	select = -1;
	energy = 50;
	clock = 1;
	file.close();
	turn = PLAYER1;
	gen = true;


	for(int i = 0; i < MAX_STATS; ++i) {
		stats[i] = 0;
	}
}


void Controller::moveCursor(int x, int y)
{
	xCursor = display->getXCamera() + ( (x - 320) * cos(display->getRotate() * M_PI / 180) - (y-280) * sin(display->getRotate() * M_PI / 180)) / 25.0f;
	yCursor = display->getYCamera() + (-(x - 320) * sin(display->getRotate() * M_PI / 180) - (y-280) * cos(display->getRotate() * M_PI / 180)) / 25.0f;
}


void Controller::endPlayerTurn()
{
	map.clearHighlights();

	if(turn == PLAYER1) {
		//End turns for all allied units and renew turns for zombie units
		for(int i = 0; i < MAX_BEING; ++i) {
			if(b[i].getType() < ZOMBIE) {
				b[i].setTurns(0);
			} else {
				b[i].setTurns(b[i].getMaxTurns());
				if((b[i].getType() == ZOMBIELORD || b[i].getType() == ZOMBIEKING) && generatorsDown()) {
					if(map.getTile(b[i].getPos().getintX(), b[i].getPos().getintY()).getTerrain() >= GENERATOR) {
						b[i].setAlive(false);
						updateOccupation();
					}
				}
			}
		}
		//Spawn new zombies
		gen = !gen;
		for(int i = 0; i < map.getMaxX(); ++i) {
			for(int d = 0; d < map.getMaxY(); ++d) {
				if(map.getTile(i,d).getTerrain() == GENERATOR && map.getTile(i,d).getPlayer() == ENEMY) {
					spawnZombie(i,d, ZOMBIE);
				} else if(map.getTile(i,d).getTerrain() == GENERATOR_SMART && map.getTile(i,d).getPlayer() == ENEMY) {
					spawnZombie(i,d, SMARTZOMBIE);
				} else if(map.getTile(i,d).getTerrain() == GENERATOR_FAST && map.getTile(i,d).getPlayer() == ENEMY) {
					spawnZombie(i,d, FASTZOMBIE);
				} else if(gen && map.getTile(i,d).getTerrain() == GENERATOR_BIG && map.getTile(i,d).getPlayer() == ENEMY) {
					spawnZombie(i,d, BIGZOMBIE);
				} else if(gen && map.getTile(i,d).getTerrain() == GENERATOR_REAPER && map.getTile(i,d).getPlayer() == ENEMY) {
					spawnZombie(i,d, REAPER);
				} else if(map.getTile(i,d).getTerrain() == GENERATOR_STEALTH && map.getTile(i,d).getPlayer() == ENEMY) {
					spawnZombie(i,d, STEALTH);
				}
			}
		}
		turn = ENEMY;
		select = -1;
	}
}


/*!
 * Calculate income of player. For every floor element the player posseses
 * he gets 0.5, while every generator increases the income for 5.
 *
 * returns income as integer
 */
int Controller::getIncome()
{
	float income = 0;

	for(int i = 0; i < map.getMaxX(); ++i) {
		for(int d = 0; d < map.getMaxY(); ++d) {
			if(map.getTile(i,d).getPlayer() == PLAYER1) {
				if(map.getTile(i,d).getTerrain() == FLOOR)
					income += 0.5f;
				else if(map.getTile(i,d).getTerrain() >= GENERATOR)
					income += 5.0f;
			}
		}
	}
	
	return int(income);
}


float Controller::getMaxEnergy()
{
	float maximum = MAX_ENERGY;

	for(int i = 0; i < map.getMaxX(); ++i) {
		for(int d = 0; d < map.getMaxY(); ++d) {
			if(map.getTile(i,d).getPlayer() == PLAYER1) {
				if(map.getTile(i,d).getTerrain() >= GENERATOR)
					maximum += 5;
			}
		}
	}
	return maximum;
}


int Controller::getStats(int id)
{
	return stats[id];
}

void Controller::newPlayerTurn()
{
	if(turn == ENEMY) {
		++clock;
		if(objective >= TIMED_ROUT && objective < SURVIVAL) {
			if(clock > time) {
				outcome = -3;
			}
		} else if(objective == SURVIVAL) {
			if(clock > time) {
				outcome = 3;
			}
		}
		//Give all player units their turns back
		for(int i = 0; i < MAX_BEING; ++i) {
			if(b[i].getType() < ZOMBIE) {
				b[i].setTurns(b[i].getMaxTurns());
			} else {
				b[i].setTurns(b[i].getMaxTurns());
				if(map.getTile(int(b[i].getPos().getintX()), int(b[i].getPos().getintY())).getTerrain() >= GENERATOR
					&& b[i].getType() != ZOMBIELORD && b[i].getType() != ZOMBIEKING) {
					//This thing failed to spawn, trash it
					b[i].setAlive(false);
				}
			}
		}
		//Generate energy from owned terrain
		energy += getIncome();
		stats[ENERGYGET] += getIncome();
		if(energy > getMaxEnergy()) {
			energy = getMaxEnergy();
		}
		turn = PLAYER1;
		select = -1;
	}
}

void Controller::updateOccupation() {
	bool zombies = false;
	bool humans = false;
	Tile t;
	for(int i = 0; i < map.getMaxX(); ++i) {
		for(int d = 0; d < map.getMaxY(); ++d) {
			t = map.getTile(i, d);
			t.setOccupied(-1);
			map.setTile(i, d, t);
		}
	}

	for(int i = 0; i < MAX_BEING; ++i) {
		if(b[i].getAlive()) {
			//Set the occupation of this tile
			t = map.getTile(int(b[i].getPos().getX()), int(b[i].getPos().getY()));
			t.setOccupied(b[i].getType());
			map.setTile(int(b[i].getPos().getX()), int(b[i].getPos().getY()), t);
			if(objective == SEIZE || objective == TIMED_SEIZE || objective == ESCORT || objective == TIMED_ESCORT) {
				if(t.getTarget() && b[i].getType() < ZOMBIE && (b[i].getType() == VIP || (objective != ESCORT && objective != TIMED_ESCORT))) {
					outcome = 2;
				}
			}
			//Adjust victory conditions
			if(b[i].getType() < ZOMBIE) {
				humans = true;
			} else {
				zombies = true;
			}
		}
	}
	if(humans && !zombies && generatorsDown()) {
		outcome = 1;
	} else if(zombies && !humans) {
		outcome = -1;
	}
	for(int i = 0; i < MAX_BEING; ++i) {
		if(b[i].getType() == STEALTH && b[i].getAlive()) {
			//Check for stealth zombie invisibility
			b[i].setVisible(map.getSpotted(b[i].getPos().getintX(), b[i].getPos().getintY()));
		}
		if(b[i].getType() == VIP && b[i].getAlive() == false) {
			outcome = -2;
		}
	}
}

point2d Controller::findPath(point2d start, point2d end, bool fullPath, bool ignoreWalls, bool resume) {
	int startTime = SDL_GetTicks();
	//Returns the first point along the shortest path from start to end
	PathNode result = PathNode(start);
	int xShift[8] = {-1, 1, 1,-1, 1, 0,-1, 0};
	int yShift[8] = {-1,-1, 1, 1, 0, 1, 0,-1};
	if(!resume) {
		openList.clear();
		closedList.clear();
	}
	list<PathNode>::iterator iter = openList.begin();
	PathNode next, startnode;

	if(!resume) {
		//Add the starting node to the closed list
		closedList.push_back(PathNode(start));
		//Add all squares adjacent to the starting node to the open list
		for(int i = 0; i < 8; ++i) {
			next = PathNode(start.getX()+xShift[i],start.getY()+yShift[i],&closedList.back());
			//Is this a legal move?
			if(map.legalMove(start.getintX(), start.getintY(), next.getintX(), next.getintY(), ENEMY, ignoreWalls, true)) {
				//Then add it to the open list.
				openList.push_back(next);
			}
		}
	}
	//Loop
	float lowestF;
	list<PathNode>::iterator best = openList.begin();
	while(!openList.empty()) {
		//Find the open node with the lowest F cost
		iter = openList.begin();
		lowestF = iter->getF(end.getX(), end.getY());
		best = iter;
		for(iter = openList.begin(); iter != openList.end(); ++iter) {
			if(iter->getF(end.getX(), end.getY()) <= lowestF) {
				lowestF = iter->getF(end.getX(), end.getY());
				best = iter;
			}
		}
		//Move the node to the closed list
		result = PathNode(best->getX(), best->getY(), best->getParent());
		if(!fullPath) {
			//Dumb zombie, so we assume this is the best possible route now
			//If this space is occupied...
			if(!map.legalMove(start.getintX(), start.getintY(), result.getintX(), result.getintY(), ENEMY, ignoreWalls, false)) {
				//Examine all 8 of our potential target nodes
				openList.clear();
				for(int i = 0; i < 8; ++i) {
					startnode = PathNode(start);
					next = PathNode(start.getX()+xShift[i],start.getY()+yShift[i],&startnode);
					//Is this a legal move?
					if(map.legalMove(start.getintX(), start.getintY(), next.getintX(), next.getintY(), ENEMY, ignoreWalls, false)) {
						//Then add it to the open list.
						openList.push_back(next);
					}
				}
				if(openList.empty()) {
					//We're immobile
					return start;
				}
				iter = openList.begin();
				lowestF = iter->getF(end.getX(), end.getY());
				best = iter;
				for(iter = openList.begin(); iter != openList.end(); ++iter) {
					if(iter->getF(end.getX(), end.getY()) <= lowestF) {
						lowestF = iter->getF(end.getX(), end.getY());
						best = iter;
					}
				}
				//Move the node to the closed list
				return point2d(best->getX(), best->getY());
			}
			return point2d(result.getX(), result.getY());
		}
		closedList.push_back(*best);
		openList.erase(best);
		//Was that the destination?
		if(result.getX() == end.getX() && result.getY() == end.getY()) {
			//If so:
			//Trace backwards through parents
			PathNode after = result;
			while(result.getParent()->getX() != start.getX() || result.getParent()->getY() != start.getY()) {
				after = result;
				result = *result.getParent();
			}
			//If this space is occupied...
			if(!map.legalMove(start.getintX(), start.getintY(), result.getintX(), result.getintY(), ENEMY, ignoreWalls, false)) {
				//Examine all 8 of our potential target nodes
				openList.clear();
				for(int i = 0; i < 8; ++i) {
					startnode = PathNode(start);
					next = PathNode(start.getX()+xShift[i],start.getY()+yShift[i],&startnode);
					//Is this a legal move?
					if(map.legalMove(start.getintX(), start.getintY(), next.getintX(), next.getintY(), ENEMY, ignoreWalls, false)) {
						//Then add it to the open list.
						openList.push_back(next);
					}
				}
				if(openList.empty()) {
					//We're immobile
					return start;
				}
				iter = openList.begin();
				lowestF = iter->getF(result.getX(), result.getY());
				best = iter;
				for(iter = openList.begin(); iter != openList.end(); ++iter) {
					if(iter->getF(after.getX(), after.getY()) <= lowestF) {
						lowestF = iter->getF(after.getX(), after.getY());
						best = iter;
					}
				}
				return point2d(best->getX(), best->getY());
			}
			//Return it!
			return point2d(result.getX(), result.getY());
		}
		//For all nodes adjacent to the current node:
		bool addNode;
		for(int i = 0; i < 8; ++i) {
			addNode = true;
			next = PathNode(result.getX()+xShift[i],result.getY()+yShift[i],&closedList.back());
			//Is this a legal move?
			if(!map.legalMove(result.getintX(), result.getintY(), next.getintX(), next.getintY(), ENEMY, ignoreWalls, true)) {
				addNode = false;
			} else {
				//Search the closedList for this node
				for(iter = closedList.begin(); iter != closedList.end(); ++iter) {
					if(iter->getX() == next.getX() && iter->getY() == next.getY()) {
						//It's on the closed list - don't add it
						addNode = false;
						break;
					}
				}
				if(addNode) {
					//Search the openList for this node
					for(iter = openList.begin(); iter != openList.end(); ++iter) {
						if(iter->getX() == next.getX() && iter->getY() == next.getY()) {
							//Is the old route cheaper?
							if(iter->getG() <= next.getG()) {
								//This node is outdated - don't add it
								addNode = false;
							}
						}
					}
				}
			}
			if(addNode) {
				//Then add it to the open list.
				openList.push_back(next);
			}
		}
		int currentTime = SDL_GetTicks();
		if(currentTime > startTime + 10) {
			//We're taking too much time, stall for a turn
			counter = -1000;
			return start;
		}
	}
	//There's no path...
	if(fullPath) {
		//If we can't get there smart, try to get there stupid
		return findPath(start, end, false, ignoreWalls, false);
	} else {
		result = PathNode(start);
		result.setX(result.getX() + rand() % 3 - 1);
		result.setY(result.getY() + rand() % 3 - 1);
		if(map.legalMove(result.getintX(), result.getintY(), next.getintX(), next.getintY(), ENEMY, ignoreWalls, false)) {
			return point2d(result.getX(), result.getY());
		} else {
			return start;
		}
	}
}


int Controller::getBeingValue(int type)
{
	int value=-2;
	
	switch(type) {
	case DEFENDER:
		value=1;
		break;
	case SOLDIER:
		value=2;
		break;
	case SLAYER:
		value=3;
		break;
	case VIP:
		value=4;
		break;
	default:
		value=-2;
		break;
	}
	return value;
}


int Controller::getOutcome()
{
	return outcome;
}

int Controller::getObjective()
{
	return objective;
}

int Controller::getTime()
{
	return time;
}


point2d Controller::findTarget(point2d start, bool ignoreWalls)
{
	int xShift[8] = {-1, 0, 1, 1, 1, 0,-1,-1};
	int yShift[8] = {-1,-1,-1, 0, 1, 1, 1, 0};
	Tile t;
	point2d result = start;
	bool targetFound = false;
	bool openSquares = true;

	//Current target data
	float targetDist = 10000;	//Squared distance from origin
	int targetPriority = -1;	//Value of target

	map.deClearHighlights();

	//Highlight the main square
	t = map.getTile(start.getintX(), start.getintY());
	t.setHighlight(1);
	map.setTile(start.getintX(), start.getintY(), t);

	while(openSquares && !targetFound) {
		openSquares = false;

		//Check prospect new tiles
		for(int i = 0; i < map.getMaxX(); ++i) {
			for(int d = 0; d < map.getMaxY(); ++d) {
				if(map.getTile(i,d).getHighlight() == 1) {
					//Old tile, check all spaces around it
					for(int j = 0; j < 8; ++j) {
						if(map.getTile(i+xShift[j], d+yShift[j]).getHighlight() == 0 && 
							!map.getTile(i+xShift[j], d+yShift[j]).getMarked() &&
							map.legalMove(i,d,i+xShift[j], d+yShift[j], ENEMY, ignoreWalls, true)) {
							//Add this to the new squares list
							openSquares = true;
							t = map.getTile(i+xShift[j], d+yShift[j]);
							t.setMarked(true);
							map.setTile(i+xShift[j], d+yShift[j], t);
							//Is this a target square?
							if(map.getTile(i+xShift[j], d+yShift[j]).getTerrain() >= GENERATOR && 
								map.getTile(i+xShift[j], d+yShift[j]).getPlayer() == PLAYER1) {
								targetFound = true;
								if(targetPriority < 4) {
									//Our old target is outclassed by this new one!
									targetPriority = 4;
									targetDist = pow(float(i+xShift[j])-start.getX(), 2) + 
													pow(float(d+yShift[j])-start.getY(), 2);
									result = point2d(i+xShift[j], d+yShift[j]);
								} else if(targetPriority == 4) {
									float temp = pow(float(i+xShift[j])-start.getX(), 2) + 
													pow(float(d+yShift[j])-start.getY(), 2);
									if(targetDist > temp) {
										//This one is closer! Change target to it.
										result = point2d(i+xShift[j], d+yShift[j]);
										targetDist = temp;
									}
								}
							} else if(map.getTile(i+xShift[j], d+yShift[j]).getOccupied() > -1) {
								//We found us a BEING
								if(getBeingValue(map.getTile(i+xShift[j], d+yShift[j]).getOccupied()) > targetPriority) {
									targetFound = true;
									//This is a better being, let's target it
									targetPriority = getBeingValue(map.getTile(i+xShift[j], d+yShift[j]).getOccupied());
									targetDist = pow(float(i+xShift[j])-start.getX(), 2) + 
													pow(float(d+yShift[j])-start.getY(), 2);
									result = point2d(i+xShift[j], d+yShift[j]);
								} else if(getBeingValue(map.getTile(i+xShift[j], d+yShift[j]).getOccupied()) == targetPriority) {
									targetFound = true;
									float temp = pow(float(i+xShift[j])-start.getX(), 2) + 
													pow(float(d+yShift[j])-start.getY(), 2);
									if(targetDist > temp) {
										//This one is closer! Change target to it.
										result = point2d(i+xShift[j], d+yShift[j]);
										targetDist = temp;
									}
								}
							}
						}
					}
				}
			}
		}

		//Change all the marked tiles to highlighted tiles
		for(int i = 0; i < map.getMaxX(); ++i) {
			for(int d = 0; d < map.getMaxY(); ++d) {
				if(map.getTile(i, d).getMarked()) {
					t = map.getTile(i,d);
					t.setMarked(false);
					t.setHighlight(1);
					map.setTile(i,d,t);
				}
			}
		}
	}

	map.clearHighlights();
	return result;
}

void Controller::moveZombieBeing(int id) {
	//Find nearest enemy
	Location end = b[id].getPos();
	int target = -1;
	int targetDist = 1000;
	point2d targetPos;
	int hunting = 0;
	for(int i = 0; i < MAX_BEING; ++i) {
		//Hunt for beings
		if(b[i].getAlive() && b[i].getType() < ZOMBIE) {
			if(targetDist > map.moveDistance(int(b[id].getPos().getX()), int(b[id].getPos().getY()), 
											 int(b[i].getPos().getX()), int(b[i].getPos().getY()))) {
				//This is a tastier target
				target = i;
				targetDist = map.moveDistance(int(b[id].getPos().getX()), int(b[id].getPos().getY()), 
											 int(b[i].getPos().getX()), int(b[i].getPos().getY()));
			} else if(targetDist == map.moveDistance(int(b[id].getPos().getX()), int(b[id].getPos().getY()), 
											 int(b[i].getPos().getX()), int(b[i].getPos().getY()))) {
				if(getBeingValue(b[i].getType()) > getBeingValue(b[target].getType())) {
					//Does this being have a higher value?
					target = i;
				} else  if(getBeingValue(b[i].getType()) == getBeingValue(b[target].getType())) {
					//Is it closer?
					float temp = pow(b[i].getPos().getX()-b[id].getPos().getX(), 2) + 
									pow(b[i].getPos().getY()-b[id].getPos().getY(), 2);
					float temp2 = pow(b[target].getPos().getX()-b[id].getPos().getX(), 2) + 
									pow(b[target].getPos().getY()-b[id].getPos().getY(), 2);
					if(temp2 > temp) {
						//This one is closer! Change target to it.
						target = i;
					}
				}
			}
		}
	}
	targetPos = b[target].getPos();
	//Hunt for generators nearby, they are preferable targets
	if(b[id].getType() != ZOMBIELORD && b[id].getType() != ZOMBIEKING) {
		for(int i = 0; i < map.getMaxX(); ++i) {
			for(int d = 0; d < map.getMaxY(); ++d) {
				if(map.getTile(i,d).getTerrain() >= GENERATOR && map.getTile(i,d).getPlayer() != ENEMY) {
					if(map.moveDistance(int(b[id].getPos().getX()), int(b[id].getPos().getY()), i, d) < targetDist) {
						hunting = 1;
						targetPos = point2d(i,d);
						targetDist = map.moveDistance(int(b[id].getPos().getX()), int(b[id].getPos().getY()), i, d);
					} else if(map.moveDistance(int(b[id].getPos().getX()), int(b[id].getPos().getY()), i, d) == targetDist) {
						float temp = pow(i-b[id].getPos().getX(), 2) + 
										pow(d-b[id].getPos().getY(), 2);
						float temp2 = pow(targetPos.getX()-b[id].getPos().getX(), 2) + 
										pow(targetPos.getY()-b[id].getPos().getY(), 2);
						if(temp2 > temp || hunting == 0) {
							//This one is closer! Change target to it.
							hunting = 1;
							targetPos = point2d(i,d);
							targetDist = map.moveDistance(int(b[id].getPos().getX()), int(b[id].getPos().getY()), i, d);
						}
					}
				}
			}
		}
		if(map.getTile(targetPos.getintX(), targetPos.getintY()).getTerrain() >= GENERATOR) {
			hunting = 1;
		}
	}
	if(targetDist >= 5 && b[id].getType() != ZOMBIELORD && b[id].getType() != ZOMBIEKING) {
		//The enemy is too far, let's look for territory to seize
		for(int i = 0; i < map.getMaxX(); ++i) {
			for(int d = 0; d < map.getMaxY(); ++d) {
				if(map.getTile(i,d).getTerrain() == FLOOR && map.getTile(i,d).getPlayer() != ENEMY) {
					if(map.moveDistance(int(b[id].getPos().getX()), int(b[id].getPos().getY()), i, d) < 5) {
						if(map.moveDistance(int(b[id].getPos().getX()), int(b[id].getPos().getY()), i, d) < targetDist) {
							hunting = 2;
							targetPos = point2d(i,d);
							targetDist = map.moveDistance(int(b[id].getPos().getX()), int(b[id].getPos().getY()), i, d);
						} else if(map.moveDistance(int(b[id].getPos().getX()), int(b[id].getPos().getY()), i, d) == targetDist) {
							float temp = pow(i-b[id].getPos().getX(), 2) + 
											pow(d-b[id].getPos().getY(), 2);
							float temp2 = pow(targetPos.getX()-b[id].getPos().getX(), 2) + 
											pow(targetPos.getY()-b[id].getPos().getY(), 2);
							if(temp2 > temp) {
								//This one is closer! Change target to it.
								hunting = 1;
								targetPos = point2d(i,d);
								targetDist = map.moveDistance(int(b[id].getPos().getX()), int(b[id].getPos().getY()), i, d);
							}
						}
					}
				}
			}
		}
		if(map.getTile(targetPos.getintX(), targetPos.getintY()).getTerrain() == FLOOR) {
			hunting = 2;
		}
	}
	if(counter > -1000) {
		//Normal movement
		switch(b[id].getType()) {
		case ZOMBIE:
			if(hunting >= 1) {
				end = findPath(end, targetPos, false, false, false);
			} else {
				end = findPath(end, b[target].getPos(), false, false, false);
			}
			break;
		case SMARTZOMBIE:
		case STEALTH:
		case FASTZOMBIE:
		case ZOMBIELORD:
		case ZOMBIEKING:
			if(hunting >= 1) {
				end = findPath(end, targetPos, true, false, false);
			} else {
				end = findPath(end, b[target].getPos(), true, false, false);
			}
			break;
		case BIGZOMBIE:
		case REAPER:
			if(hunting >= 1) {
				end = findPath(end, targetPos, false, true, false);
			} else {
				end = findPath(end, b[target].getPos(), false, true, false);
			}
			break;
		}
		if(counter <= -1000) {
			//We interrupted the move to keep the framerate up, don't move
			return;
		}
	} else {
		//Resuming a dropped move
		counter = -100;
		switch(b[id].getType()) {
		case ZOMBIE:
			if(hunting >= 1) {
				end = findPath(end, targetPos, false, false, true);
			} else {
				end = findPath(end, b[target].getPos(), false, false, true);
			}
			break;
		case SMARTZOMBIE:
		case STEALTH:
		case FASTZOMBIE:
		case ZOMBIELORD:
		case ZOMBIEKING:
			if(hunting >= 1) {
				end = findPath(end, targetPos, true, false, true);
			} else {
				end = findPath(end, b[target].getPos(), true, false, true);
			}
			break;
		case BIGZOMBIE:
		case REAPER:
			if(hunting >= 1) {
				end = findPath(end, targetPos, false, true, true);
			} else {
				end = findPath(end, b[target].getPos(), false, true, true);
			}
			break;
		}
		if(counter <= -1000) {
			//We interrupted the move to keep the framerate up, don't move
			return;
		}
	}
	Tile t;
	//Are we moving at all?
	if(end.equals(b[id].getPos())) {
		//If not, end the unit's turn
		b[id].setTurns(0);
		select = -1;
	}
	//Is there a being here?
	if(hunting == 0 && end.equals(b[target].getPos())) {
		b[target].setAlive(false);
		switch(b[target].getType()) {
		case DEFENDER:
			++stats[MILITIADIED];
			break;
		case SOLDIER:
			++stats[SOLDIERSDIED];
			break;
		case SLAYER:
			++stats[SLAYERSDIED];
			break;
		}
		if(b[id].getType() != ZOMBIEKING) {
			b[id].setTurns(0);
			select = -1;
		}
	}

	//Are we destroying a wall tile?
	if(b[id].getType() == BIGZOMBIE && map.getTile(end.getintX(), end.getintY()).getTerrain() == WALL) {
		t = map.getTile(int(end.getintX()), int(end.getintY()));
		t.setTerrain(FLOOR);
		map.setTile(int(end.getX()), int(end.getY()), t);
		b[id].setTurns(0);
		select = -1;
		end = b[id].getPos();
	}
	if(b[id].getType() == REAPER) {
		if(map.getTile(end.getintX(), end.getintY()).getTerrain() == FLOOR) {
			b[id].setZ(0);
		} else {
			b[id].setZ(2);
		}
	}

	//Since zombies can't enter generators, see if we're next to it (hack)
	if(hunting == 1 && targetDist == 1) {
		end = targetPos;
		b[id].setTurns(0);
		select = -1;
	}

	//Face target tile
	float temp = 0;
	if(end.getX() < b[id].getPos().getX()) {
		if(end.getY() < b[id].getPos().getY()) {
			temp = 315;
		} else if(end.getY() > b[id].getPos().getY()) {
			temp = 225;
		} else {
			temp = 270;
		}

	} else if(end.getX() > b[id].getPos().getX()) {
		if(end.getY() < b[id].getPos().getY()) {
			temp = 45;
		} else if(end.getY() > b[id].getPos().getY()) {
			temp = 135;
		} else {
			temp = 90;
		}
	} else {
		if(end.getY() < b[id].getPos().getY()) {
			temp = 0;
		} else if(end.getY() > b[id].getPos().getY()) {
			temp = 180;
		} else {
			temp = end.getRot();
		}
	}
	//Move to target tile
	end.setRot(temp);
	if(hunting == 1 && targetDist == 1) {
		end.setX(b[id].getPos().getX());
		end.setY(b[id].getPos().getY());
		b[id].setPos(end);
		if(b[id].getType() == REAPER) {
			if(map.getTile(end.getintX(), end.getintY()).getTerrain() == FLOOR) {
				b[id].setZ(0);
			} else {
				b[id].setZ(2);
			}
		}
		t = map.getTile(int(targetPos.getintX()), int(targetPos.getintY()));
		t.setPlayer(ENEMY);
		map.setTile(int(targetPos.getX()), int(targetPos.getY()), t);
	} else {
		b[id].setPos(end);
		t = map.getTile(int(b[id].getPos().getintX()), int(b[id].getPos().getintY()));
		t.setPlayer(ENEMY);
		map.setTile(int(b[id].getPos().getX()), int(b[id].getPos().getY()), t);
	}
	b[id].setTurns(b[id].getTurns() - 1);
	if(b[id].getTurns() <= 0) {
		select = -1;
	}
	updateOccupation();
}

void Controller::spawnZombie(int x, int y, int type) {
	for(int i = 0; i < MAX_BEING; ++i) {
		if(!b[i].getAlive()) {
			b[i].setAlive(true);
			int x2 = rand() % maxFirstnames;
			int y2 = rand() % maxLastnames;
			string middleInitial;
			if(rand() % 20 == 0) {
				middleInitial = ". ";
				middleInitial[0] = char(rand() % 26 + 65);
			} else {
				middleInitial = "";
			}
			b[i].setName(firstnames[x2] + " " + middleInitial + lastnames[y2]);
			b[i].setZ(0);
			b[i].setPos(Location(float(x), float(y), rand() % 8 * 45));
			b[i].setSeenPos(b[i].getPos());
			b[i].setType(type);
			b[i].setTurns(b[i].getMaxTurns());
			b[i].setHealth(b[i].getMaxHealth());
			break;
		}
	}
	updateOccupation();
}

void Controller::process() {
	for(int i = 0; i < MAX_BEING; ++i) {
		b[i].adjustPos();
	}
	if(outcome != 0) {
		turn = PLAYER1;
	}
	if(turn == ENEMY) {
		--counter;
		if(counter <= 0) {
			if(select == -1) {
				int i;
				//find a unit that has not taken a turn yet
				for(i = 0; i < MAX_BEING; ++i) {
					if(b[i].getType() >= ZOMBIE && b[i].getTurns() > 0 && b[i].getAlive()) {
						select = i;
						break;
					}
				}
				if(i >= MAX_BEING) {
					//Nothing left to move, turn over
					newPlayerTurn();
				}
			} else {
				if(b[select].getType() != ZOMBIEKING || clock > 9) {
					moveZombieBeing(select);
				} else {
					b[select].setTurns(0);
					select = -1;
				}
			}
			if(counter > -1000) counter += AI_DELAY;
		}
	}
}

void Controller::requestNewUnit(int num) {
	if(turn == PLAYER1 && outcome == 0) {
		if(energy >= (1+num)*10) {
			map.clearHighlights();
			select = -1-num;
		}
	}
}

void Controller::selectNextUnit() {
	int initialSelect = select;
	if(turn == PLAYER1 && outcome == 0) {
		map.clearHighlights();
		++select;
		while(!b[select].getAlive() || b[select].getType() >= ZOMBIE || b[select].getTurns() < 1) {
			++select;
			if(select >= MAX_BEING) {
				select = -1;
				return;
			}
			if(select == initialSelect) {
				//There's nothing we can select
				return;
			}
		}
		map.highlightMoves(b[select].getPos().getintX(), b[select].getPos().getintY(), b[select].getTurns(), b[select].getMaxTurns());
		display->setCamera(b[select].getPos().getX(), b[select].getPos().getY());
	}
}

bool Controller::generatorsDown() {
	for(int i = 0; i < map.getMaxX(); ++i) {
		for(int d = 0; d < map.getMaxY(); ++d) {
			if(map.getTile(i,d).getTerrain() >= GENERATOR && map.getTile(i,d).getPlayer() == ENEMY) {
				return false;
			}
		}
	}
	return true;
}

void Controller::saveZombieLord(int id) {
	//Move being[id] to a random enemy-held generator
	int amount = 0;
	for(int i = 0; i < map.getMaxX(); ++i) {
		for(int d = 0; d < map.getMaxY(); ++d) {
			if(map.getTile(i,d).getTerrain() >= GENERATOR && map.getTile(i,d).getPlayer() == ENEMY) {
				++amount;
			}
		}
	}
	amount = rand() % amount;
	for(int i = 0; i < map.getMaxX(); ++i) {
		for(int d = 0; d < map.getMaxY(); ++d) {
			if(map.getTile(i,d).getTerrain() >= GENERATOR && map.getTile(i,d).getPlayer() == ENEMY) {
				--amount;
				if(amount < 0) {
					b[id].setPos(Location(i,d,0));
					b[id].setSeenPos(Location(i,d,0));
					b[id].setHealth(b[id].getMaxHealth());
				}
			}
		}
	}
}

void Controller::duplicate(Controller *source) {
	for(int i = 0; i < map.getMaxX(); ++i) {
		for(int d = 0; d < map.getMaxY(); ++d) {
			map.setTile(i,d,source->getMap().getTile(i,d));
		}
	}
	for(int i = 0; i < MAX_BEING; ++i) {
		b[i] = source->getBeing(i);
	}
	for(int i = 0; i < MAX_STATS; ++i) {
		stats[i] = source->getStats(i);
	}

	select = source->getSelect();		//Which being is selected
	turn = source->getTurn();		//Whose turn it is
	energy = source->getEnergy();	//Amount of Energy player has

	outcome = source->getOutcome();	//0 = Undecided, 1 = humans win, -1 = zombies win, -2 = VIP died
}

void Controller::revert() {
	for(int i = 0; i < map.getMaxX(); ++i) {
		for(int d = 0; d < map.getMaxY(); ++d) {
			map.setTile(i,d,undo->getMap().getTile(i,d));
		}
	}
	for(int i = 0; i < MAX_BEING; ++i) {
		b[i] = undo->getBeing(i);
	}
	for(int i = 0; i < MAX_STATS; ++i) {
		if(i != UNDOS)
			stats[i] = undo->getStats(i);
	}

	select = -1;		//Which being is selected
	turn = undo->getTurn();		//Whose turn it is
	energy = undo->getEnergy();	//Amount of Energy player has

	outcome = undo->getOutcome();	//0 = Undecided, 1 = humans win, -1 = zombies win, -2 = VIP died
	map.clearHighlights();

	++stats[UNDOS];
}

void Controller::issueOrder() {
	if(outcome == 0) {
		Tile t;
		if(select == -1) {
			for(int i = 0; i < MAX_BEING; ++i) {
				if(b[i].getPos().getX() == float(int(xCursor+0.5)) && b[i].getPos().getY() == float(int(yCursor+0.5))) {
					if(b[i].getType() < ZOMBIE && b[i].getTurns() > 0 && b[i].getAlive()) {
						select = i;
						map.highlightMoves(int(xCursor+0.5), int(yCursor+0.5), b[i].getTurns(), b[i].getMaxTurns());
						break;
					}
				}
			}
		} else if(select < -1) {
			undo->duplicate(this);
			if(map.getTile(int(xCursor+0.5), int(yCursor+0.5)).getPlayer() == PLAYER1) {
				if(map.getTile(int(xCursor+0.5), int(yCursor+0.5)).getOccupied() == -1 &&
					map.getTile(int(xCursor+0.5), int(yCursor+0.5)).getTerrain() == FLOOR) {
					//Create a new being at this space and expend energy
					for(int i = 0; i < MAX_BEING; ++i) {
						if(!b[i].getAlive()) {
							//We use this being
							b[i].setAlive(true);
							int x = rand() % maxFirstnames;
							int y = rand() % maxLastnames;
							string middleInitial;
							if(rand() % 20 == 0) {
								middleInitial = ". ";
								middleInitial[0] = char(rand() % 26 + 65);
							} else {
								middleInitial = "";
							}
							b[i].setName(firstnames[x] + " " + middleInitial + lastnames[y]);
							b[i].setZ(0);
							b[i].setPos(Location(float(int(xCursor+0.5)), float(int(yCursor+0.5)), rand() % 8 * 45));
							b[i].setSeenPos(b[i].getPos());
							b[i].setType(-1 - select);
							b[i].setTurns(0);
							b[i].setVisible(true);
							b[i].setHealth(b[i].getMaxHealth());
							energy += select * 10;
							stats[ENERGYSPENT] -= select * 10;
							switch(b[i].getType()) {
							case DEFENDER:
								++stats[MILITIABUILT];
								break;
							case SOLDIER:
								++stats[SOLDIERSBUILT];
								break;
							case SLAYER:
								++stats[SLAYERSBUILT];
								break;
							}
							select = -1;
							break;
						}
					}
				}
			}
		} else {
			if(b[select].getTurns() == b[select].getMaxTurns()) {
				undo->duplicate(this);
			}
			if(map.legalMove(int(b[select].getPos().getintX() + 0.5), 
					int(b[select].getPos().getY() + 0.5), int(xCursor+0.5), int(yCursor+0.5), PLAYER1, false, false)) {
				//Is there a zombie here?
				bool killed = false;
				bool stopped = false;
				if(map.getTile(int(xCursor+0.5), int(yCursor+0.5)).getOccupied() > -1) {
					//Let's kill it
					if(b[select].getType() == VIP) {
						stopped = true;
					} else {
						for(int i = 0; i < MAX_BEING; ++i) {
							if(b[i].getPos().getX() == float(int(xCursor+0.5)) && b[i].getPos().getY() == float(int(yCursor+0.5))
								&& b[i].getAlive()) {
								b[i].setHealth(b[i].getHealth() - 1);
								if(b[i].getHealth() <= 0) {
									killed = true;
									if((b[i].getType() == ZOMBIELORD || b[i].getType() == ZOMBIEKING) && !generatorsDown()) {
										saveZombieLord(i);
									} else {
										b[i].setAlive(false);
										++stats[ZOMBIEKILLS];
									}
								} else {
									stopped = true;
								}
									
							}
						}
					}
				}
				//Face the square
				float temp = 0;
				if(int(xCursor+0.5) < int(b[select].getPos().getX() + 0.5)) {
					if(int(yCursor+0.5) < int(b[select].getPos().getY() + 0.5)) {
						temp = 315;
					} else if(int(yCursor+0.5) > int(b[select].getPos().getY() + 0.5)) {
						temp = 225;
					} else {
						temp = 270;
					}

				} else if(int(xCursor+0.5) > int(b[select].getPos().getX() + 0.5)) {
					if(int(yCursor+0.5) < int(b[select].getPos().getY() + 0.5)) {
						temp = 45;
					} else if(int(yCursor+0.5) > int(b[select].getPos().getY() + 0.5)) {
						temp = 135;
					} else {
						temp = 90;
					}
				} else {
					if(int(yCursor+0.5) < int(b[select].getPos().getY() + 0.5)) {
						temp = 0;
					} else {
						temp = 180;
					}
				}
				if(map.getTile(int(xCursor+0.5), int(yCursor+0.5)).getTerrain() >= GENERATOR) {
					//Capture generators
					t = map.getTile(int(xCursor+0.5), int(yCursor+0.5));
					t.setPlayer(PLAYER1);
					map.setTile(int(xCursor+0.5), int(yCursor+0.5), t);
					b[select].setTurns(0);
					select = -1;
					map.clearHighlights();
				} else {
					//Move to the square
					if((b[select].getType() != SOLDIER || !killed) && !stopped) {
						t = map.getTile(int(b[select].getPos().getX()), int(b[select].getPos().getY()));
						map.setTile(int(b[select].getPos().getX()), int(b[select].getPos().getY()), t);

						b[select].setPos(Location(float(int(xCursor+0.5)), float(int(yCursor+0.5)), temp));
						t = map.getTile(int(xCursor+0.5), int(yCursor+0.5));
						t.setPlayer(PLAYER1);
						map.setTile(int(xCursor+0.5), int(yCursor+0.5), t);
					} else {
						b[select].setPos(Location(b[select].getPos().getX(), b[select].getPos().getY(), temp));
					}
					b[select].setTurns(b[select].getTurns() - 1);
					if(killed || stopped)
						b[select].setTurns(0);
					if(b[select].getTurns() <= 0) {
						select = -1;
						map.clearHighlights();
					} else {
						updateOccupation();
						map.highlightMoves(int(xCursor+0.5), int(yCursor+0.5), b[select].getTurns(), b[select].getMaxTurns());
					}
				}
			} else {
				if(b[select].getType() == SOLDIER && map.moveDistance(int(b[select].getPos().getX() + 0.5), 
					int(b[select].getPos().getY() + 0.5), int(xCursor+0.5), int(yCursor+0.5)) == 1) {
					//Is there a reaper on the wall?
					if(map.getTile(int(xCursor+0.5), int(yCursor+0.5)).getOccupied() == REAPER) {
						//Damage it
						for(int i = 0; i < MAX_BEING; ++i) {
							if(b[i].getPos().getX() == float(int(xCursor+0.5)) && b[i].getPos().getY() == float(int(yCursor+0.5))
								&& b[i].getAlive()) {
								b[i].setHealth(b[i].getHealth() - 1);
								if(b[i].getHealth() <= 0) {
									b[i].setAlive(false);
								}
								b[select].setTurns(0);
								select = -1;
								map.clearHighlights();
							}
						}
					}
				}
			}
		}
		updateOccupation();
	}
}

void Controller::cancelOrder() {
	if(turn == PLAYER1) {
		if(select > -1) {
			select = -1;
			map.clearHighlights();
		} else if(select < -1) {
			select = -1;
		}
	}
}

void Controller::killMap() {
	delete b;
}
