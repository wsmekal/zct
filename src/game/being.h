#ifndef BEING_H
#define BEING_H

#include <string>
#include "location.h"

class Controller;

class Being {
public:
	Being(Controller* n_control = NULL);

	Location getPos();
	Location getSeenPos();
	int getType();
	int getHealth();
	int getTurns();
	std::string getName();
	int getMaxHealth();
	int getMaxTurns();
	float getColor(int id);
	bool getAlive();
	float getZ();
	float getSeenZ();
	bool getVisible();

	void setPos(Location n_pos);
	void setSeenPos(Location n_seenPos);
	void setType(int n_type);
	void setTurns(int n_turns);
	void setHealth(int n_health);
	void setAlive(bool n_alive);
	void setZ(float n_z);
	void setSeenZ(float n_seenZ);
	void setVisible(bool n_visible);
	void setName(std::string n_name);

	void adjustPos();
private:
	Location pos;		//The game-mechanics-wise position of the piece
	float z;			//Vertical displacement (for Reapers)
	Location seenPos;	//The displayed position of the piece
	float seenZ;
	int type;

	int health;
	int turns;			//How many spaces this piece can still move
	bool visible;		//
	bool alive;			//Is this being alive?

	float color[3];
	std::string name;

	Controller *control;	//Controller associated with this piece
};

#endif
