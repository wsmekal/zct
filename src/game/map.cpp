
#include <fstream>

#include "main.h"
#include "map.h"

int Map::loadNumberFromFile(ifstream &file) {
	char buffer[32];
	file.getline( buffer, 32 );
	return atoi( buffer );
}

Map::Map() {
	t = new Tile[MAX_SIZE*MAX_SIZE];
	maxX = MAX_SIZE;
	maxY = MAX_SIZE;
}

/*Map::~Map() {
	delete t;
}*/

int Map::getMaxX() {
	return maxX;
}

int Map::getMaxY() {
	return maxY;
}

Tile Map::getTile(int x, int y) {
	if(x < 0 || y < 0 || x >= maxX || y >= maxY) {
		return Tile(FLOOR);
	}
	return t[x*MAX_SIZE+y];
}

void Map::setTile(int x, int y, Tile n_tile) {
	t[x*MAX_SIZE+y] = n_tile;
}

void Map::setMaxX(int n_maxX) {
	maxX = n_maxX;
}

void Map::setMaxY(int n_maxY) {
	maxY = n_maxY;
}

int Map::moveDistance(int x1, int y1, int x2, int y2) {
	return max(abs(x1-x2), abs(y1-y2));
}

bool Map::legalMove(int x1, int y1, int x2, int y2, int player, bool ignoreWalls, bool ignoreBeings) {
	if(x2 < 1 || y2 < 1 || x2 >= maxX-1 || y2 >= maxY-1) {
		return false;
	}
	if(moveDistance(x1, y1, x2, y2) <= 1) {
		if((getTile(x1, y1).getWall() == getTile(x2, y2).getWall() || ignoreWalls) && 
			(getTile(x2, y2).getTerrain() < GENERATOR || player != ENEMY)) {
			if(getTile(x2, y2).getOccupied() > -1 && !ignoreBeings) {
				if(player == PLAYER1 && getTile(x2, y2).getOccupied() < ZOMBIE) {
					return false;
				}
				if(player == ENEMY && getTile(x2, y2).getOccupied() >= ZOMBIE) {
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}

bool Map::getSpotted(int x, int y) {
	for(int i = x-2; i < x+3; ++i) {
		for(int d = y-2; d < y+3; ++d) {
			if(getTile(i,d).getOccupied() > -1 && getTile(i,d).getOccupied() < ZOMBIE) {
				return true;
			}
		}
	}
	return false;
}

void Map::clearHighlights() {
	for(int i = 0; i < MAX_SIZE * MAX_SIZE; ++i) {
		t[i].setHighlight(1);
		t[i].setMarked(false);
	}
}

void Map::deClearHighlights() {
	for(int i = 0; i < MAX_SIZE * MAX_SIZE; ++i) {
		t[i].setHighlight(0);
		t[i].setMarked(false);
	}
}

void Map::highlightMoves(int x, int y, int turns, int maxTurns) {
	for(int i = 0; i < MAX_SIZE * MAX_SIZE; ++i) {
		t[i].setHighlight(0.5f);
		t[i].setMarked(false);
	}
	t[x*MAX_SIZE+y].setHighlight(1);
	for(int d = 0; d < turns; ++d) {
		//Mark all places that can be moved to from highlighted squares
		for(int i = 0; i < maxX; ++i) {
			for(int j = 0; j < maxY; ++j) {
				if(getTile(i,j).getHighlight() < 1 && getTile(i,j).getTerrain() == FLOOR) {
					if((getTile(i+1,j).getHighlight() > 0.5f    && legalMove(i+1,j, i,j, PLAYER1, false, false)) ||
						(getTile(i+1,j+1).getHighlight() > 0.5f && legalMove(i+1,j+1, i,j, PLAYER1, false, false)) ||
						(getTile(i,j+1).getHighlight() > 0.5f   && legalMove(i,j+1, i,j, PLAYER1, false, false)) ||
						(getTile(i-1,j+1).getHighlight() > 0.5f && legalMove(i-1,j+1, i,j, PLAYER1, false, false)) ||
						(getTile(i-1,j).getHighlight() > 0.5f   && legalMove(i-1,j, i,j, PLAYER1, false, false)) ||
						(getTile(i-1,j-1).getHighlight() > 0.5f && legalMove(i-1,j-1, i,j, PLAYER1, false, false)) ||
						(getTile(i,j-1).getHighlight() > 0.5f   && legalMove(i,j-1, i,j, PLAYER1, false, false)) ||
						(getTile(i+1,j-1).getHighlight() > 0.5f && legalMove(i+1,j-1, i,j, PLAYER1, false, false))) {
						t[i*MAX_SIZE+j].setMarked(true);
					}
				}
			}
		}
		for(int i = 0; i < MAX_SIZE * MAX_SIZE; ++i) {
			if(t[i].getMarked()) {
				t[i].setMarked(false);
				if(d == 0 && turns == maxTurns) {
					t[i].setHighlight(1);
				} else {
					t[i].setHighlight(0.85f);
				}
			}
		}
	}
}
