
#ifndef DISPLAY_H
#define DISPLAY_H

#include "SDL.h"												// Finally: The SDL Header!

#include "main.h"
#include "controller.h"
#include "being.h"
#include "organizer.h"

class Controller;
class Being;

class Display
{
public:
	Display();
	void glEnable2D();
	void glDisable2D();

	void BuildLists();
	SDL_Surface* LoadBMP( const char *Filename );
	int LoadGLTextures();
	void BuildFont();
	void KillFont();
	void KillParticles();
	void glPrint( GLint x, GLint y, GLfloat scale, bool set, bool center, const char *string, ... );
	bool InitGL( SDL_Surface *S );

	float getRotate();
	float getXCamera();
	float getYCamera();
	bool getDisplayHelp()  { return displayHelp; }

	void moveCamera( float x, float y );
	void setCamera( float x, float y );
	void setSeenCamera( float x, float y );
	void pivotCamera( float rot );
	void tiltCamera( float rot );
	void zoomInOut(float change);
  void toggleDisplayHelp();

	void drawTile( Controller *c, int x, int y );
	void draw( Controller c );
	void draw( Being b, bool selected );
	void drawMenu(Organizer o);  //!< draw menu to screen
	void drawStats(Controller c);  //!< draw end game statistics to screen
	void drawHelp();  //!< draw quick help to screen

private:
	GLUquadricObj *quadratic;

	int list;
	GLuint texture[NUM_TEXTURES];
	GLuint base;			               		 // Base Display List For The Font
	float xCamera, yCamera, tilt;
	float xTarget, yTarget, targetTilt;	 //Target positions of camera
	float lightposition[4];
	float lightDiffuse[4];

	float rotate, targetRotate;
	float zoom;
	float cursorSpin;
	float subCursorHeight;
	bool showDamage;
	bool displayHelp;
};

#endif // DISPLAY_H
