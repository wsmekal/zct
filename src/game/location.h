#ifndef LOCATION_H
#define LOCATION_H

#include "SDL_opengl.h"

#include "point.h"

class Location : public point2d {
	public:
		Location();				//Constructors
		Location(point2d);
		Location(GLfloat, GLfloat, GLfloat);

		GLfloat getRot();		//Accessors

		void setRot(GLfloat);	//Mutators

		void pivot(point2d, GLfloat, bool);		//Rotate around point
		void facePoint(point2d);					//Rot faces point
	private:
		GLfloat rot;
};

#endif
