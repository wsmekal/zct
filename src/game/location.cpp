
#include <cmath>

#include "location.h"

Location::Location() {
	point2d::point2d();
	rot = 0.0f;
}

Location::Location(point2d pt) {
	x = pt.getX();
	y = pt.getY();
	rot = 0.0f;
}

Location::Location(GLfloat n_x, GLfloat n_y, GLfloat n_rot) {
	x = n_x;
	y = n_y;
	rot = n_rot;
}

GLfloat Location::getRot() {
	return rot;
}

void Location::setRot(GLfloat n_rot) {
	rot = n_rot;
}

void Location::pivot(point2d pt, GLfloat degrees, bool adjustRot) {
	point2d::pivot(pt, degrees);
	if(adjustRot)
		rot += degrees;
}

void Location::facePoint(point2d pt) {
	if(!(equals(pt))) {
		GLfloat xNew, yNew;
		xNew = x - pt.getX();
		yNew = -(y - pt.getY());

		double n_rot = -atan(GLfloat(yNew) / GLfloat(xNew));	//Get the Angle & distance
		n_rot -= M_PI / 2.0f;
		if(xNew < 0.0f) n_rot += M_PI;
		if(n_rot < 0.0) n_rot += M_PI * 2.0f;

		rot = GLfloat(n_rot);
		rot *= 180.0f / M_PI;
	}
}
