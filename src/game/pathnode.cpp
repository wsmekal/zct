
#include <cmath>

#include "main.h"
#include "pathnode.h"

PathNode::PathNode()
{
	point2d::point2d();
	g = 0;
	parent = NULL;
}

PathNode::PathNode(point2d pt)
{
	x = pt.getX();
	y = pt.getY();
	g = 0;
	parent = NULL;
}

PathNode::PathNode(float x2, float y2, PathNode *n_parent)
{
	x = x2;
	y = y2;
	g = 1+n_parent->getG();
	parent = n_parent;
}

PathNode *PathNode::getParent()
{
	return parent;
}

float PathNode::getG()
{
	return g;
}

float PathNode::getF(float x2, float y2)
{
	float dist = float(sqrt(pow(double(x - x2),2) + pow(double(y - y2),2)));
	return dist + g;
}
