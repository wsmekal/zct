#include "organizer.h"
#include "main.h"

int Organizer::loadNumberFromFile(ifstream &file) {
	char buffer[32];
	file.getline(buffer, 32);
	return atoi(buffer);
}

Organizer::Organizer() {
	select = -1;
	for(int i = 0; i < MAX_MAP; ++i) {
		timeRecord[i] = 0;
		mapName[i] = "";
	}
	numMaps = 0;
	numCleared = 0;
	xCursor = 0;
	yCursor = 0;
}

string Organizer::getMapName(int id) {
	return mapName[id];
}

bool Organizer::getCleared(int id) {
	if(timeRecord[id] == 0)
		return false;
	return true;
}

int Organizer::getTimeRecord(int id) {
	return timeRecord[id];
}

int Organizer::getSelect() {
	return select;
}

int Organizer::getNumMaps() {
	return numMaps;
}

float Organizer::getXCursor() {
	return xCursor;
}

float Organizer::getYCursor() {
	return yCursor;
}

int Organizer::toUnlock(int id) {
	if(id < 10) {
		return 0;
	} else if(id < 20) {
		return 6 - numCleared;
	} else if(id < 30) {
		return 13 - numCleared;
	} else if(id < 40) {
		return 20 - numCleared;
	} else if(id < 50) {
		return 28 - numCleared;
	} else if(id < 60) {
		return 37 - numCleared;
	} else if(id < 70) {
		return 46 - numCleared;
	} else if(id < 80) {
		return 56 - numCleared;
	} else if(id < 90) {
		return 67 - numCleared;
	} else if(id < 99) {
		return 78 - numCleared;
	} else {
		return id - numCleared;
	}
}


void Organizer::setTimeRecord(int id, int n_timeRecord) {
	if(n_timeRecord > 0 && timeRecord[id] == 0)
		++numCleared;
	timeRecord[id] = n_timeRecord;
}

void Organizer::setSelect(int n_select) {
	select = n_select;
}


void Organizer::moveCursor(int x, int y) {
	xCursor = x;
	yCursor = 480-y;
	//Select a tile, if applicable
	if(x >= 40 && x < 276 && y > 110 && y < 347) {
		int xPos = (x - 40) / 24;
		int yPos = (y - 110) / 24;
		select = xPos + 10*yPos;
	} else {
		select = -1;
	}
}

void Organizer::load() {
	char buffer2[128];

	ifstream file;
	file.open(formatString("%s/data/maps/all.dat", dataPath.c_str()));
	if(!file) {
#if WIN32
		MessageBox(NULL,"ALL.DAT could not be opened.","Initialization Error", MB_OK | MB_ICONINFORMATION);
#else
		fprintf( stderr, "Initialization Error: ALL.DAT could not be opened." );
#endif
	}

	numMaps = loadNumberFromFile(file);
	for(int i = 0; i < numMaps; ++i) {
		file.getline(buffer2, 128);
		mapName[i] = string(buffer2);
		timeRecord[i] = loadNumberFromFile(file);
		if(timeRecord[i] == 1)
			//It's from the old file type, let's reset it
			timeRecord[i] = 999;
		if(timeRecord[i] > 0)
			++numCleared;
	}
	file.close();
}

void Organizer::save() {
	ofstream file;
	file.open(formatString("%s/data/maps/all.dat", dataPath.c_str()));

	file << numMaps << endl;

	for(int i = 0; i < numMaps; ++i) {
		file << mapName[i].c_str() << endl << timeRecord[i] << endl;
	}
	file.close();
}
