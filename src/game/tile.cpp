

#include "main.h"
#include "tile.h"

Tile::Tile(int n_terrain) {
	terrain = n_terrain;
	player = ENEMY;
	marked = false;
	highlight = true;
	target = false;
	occupied = -1;
}


bool Tile::getWall() {
	if(terrain == WALL)
		return true;
	return false;
}

int Tile::getTerrain() {
	return terrain;
}

int Tile::getPlayer() {
	return player;
}

bool Tile::getHighlight() {
	return highlight;
}

bool Tile::getMarked() {
	return marked;
}

bool Tile::getTarget() {
	return target;
}

int Tile::getOccupied() {
	return occupied;
}


void Tile::setTerrain(int n_terrain) {
	terrain = n_terrain;
}

void Tile::setPlayer(int n_player) {
	player = n_player;
}

void Tile::setHighlight(bool n_highlight) {
	highlight = n_highlight;
}

void Tile::setMarked(bool n_marked) {
	marked = n_marked;
}

void Tile::setTarget(bool n_target) {
	target = n_target;
}

void Tile::setOccupied(int n_occupied) {
	occupied = n_occupied;
}
