
// standard headers
#include <cmath>

// 3rd party headers
#include "SDL_opengl.h"

// own headers
#include "main.h"
#include "display.h"


/*!
 * Setting default values to variables.
 */
Display::Display()
{
	rotate = 225;
	tilt = 30;
	zoom = 20;
	targetRotate = 225;
	targetTilt = 30;
	cursorSpin = 0;
	subCursorHeight = 0;

	xCamera = 0;
	yCamera = 0;
	xTarget = 0;
	yTarget = 0;

	lightposition[0] = -1.0f;
	lightposition[1] =  0.0f;
	lightposition[2] = -5.0f;
	lightposition[3] =  1.0f;
	lightDiffuse[0] = 1.0f;
	lightDiffuse[1] = 1.0f;
	lightDiffuse[2] = 1.0f;
	lightDiffuse[3] = 1.0f;

	displayHelp=false;
}


/*!
 * Enable OpenGL 2d mode.
 */
void Display::glEnable2D()
{
	int vPort[4];

	glGetIntegerv ( GL_VIEWPORT, vPort );

	glMatrixMode ( GL_PROJECTION );
	glPushMatrix();
	glLoadIdentity();

	glOrtho ( 0, vPort[2], 0, vPort[3], -1, 1 );
	glMatrixMode ( GL_MODELVIEW );
	glPushMatrix();
	glLoadIdentity();
}


/*!
 * Disable OpenGL 2d mode.
 */
void Display::glDisable2D()
{
	glMatrixMode ( GL_PROJECTION );
	glPopMatrix();
	glMatrixMode ( GL_MODELVIEW );
	glPopMatrix();
}


/*!
 * Build list of entities (wall, floor, cursors, beings, generator,
 * menu items).
 */
void Display::BuildLists()
{
	list=glGenLists ( 9 );

	// List = Floor
	glNewList ( list, GL_COMPILE );
	glBegin ( GL_QUADS );
	glNormal3f ( 0.0f, 0.0f, 1.0f );
	glVertex3f ( -0.5f,-0.5f, 0.5f );
	glVertex3f ( -0.5f, 0.5f, 0.5f );
	glVertex3f ( 0.5f, 0.5f, 0.5f );
	glVertex3f ( 0.5f,-0.5f, 0.5f );
	glEnd();
	glEndList();

	// List+1 = Wall
	glNewList ( list+1, GL_COMPILE );					
	glBegin ( GL_QUADS );
	glNormal3f ( 0.0f, 0.0f, 1.0f );
	glVertex3f ( -0.5f,-0.5f, 0.5f );
	glVertex3f ( -0.5f, 1.5f, 0.5f );
	glVertex3f ( 0.5f, 1.5f, 0.5f );
	glVertex3f ( 0.5f,-0.5f, 0.5f );
	glEnd();
	glEndList();

	// List+2 = Particle
	glNewList ( list+2, GL_COMPILE );
	glBindTexture ( GL_TEXTURE_2D, texture[1] );
	glBegin ( GL_TRIANGLE_STRIP );
	glNormal3f ( 0.0f, 0.0f, 1.0f );
	glTexCoord2f ( 0,1 );
	glVertex3f ( -0.7f, 0.7f, 0.0f );
	glTexCoord2f ( 1,1 );
	glVertex3f ( 0.7f, 0.7f, 0.0f );
	glTexCoord2f ( 0,0 );
	glVertex3f ( -0.7f,-0.7f, 0.0f );
	glTexCoord2f ( 1,0 );
	glVertex3f ( 0.7f,-0.7f, 0.0f );
	glEnd();
	glEndList();

	// Being
	glNewList ( list+3, GL_COMPILE );
	glBegin ( GL_TRIANGLES );				
	glNormal3f ( 0.577f, 0.577f, 0.577f );
	glVertex3f ( 0.0f, 0.0f, 1.2f );
	glVertex3f ( 0.3f, 0.0f, 0.9f );
	glVertex3f ( 0.0f, 0.3f, 0.9f );

	glNormal3f ( -0.577f, 0.577f, 0.577f );
	glVertex3f ( 0.0f, 0.0f, 1.2f );
	glVertex3f ( -0.3f, 0.0f, 0.9f );
	glVertex3f ( 0.0f, 0.3f, 0.9f );

	glNormal3f ( 0.577f, 0.577f,-0.577f );
	glVertex3f ( 0.0f, 0.0f, 0.6f );
	glVertex3f ( 0.3f, 0.0f, 0.9f );
	glVertex3f ( 0.0f, 0.3f, 0.9f );

	glNormal3f ( -0.577f, 0.577f,-0.577f );
	glVertex3f ( 0.0f, 0.0f, 0.6f );
	glVertex3f ( -0.3f, 0.0f, 0.9f );
	glVertex3f ( 0.0f, 0.3f, 0.9f );

	glNormal3f ( 0.0f,-1.0f, 0.0f );
	glVertex3f ( 0.0f, 0.0f, 1.2f );
	glVertex3f ( 0.3f, 0.0f, 0.9f );
	glVertex3f ( 0.0f, 0.0f, 0.6f );

	glNormal3f ( 0.0f,-1.0f, 0.0f );
	glVertex3f ( 0.0f, 0.0f, 1.2f );
	glVertex3f ( -0.3f, 0.0f, 0.9f );
	glVertex3f ( 0.0f, 0.0f, 0.6f );

	glNormal3f ( 0.667f, 0.667f, 0.333f );
	glVertex3f ( 0.0f, 0.0f, 0.9f );
	glVertex3f ( 0.3f, 0.0f, 0.0f );
	glVertex3f ( 0.0f, 0.3f, 0.0f );

	glNormal3f ( 0.667f,-0.667f, 0.333f );
	glVertex3f ( 0.0f, 0.0f, 0.9f );
	glVertex3f ( 0.3f, 0.0f, 0.0f );
	glVertex3f ( 0.0f,-0.3f, 0.0f );

	glNormal3f ( -0.667f, 0.667f, 0.333f );
	glVertex3f ( 0.0f, 0.0f, 0.9f );
	glVertex3f ( -0.3f, 0.0f, 0.0f );
	glVertex3f ( 0.0f, 0.3f, 0.0f );

	glNormal3f ( -0.667f,-0.667f, 0.333f );
	glVertex3f ( 0.0f, 0.0f, 0.9f );
	glVertex3f ( -0.3f, 0.0f, 0.0f );
	glVertex3f ( 0.0f,-0.3f, 0.0f );

	glNormal3f ( 0.0f, 0.0f,-1.0f );
	glVertex3f ( 0.0f, 0.3f, 0.0f );
	glVertex3f ( 0.3f, 0.0f, 0.0f );
	glVertex3f ( -0.3f, 0.0f, 0.0f );

	glNormal3f ( 0.0f, 0.0f,-1.0f );
	glVertex3f ( 0.0f,-0.3f, 0.0f );
	glVertex3f ( 0.3f, 0.0f, 0.0f );
	glVertex3f ( -0.3f, 0.0f, 0.0f );
	glEnd();
	glEndList();
	glNewList ( list+4, GL_COMPILE );
	glColor3f ( 1.0f, 1.0f, 1.0f );

	glBegin ( GL_LINES );							//List+4 = cursor
	glVertex3f ( 0.0f, 0.2f, 0.0f );
	glVertex3f ( 0.0f, 0.4f, 0.0f );
	glVertex3f ( 0.0f,-0.2f, 0.0f );
	glVertex3f ( 0.0f,-0.4f, 0.0f );
	glVertex3f ( 0.2f, 0.0f, 0.0f );
	glVertex3f ( 0.4f, 0.0f, 0.0f );
	glVertex3f ( -0.2f, 0.0f, 0.0f );
	glVertex3f ( -0.4f, 0.0f, 0.0f );
	glEnd();
	gluDisk ( quadratic, 0.27f, 0.3f, 12, 1 );
	glEndList();
	glNewList ( list+5, GL_COMPILE );
	glBegin ( GL_LINES );

	glNormal3f ( 0.0f, 0.0f, 1.0f );			//List+5 = subCursor
	glVertex3f ( -0.5f,-0.5f, 0.5f );
	glVertex3f ( -0.5f, 0.5f, 0.5f );

	glVertex3f ( -0.5f, 0.5f, 0.5f );
	glVertex3f ( 0.5f, 0.5f, 0.5f );

	glVertex3f ( 0.5f, 0.5f, 0.5f );
	glVertex3f ( 0.5f,-0.5f, 0.5f );

	glVertex3f ( 0.5f,-0.5f, 0.5f );
	glVertex3f ( -0.5f,-0.5f, 0.5f );
	glEnd();
	glEndList();

	glNewList ( list+6, GL_COMPILE );					//List+6 = unmoved cursor
	glBegin ( GL_LINES );
	glVertex3f ( 0.0f, 0.0f, 1.4f );
	glVertex3f ( 0.2f, 0.0f, 1.6f );

	glVertex3f ( 0.2f, 0.0f, 1.6f );
	glVertex3f ( -0.2f, 0.0f, 1.6f );

	glVertex3f ( -0.2f, 0.0f, 1.6f );
	glVertex3f ( 0.0f, 0.0f, 1.4f );
	glEnd();
	glEndList();

	glNewList ( list+7, GL_COMPILE );					//List+7 = Generator
	glBegin ( GL_QUADS );
	glNormal3f ( 0.0f, 0.7f, 0.3f );	//North face
	glVertex3f ( 0.4f, 0.4f, 0.0f );
	glVertex3f ( 0.2f, 0.2f, 2.8f );
	glVertex3f ( -0.2f, 0.2f, 2.8f );
	glVertex3f ( -0.4f, 0.4f, 0.0f );

	glNormal3f ( 0.0f,-0.7f, 0.3f );	//South face
	glVertex3f ( 0.4f,-0.4f, 0.0f );
	glVertex3f ( 0.2f,-0.2f, 2.8f );
	glVertex3f ( -0.2f,-0.2f, 2.8f );
	glVertex3f ( -0.4f,-0.4f, 0.0f );

	glNormal3f ( 0.7f, 0.0f, 0.3f );	//East face
	glVertex3f ( 0.4f,-0.4f, 0.0f );
	glVertex3f ( 0.2f,-0.2f, 2.8f );
	glVertex3f ( 0.2f, 0.2f, 2.8f );
	glVertex3f ( 0.4f, 0.4f, 0.0f );

	glNormal3f ( -0.7f, 0.0f, 0.3f );	//West face
	glVertex3f ( -0.4f,-0.4f, 0.0f );
	glVertex3f ( -0.2f,-0.2f, 2.8f );
	glVertex3f ( -0.2f, 0.2f, 2.8f );
	glVertex3f ( -0.4f, 0.4f, 0.0f );

	glNormal3f ( 0.0f, 0.0f, 1.0f );	//Top
	glVertex3f ( 0.2f,-0.2f, 2.8f );
	glVertex3f ( -0.2f,-0.2f, 2.8f );
	glVertex3f ( -0.2f, 0.2f, 2.8f );
	glVertex3f ( 0.2f, 0.2f, 2.8f );
	glEnd();
	glEndList();

	glNewList ( list+8, GL_COMPILE );					//List+8 = menu tile
	glBegin ( GL_QUADS );
	glVertex2d ( -10,-10 );
	glVertex2d ( -10, 10 );
	glVertex2d ( 10, 10 );
	glVertex2d ( 10,-10 );
	glEnd();
	glEndList();

	glNewList ( list+9, GL_COMPILE );					//List+9 = menu cursor
	glBegin ( GL_LINES );
	glVertex2d ( -5, 0 );
	glVertex2d ( 6, 0 );
	glVertex2d ( 0,-5 );
	glVertex2d ( 0, 6 );
	glEnd();
	glEndList();
}


/*! 
 * Loads a bitmap image from a file.
 *
 * @param Filename : filename to bitmap image
 * @return pointer to SDL_Surface
 */
SDL_Surface* Display::LoadBMP(const char* Filename)
{
	FILE* File=NULL;									// File Handle

	if(!Filename)										// Make Sure A Filename Was Given
		return NULL;									// If Not Return NULL

	File=fopen(Filename,"r");				// Check To See If The File Exists

	if(File) {  											// Does The File Exist?
		fclose( File );									// Close The Handle
		return SDL_LoadBMP(Filename);
	}

	return NULL;										// If Load Failed Return NULL
}


/*! 
 * Loads a bitmap image from a file.
 *
 * @return pointer to SDL_Surface
 */
int Display::LoadGLTextures()									// Load Bitmaps And Convert To Textures
{
	bool Status=false;									// Status Indicator

	SDL_Surface* TextureImage[NUM_TEXTURES];					// Create Storage Space For The Texture
	memset ( TextureImage, 0, sizeof ( void * ) *NUM_TEXTURES );           	// Set The Pointer To NULL

	// Load The Bitmap, Check For Errors, If Bitmap's Not Found Quit
	if ( ( TextureImage[0]=LoadBMP (formatString("%s/data/font.bmp", dataPath.c_str())) ) )			// Load Block Texture
	{
		Status=true;                                    // Set The Status To TRUE
		glGenTextures ( NUM_TEXTURES, &texture[0] );               // Create One Textures

		for ( int loop1=0; loop1<NUM_TEXTURES; loop1++ )			// Loop Through 5 Textures
		{
			glBindTexture ( GL_TEXTURE_2D, texture[loop1] );
			glTexImage2D ( GL_TEXTURE_2D, 0, 3, TextureImage[loop1]->w, TextureImage[loop1]->h, 0,
			               GL_RGB, GL_UNSIGNED_BYTE, TextureImage[loop1]->pixels );
			glTexParameteri ( GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST );
			glTexParameteri ( GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST );
			//change to texture matrix and do the trick
			glMatrixMode ( GL_TEXTURE );
			glRotatef ( 180.0f,0.0f,0.0f,1.0f );
			glScalef ( -1.0f,1.0f,1.0f );

			//back to normal
			glMatrixMode ( GL_MODELVIEW );
		}
		for ( int loop1=0; loop1<NUM_TEXTURES; loop1++ )				// Loop Through 5 Textures
			if ( TextureImage[loop1] )			// If Texture Exists
				SDL_FreeSurface ( TextureImage[loop1] );		// Free The Image Structure
	}
	return Status;										// Return The Status
}

void Display::BuildFont()								// Build Our Font Display List
{
	float	cx;											// Holds Our X Character Coord
	float	cy;											// Holds Our Y Character Coord

	base=glGenLists ( 256 );								// Creating 256 Display Lists
	glBindTexture ( GL_TEXTURE_2D, texture[0] );			// Select Our Font Texture
	for ( int loop=0; loop<256; loop++ )  						// Loop Through All 256 Lists
	{
		cx=float ( loop%16 ) /16.0f;						// X Position Of Current Character
		cy=float ( loop/16 ) /16.0f;						// Y Position Of Current Character

		glNewList ( base+loop,GL_COMPILE );				// Start Building A List
		glBegin ( GL_QUADS );							// Use A Quad For Each Character
		glTexCoord2f ( cx,1-cy-0.0625f );	// Texture Coord (Bottom Left)
		glVertex2i ( 0,0 );						// Vertex Coord (Bottom Left)
		glTexCoord2f ( cx+0.065f,1-cy-0.0625f );	// Texture Coord (Bottom Right)
		glVertex2i ( 16,0 );						// Vertex Coord (Bottom Right)
		glTexCoord2f ( cx+0.065f,1-cy );			// Texture Coord (Top Right)
		glVertex2i ( 16,16 );						// Vertex Coord (Top Right)
		glTexCoord2f ( cx,1-cy );			// Texture Coord (Top Left)
		glVertex2i ( 0,16 );						// Vertex Coord (Top Left)
		glEnd();									// Done Building Our Quad (Character)
		glTranslated ( 10,0,0 );						// Move To The Right Of The Character
		glEndList();									// Done Building The Display List
	}													// Loop Until All 256 Are Built
}

void Display::KillFont()									// Delete The Font From Memory
{
	glDeleteLists ( base, 256 );							// Delete All 256 Display Lists
}

void Display::glPrint ( GLint x, GLint y, float scale, bool set, bool center, const char *string, ... )	// Where The Printing Happens
{
	char text[256];				// Holds Our String
	int font_set=0;
	if ( set )
		font_set=1;

	va_list	ap;										// Pointer To List Of Arguments
	va_start ( ap, string );							// Parses The String For Variables
	vsprintf ( text, string, ap );					// And Converts Symbols To Actual Numbers
	va_end ( ap );										// Results Are Stored In Text

	glBindTexture ( GL_TEXTURE_2D, texture[0] );			// Select Our Font Texture
	glDisable ( GL_DEPTH_TEST );							// Disables Depth Testing
	glMatrixMode ( GL_PROJECTION );						// Select The Projection Matrix
	glPushMatrix();										// Store The Projection Matrix
	glLoadIdentity();									// Reset The Projection Matrix
	glOrtho ( 0, 640, 0, 480, -1, 1 );							// Set Up An Ortho Screen
	glMatrixMode ( GL_MODELVIEW );							// Select The Modelview Matrix
	glPushMatrix();										// Store The Modelview Matrix
	glLoadIdentity();									// Reset The Modelview Matrix
	if( center )
		glTranslated ( x - strlen ( text ) * 5 * scale,y,0 );	// Position The Text (0,0 - Bottom Left)
	else
		glTranslated ( x,y,0 );							// Position The Text (0,0 - Bottom Left)
	glScalef ( scale, scale, scale );
	glListBase ( base-32+ ( 128*font_set ) );						// Choose The Font Set (0 or 1)
	glCallLists ( strlen ( text ),GL_BYTE,text );				// Write The Text To The Screen
	glMatrixMode ( GL_PROJECTION );						// Select The Projection Matrix
	glPopMatrix();										// Restore The Old Projection Matrix
	glMatrixMode ( GL_MODELVIEW );							// Select The Modelview Matrix
	glPopMatrix();										// Restore The Old Projection Matrix
	glEnable ( GL_DEPTH_TEST );
}

bool Display::InitGL ( SDL_Surface *S )										// Any OpenGL Initialization Code Goes Here
{
	srand ( SDL_GetTicks() );

	if ( !LoadGLTextures() )
		return false;
	BuildFont();
	glClearColor ( 0.0f, 0.0f, 0.0f, 0.5f );				// Black Background
	glClearDepth ( 1.0f );									// Depth Buffer Setup
	quadratic=gluNewQuadric();							// Create A Pointer To The Quadric Object
	gluQuadricNormals ( quadratic, GLU_SMOOTH );			// Create Smooth Normals
	gluQuadricTexture ( quadratic, GL_TRUE );				// Create Texture Coords
	gluQuadricOrientation ( quadratic, GLU_INSIDE );
	glDepthFunc ( GL_LEQUAL );								// The Type Of Depth Testing To Do
	glEnable ( GL_DEPTH_TEST );							// Enables Depth Testing
	glEnable ( GL_COLOR_MATERIAL );
	glBlendFunc ( GL_SRC_ALPHA,GL_ONE );					// Select The Type Of Blending
	glEnable ( GL_BLEND );
	glLightfv ( GL_LIGHT1, GL_DIFFUSE, lightDiffuse );
	glLightfv ( GL_LIGHT1, GL_POSITION, lightposition );
	glEnable ( GL_LIGHT1 );
	glEnable ( GL_LIGHTING );
	glShadeModel ( GL_SMOOTH );							// Enables Smooth Color Shading
	glHint ( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );	// Really Nice Perspective Calculations
	glDisable ( GL_TEXTURE_2D );
	BuildLists();
	return true;										// Initialization Went OK
}


float Display::getRotate()
{
	return rotate;
}

float Display::getXCamera()
{
	return xCamera;
}

float Display::getYCamera()
{
	return yCamera;
}


void Display::moveCamera ( float x, float y )
{
	point2d temp = point2d ( x,y );
	temp.pivot ( point2d ( 0,0 ), ( ( rotate - 45 ) / 180 * M_PI ) );
	xTarget += temp.getX();
	yTarget += temp.getY();
}


void Display::zoomInOut( float change )
{
	zoom += change;
	if(zoom>30.0f)
		zoom=30.0f;
	else if(zoom<10.0f)
		zoom=10.0f;
}

void Display::setCamera ( float x, float y )
{
	xTarget = x;
	yTarget = y;
}

void Display::setSeenCamera ( float x, float y )
{
	xCamera = x;
	yCamera = y;
}


/*!
 * Toggle the displayHelp variable. If true a help window
 * with the keyboard commands is shown over the current display.
 */
void Display::toggleDisplayHelp()
{
	displayHelp = !displayHelp;
}



void Display::pivotCamera ( float rot )
{
	targetRotate += rot;
}


void Display::tiltCamera ( float rot )
{
	targetTilt -= rot;
	if ( targetTilt < 0 ) targetTilt = 0;
	if ( targetTilt > 45 ) targetTilt = 45;
}

void Display::drawTile ( Controller *c, int x, int y )
{
	Map m = c->getMap();
	
	if ( m.getTile ( x,y ).getTerrain() == WALL ) {  // tile is wall
		glTranslatef ( 0.0f, 0.0f, 1.5f );
		glColor3f ( 0.7f, 0.7f, 0.7f );
		
	} else if ( m.getTile ( x,y ).getTerrain() == FLOOR ) {  // tile is floor
		glTranslatef ( 0.0f, 0.0f, -0.5f );
		
		switch ( m.getTile ( x,y ).getPlayer() )
		{
			case ENEMY:
				if ( m.getTile ( x,y ).getTarget() )
					glColor3f ( 0.5f * m.getTile ( x,y ).getHighlight() + 0.25 * sin ( cursorSpin * 0.03f ), 0.5f * m.getTile ( x,y ).getHighlight() + 0.25 * sin ( cursorSpin * 0.03f ), 0.5f * m.getTile ( x,y ).getHighlight() + 0.25 * sin ( cursorSpin * 0.03f ) );
				else
					glColor3f ( 0.5f * m.getTile ( x,y ).getHighlight(), 0.5f * m.getTile ( x,y ).getHighlight(), 0.5f * m.getTile ( x,y ).getHighlight() );
				break;
			case PLAYER1:
				if ( m.getTile ( x,y ).getTarget() )
					glColor3f ( 0.3f * m.getTile ( x,y ).getHighlight() + 0.15 * sin ( cursorSpin * 0.03f ), 0.3f * m.getTile ( x,y ).getHighlight() + 0.15 * sin ( cursorSpin * 0.03f ), 0.8f * m.getTile ( x,y ).getHighlight() + 0.4 * sin ( cursorSpin * 0.03f ) );
				else
					glColor3f ( 0.3f * m.getTile ( x,y ).getHighlight(), 0.3f * m.getTile ( x,y ).getHighlight(), 0.8f * m.getTile ( x,y ).getHighlight() );
				break;
		}
		
	} else if ( m.getTile ( x,y ).getTerrain() >= GENERATOR ) {  // entity
		glTranslatef ( 0.0f, 0.0f,-0.5f );
		
		//Switch to color for generator
		if ( m.getTile ( x,y ).getPlayer() == PLAYER1 )
			glColor3f ( 0.0f, 0.0f, m.getTile ( x,y ).getHighlight() );
		else {
			switch ( m.getTile ( x,y ).getTerrain() )
			{
				case GENERATOR:
					glColor3f ( 0.0f, 0.7f * m.getTile ( x,y ).getHighlight(), 0.0f );
					break;
				case GENERATOR_SMART:
					glColor3f ( 0.0f, m.getTile ( x,y ).getHighlight(), 0.0f );
					break;
				case GENERATOR_FAST:
					glColor3f ( 0.5f * m.getTile ( x,y ).getHighlight(), 1.0f * m.getTile ( x,y ).getHighlight(), 0.5f * m.getTile ( x,y ).getHighlight() );
					break;
				case GENERATOR_BIG:
					glColor3f ( 0.0f, 0.5f * m.getTile ( x,y ).getHighlight(), 0.0f );
					break;
				case GENERATOR_REAPER:
					glColor3f ( m.getTile ( x,y ).getHighlight(), 0.0f, m.getTile ( x,y ).getHighlight() );
					break;
				case GENERATOR_STEALTH:
					glColor3f ( 0.3f * m.getTile ( x,y ).getHighlight(), 0.0f, 0.3f * m.getTile ( x,y ).getHighlight() );
					break;
			}
		}
		glCallList ( list+7 );
		
		//Then, switch to color for floor
		switch ( m.getTile ( x,y ).getPlayer() )
		{
			case ENEMY:
				glColor3f ( 0.5f * m.getTile ( x,y ).getHighlight(), 0.5f * m.getTile ( x,y ).getHighlight(), 0.5f * m.getTile ( x,y ).getHighlight() );
				break;
			case PLAYER1:
				glColor3f ( 0.3f * m.getTile ( x,y ).getHighlight(), 0.3f * m.getTile ( x,y ).getHighlight(), 0.8f * m.getTile ( x,y ).getHighlight() );
				break;
		}
	}
	glCallList ( list );		//Render floor/cieling

	glColor3f ( 0.4f, 0.4f, 0.4f );
	if ( m.getTile ( x,y ).getWall() )
	{
		glRotatef ( -90.0f, 1.0f, 0.0f, 0.0f );
		if ( y == MAX_SIZE-1 || !m.getTile ( x,min ( y+1, MAX_SIZE-1 ) ).getWall() )
		{
			glCallList ( list+1 );					//Draw the north wall
		}
		glRotatef ( 90.0f, 0.0f, 1.0f, 0.0f );
		if ( x == MAX_SIZE-1 || !m.getTile ( min ( x+1, MAX_SIZE-1 ), y ).getWall() )
		{
			glCallList ( list+1 );					//Draw the west wall
		}
		glRotatef ( 90.0f, 0.0f, 1.0f, 0.0f );
		if ( y == 0 || !m.getTile ( x, max ( y-1,0 ) ).getWall() )
		{
			glCallList ( list+1 );					//Draw the south wall
		}
		glRotatef ( 90.0f, 0.0f, 1.0f, 0.0f );
		if ( x == 0 || !m.getTile ( max ( x-1,0 ),y ).getWall() )
		{
			glCallList ( list+1 );					//Draw the east wall
		}
	}
}


void Display::draw (Being b, bool selected)
{
	glLoadIdentity();
	glTranslatef ( 0.0f, 0.0f,-zoom );
	glRotatef ( -tilt, 1.0f, 0.0f, 0.0f );
	glRotatef ( rotate, 0.0f, 0.0f, 1.0f );
	glTranslatef ( b.getSeenPos().getX() - xCamera, b.getSeenPos().getY() - yCamera, b.getSeenZ() );

	switch ( b.getType() )
	{
		case FASTZOMBIE:
			glScalef ( 0.8f, 0.8f, 0.8f );
			break;
		case BIGZOMBIE:
			glScalef ( 1.2f, 1.2f, 1.2f );
			break;
		case ZOMBIELORD:
			glScalef ( 1.2f, 1.2f, 1.4f );
			break;
		case ZOMBIEKING:
			glScalef ( 1.3f, 1.3f, 1.6f );
			break;
	}
	glRotatef ( b.getSeenPos().getRot() + 180, 0.0f, 0.0f, 1.0f );
	if ( selected )
		glColor3f ( b.getColor ( 0 ) * ( sin ( cursorSpin*10 ) *0.35+1 ), b.getColor ( 1 ) * ( sin ( cursorSpin*10 ) *0.35+1 ), b.getColor ( 2 ) * ( sin ( cursorSpin*10 ) *0.35+1 ) );
	else
		glColor3f ( b.getColor ( 0 ), b.getColor ( 1 ), b.getColor ( 2 ) );

	glCallList ( list+3 );
	if ( b.getTurns() > 0 && b.getType() < ZOMBIE )
	{
		glDisable ( GL_LIGHTING );
		glColor3f ( 1, 1, 1 );
		glRotatef ( cursorSpin*0.5f, 0.0f, 0.0f, 1.0f );
		if ( selected )
			glRotatef ( cursorSpin*1.5f, 0.0f, 0.0f, 1.0f );
		glCallList ( list+6 );
		glEnable ( GL_LIGHTING );
	}
}


void Display::draw ( Controller c )
{
	int radius = int ( zoom ) + 11;
	glClearColor ( 0.0f, 0.0f, 0.0f, 0.5f );
	int centerX = int ( xCamera );
	int centerY = int ( yCamera );

	//Loop through every tile
	for ( int i = max ( centerX - radius + 2, 0 ); i < min ( centerX + radius + 2, c.getMap().getMaxX() ); ++i )
	{
		for ( int d = max ( centerY - radius + 2, 0 ); d < min ( centerY + radius + 2, c.getMap().getMaxY() ); ++d )
		{
			//Render the tile
			glLoadIdentity();
			glTranslatef ( 0.0f, 0.0f, -zoom );
			glRotatef ( -tilt, 1.0f, 0.0f, 0.0f );
			glRotatef ( rotate, 0.0f, 0.0f, 1.0f );
			glTranslatef ( float ( i ) - xCamera, float ( d ) - yCamera, 0.0f );
			drawTile ( &c, i, d );

			if ( int ( c.getXCursor() +0.5 ) == i && int ( c.getYCursor() +0.5 ) == d && c.getTurn() == PLAYER1 )
			{
				//Draw the subcursor
				glLoadIdentity();
				glTranslatef ( 0.0f, 0.0f, -zoom );
				glRotatef ( -tilt, 1.0f, 0.0f, 0.0f );
				glRotatef ( rotate, 0.0f, 0.0f, 1.0f );
				glTranslatef ( float ( i ) - xCamera, float ( d ) - yCamera, 0.0f );
				glColor3f ( 1,1,1 );
				glTranslatef ( 0,0,-0.49f );
				if ( c.getMap().getTile ( i,d ).getWall() )
					glTranslatef ( 0,0,2 );
				glCallList ( list+5 );
			}
		}
	}

	//Draw the people
	for ( int i = 0; i < MAX_BEING; ++i )
	{
		if ( c.getBeing ( i ).getAlive() && c.getBeing ( i ).getVisible() )
		{
			if ( c.getMap().getTile ( c.getBeing ( i ).getPos().getintX(), c.getBeing ( i ).getPos().getintY() ).getTerrain() < GENERATOR )
			{
				if ( c.getSelect() == i )
				{
					draw ( c.getBeing ( i ), true );
				}
				else
				{
					draw ( c.getBeing ( i ), false );
				}
			}
		}
	}

	if ( c.getTurn() == PLAYER1 )
	{
		//Draw the cursor
		glDisable ( GL_DEPTH_TEST );
		glDisable ( GL_LIGHTING );
		glLoadIdentity();
		glTranslatef ( 0.0f, 0.0f, -zoom );
		glRotatef ( -tilt, 1.0f, 0.0f, 0.0f );
		glRotatef ( rotate, 0.0f, 0.0f, 1.0f );
		glTranslatef ( c.getXCursor() - xCamera, c.getYCursor() - yCamera, 0 );
		if ( c.getMap().getTile ( int ( c.getXCursor() + 0.5 ), int ( c.getYCursor() +0.5 ) ).getWall() ||
		        c.getXCursor() < 0 || c.getYCursor() < 0 ||
		        int ( c.getXCursor() + 0.5 ) > c.getMap().getMaxX()-1 || int ( c.getYCursor() + 0.5 ) > c.getMap().getMaxY()-1 )
		{
			glTranslatef ( 0,0,2 );
		}
		glRotatef ( cursorSpin, 0.0f, 0.0f, 1.0f );
		glCallList ( list+4 );
		glEnable ( GL_DEPTH_TEST );

		//Draw the HUD
		glDisable ( GL_LIGHTING );
		glEnable ( GL_BLEND );
		glEnable ( GL_TEXTURE_2D );

		//Building instructions
		if ( c.getEnergy() >= 20 )
		{
			glColor3f ( 1,1,1 );
		}
		else
		{
			glColor3f ( 1,0,0 );
		}
		glPrint ( 5,460,0.8f,false,false,"1-Militia (20%)" );
		if ( c.getEnergy() >= 30 )
		{
			glColor3f ( 1,1,1 );
		}
		else
		{
			glColor3f ( 1,0,0 );
		}
		glPrint ( 5,445,0.8f,false,false,"2-Soldier (30%)" );
		if ( c.getEnergy() >= 40 )
		{
			glColor3f ( 1,1,1 );
		}
		else
		{
			glColor3f ( 1,0,0 );
		}
		glPrint ( 5,430,0.8f,false,false,"3-Slayer  (40%)" );
		glColor3f ( 1,1,1 );
		glPrint ( 5, 415, 0.8f, false, false, "h-Help" );

		//Status
		glColor3f ( 1,1,1 );
		glPrint ( 5,5,1,false,false,"Energy:", c.getEnergy() );
		if ( c.getEnergy() == c.getMaxEnergy() )
		{
			glColor3f ( 0,1,0 );
		}
		else
		{
			glColor3f ( 1,1,1 );
		}
		glPrint ( 5,5,1,false,false,"        %2.1i%%", c.getEnergy() );
		if ( c.getIncome() >= c.getMaxEnergy() )
		{
			glColor3f ( 0,1,0 );
		}
		else
		{
			glColor3f ( 1,1,1 );
		}
		glPrint ( 5,5,1,false,false,"             (+%1.1i%%)", min ( c.getIncome(), int ( c.getMaxEnergy() ) ) );
		glColor3f ( 1,1,1 );
		for ( int i = 0; i < MAX_BEING; ++i )
		{
			if ( c.getBeing ( i ).getAlive() && int ( c.getXCursor() + 0.5f ) == c.getBeing ( i ).getPos().getX() &&
			        int ( c.getYCursor() + 0.5f ) == c.getBeing ( i ).getPos().getY()
			        && c.getBeing ( i ).getVisible() )
			{
				glPrint ( 420,52,1,false,false,c.getBeing ( i ).getName().c_str() );
				switch ( c.getBeing ( i ).getType() )
				{
					case DEFENDER:
						glPrint ( 420,35,0.8f,false,false,"Militia" );
						break;
					case SOLDIER:
						glPrint ( 420,35,0.8f,false,false,"Soldier" );
						break;
					case SLAYER:
						glPrint ( 420,35,0.8f,false,false,"Slayer" );
						break;
					case VIP:
						glPrint ( 420,35,0.8f,false,false,"V.I.P" );
						break;
					case ZOMBIE:
						glPrint ( 420,35,0.8f,false,false,"Zombie" );
						break;
					case SMARTZOMBIE:
						glPrint ( 420,35,0.8f,false,false,"Smart Zombie" );
						break;
					case FASTZOMBIE:
						glPrint ( 420,35,0.8f,false,false,"Fast Zombie" );
						break;
					case BIGZOMBIE:
						glPrint ( 420,35,0.8f,false,false,"Giant Zombie" );
						break;
					case REAPER:
						glPrint ( 420,35,0.8f,false,false,"Reaper" );
						break;
					case STEALTH:
						glPrint ( 420,35,0.8f,false,false,"Stalker" );
						break;
					case ZOMBIELORD:
						glPrint ( 420,35,0.8f,false,false,"Zombie Lord" );
						break;
					case ZOMBIEKING:
						glPrint ( 420,35,0.8f,false,false,"Master Zombie" );
						break;
				}
				glPrint ( 420,20,0.8f,false,false,"Health: %1.1i/%1.1i", c.getBeing ( i ).getHealth(), c.getBeing ( i ).getMaxHealth() );
				glPrint ( 420,5,0.8f,false,false,"Moves: %1.1i/%1.1i", c.getBeing ( i ).getTurns(), c.getBeing ( i ).getMaxTurns() );
				if ( c.getBeing ( i ).getType() == VIP )
				{
					glPrint ( 320,90,0.8f,false,true,"Non-combat V.I.P - Protect at all costs" );
				}
			}
		}
		switch ( c.getObjective() )
		{
			case ROUT:
			case TIMED_ROUT:
				glPrint ( 320,20,0.8f,false,true,"Objective: Rout" );
				break;
			case SEIZE:
			case TIMED_SEIZE:
				glPrint ( 320,20,0.8f,false,true,"Objective: Seize" );
				if ( c.getMap().getTile ( int ( c.getXCursor() + 0.5f ), int ( c.getYCursor() + 0.5f ) ).getTarget() )
				{
					glPrint ( 320,75,0.8f,false,true,"Bring any unit to this location to complete mission" );
				}
				break;
			case ESCORT:
			case TIMED_ESCORT:
				glPrint ( 320,20,0.8f,false,true,"Objective: Escort" );
				if ( c.getMap().getTile ( int ( c.getXCursor() + 0.5f ), int ( c.getYCursor() + 0.5f ) ).getTarget() )
				{
					glPrint ( 320,75,0.8f,false,true,"Bring a V.I.P to this location to complete mission" );
				}
				break;
			case SURVIVAL:
				glPrint ( 320,20,0.8f,false,true,"Objective: Survive" );
				break;
		}

		//DEBUG
		if ( c.getObjective() >= TIMED_ROUT )
		{
			if ( c.getObjective() == SURVIVAL )
			{
				if ( c.getClock() > c.getTime() )
				{
					glColor3f ( 0,0,1 );
				}
				else if ( c.getClock() == c.getTime() )
				{
					glColor3f ( 0,1,0 );
				}
				else if ( c.getClock() == c.getTime() - 1 )
				{
					glColor3f ( 0.5f,1,0.5f );
				}
			}
			else
			{
				if ( c.getClock() > c.getTime() )
				{
					glColor3f ( 1,0,0 );
				}
				else if ( c.getClock() == c.getTime() )
				{
					glColor3f ( 1,0.5f,0 );
				}
				else if ( c.getClock() == c.getTime() - 1 )
				{
					glColor3f ( 1,1,0 );
				}
			}
			glPrint ( 320,5,0.8f,false,true,"Turn %1.1i/%1.1i", c.getClock(), c.getTime() );
			glColor3f ( 1,1,1 );
		}
		else
		{
			glPrint ( 320,5,0.8f,false,true,"Turn %1.1i", c.getClock() );
		}

		//Building prompts
		if ( c.getSelect() == -2 )
		{
			glPrint ( 320,105,0.8f,false,true,"Select location for new Militia" );
		}
		else if ( c.getSelect() == -3 )
		{
			glPrint ( 320,105,0.8f,false,true,"Select location for new Soldier" );
		}
		else if ( c.getSelect() == -4 )
		{
			glPrint ( 320,105,0.8f,false,true,"Select location for new Slayer" );
		}

		//Outcome prompts
		switch ( c.getOutcome() )
		{
			case -3:
				glPrint ( 320,240,1.2f,false,true,"MISSION FAILED - TIME LIMIT EXCEEDED" );
				break;
			case -2:
				glPrint ( 320,240,1.2f,false,true,"MISSION FAILED - FAILED TO PROTECT VIP" );
				break;
			case -1:
				glPrint ( 320,240,1.2f,false,true,"MISSION FAILED - ALLIED FORCES ELIMINATED" );
				break;
			case 1:
				glPrint ( 320,240,1.2f,false,true,"MISSION COMPLETE - ZOMBIES ELIMINATED" );
				break;
			case 2:
				glPrint ( 320,240,1.2f,false,true,"MISSION COMPLETE - DESTINATION REACHED" );
				break;
			case 3:
				glPrint ( 320,240,1.2f,false,true,"MISSION COMPLETE - TIME LIMIT REACHED" );
				break;
		}
		if ( c.getOutcome() != 0 )
		{
			glPrint ( 320, 200, 1, false, true, "PRESS ESC OR ENTER TO CONTINUE" );
		}
		glDisable ( GL_BLEND );
		glDisable ( GL_TEXTURE_2D );
	}

	glEnable ( GL_LIGHTING );

	//Spin the cursor, etc
	cursorSpin += 5;
	xCamera += ( xTarget - xCamera ) / 4;
	yCamera += ( yTarget - yCamera ) / 4;
	tilt += ( targetTilt - tilt ) / 4;
	rotate += ( targetRotate - rotate ) / 4;
}


void Display::drawMenu(Organizer o)
{
	glDisable ( GL_LIGHTING );
	glDisable ( GL_TEXTURE_2D );
	glEnable2D();

	int xPos = 0;
	int yPos = 0;

	for ( int i = 0; i < o.getNumMaps(); ++i ) {
		glLoadIdentity();
		glTranslatef ( 50+24*xPos, 360-24*yPos, 0 );
		if ( o.getSelect() == i )
			glScalef ( 1.2f, 1.2f, 1 );

		if ( o.getCleared ( i ) ) {
			if ( o.getSelect() == i )
				glColor3f ( 0, 1 + 0.25f * sin ( cursorSpin * 0.03f ), 0 );
			else
				glColor3f ( 0, 1, 0 );
		} else {
			if ( o.toUnlock ( i ) <= 0 ) {
				if ( o.getSelect() == i )
					glColor3f ( 0, 0.3f + 0.15f * sin ( cursorSpin * 0.03f ), 0 );
				else
					glColor3f ( 0, 0.3f, 0 );
			} else {
				if ( o.getSelect() == i )
					glColor3f ( 0.3f + 0.15f * sin ( cursorSpin * 0.03f ), 0, 0 );
				else
					glColor3f ( 0.3f, 0, 0 );
			}
		}
		glCallList ( list+8 );
		++xPos;
		if (xPos >= 10) {
			xPos -= 10;
			yPos += 1;
		}
	}

	glLoadIdentity();
	glTranslatef ( o.getXCursor(), o.getYCursor(), 0 );
	glColor3f ( 1,1,1 );
	glCallList ( list+9 );

	glDisable2D();

	glEnable ( GL_BLEND );
	glEnable ( GL_TEXTURE_2D );
	if ( o.getSelect() > -1 && o.getSelect() < o.getNumMaps() ) {
		glPrint ( 300, 350, 0.85f,false,false,"Map #%1.1i", o.getSelect() +1 );
		glPrint ( 300, 328, 1.2f,false,false,o.getMapName ( o.getSelect() ).c_str() );
		if ( o.toUnlock ( o.getSelect() ) <= 0 )
		{
			if ( o.getCleared ( o.getSelect() ) )
			{
				glColor3f ( 0,1,0 );
				glPrint ( 300,300,0.9f,false, false, "Cleared" );
				glPrint ( 300,280,0.9f,false, false, "Best Time: %1.1i Turns", o.getTimeRecord ( o.getSelect() ) );
			} else {
				glColor3f ( 0, 0.4f, 0 );
				glPrint ( 300,300,0.85f,false, false, "Unlocked" );
			}
		} else {
			glColor3f ( 0.6f, 0, 0 );
			glPrint ( 300,300,0.85f,false, false, "Locked" );
			glColor3f ( 1, 1, 1 );
			if ( o.toUnlock ( o.getSelect() ) == 1 )
			{
				glPrint ( 300,280,0.85f,false, false, "You must clear   more map" );
				glPrint ( 300,260,0.85f,false, false, "to unlock this map" );
				glColor3f ( 1,1,0 );
				glPrint ( 300,280,0.85f,false, false, "               1" );
			} else if ( o.toUnlock ( o.getSelect() ) < 10 ) {
				glPrint ( 300,280,0.85f,false, false, "You must clear %1.1i more maps" );
				glPrint ( 300,260,0.85f,false, false, "to unlock this map" );
				glColor3f ( 1,1,0 );
				glPrint ( 300,280,0.85f,false, false, "               ", o.toUnlock ( o.getSelect() ) );
			} else {
				glPrint ( 300,280,0.85f,false, false, "You must clear    more maps" );
				glPrint ( 300,260,0.85f,false, false, "to unlock this map" );
				glColor3f ( 1,1,0 );
				glPrint ( 300,280,0.85f,false, false, "               %1.1i", o.toUnlock ( o.getSelect() ) );
			}
		}
	}
	glColor3f ( 0, 1, 0 );
	glPrint ( 320, 440, 2, false, true, "ZOMBIE CITY TACTICS" );

	cursorSpin += 5;
}


void Display::drawStats ( Controller c )
{
	glEnable ( GL_BLEND );
	glEnable ( GL_TEXTURE_2D );
	glDisable ( GL_LIGHTING );
	if ( c.getOutcome() > 0 )
	{
		glColor3f ( 1,1,1 );
		glPrint ( 320,440,2,false, true, "MISSION ACCOMPLISHED" );
	}
	else
	{
		glColor3f ( 1,0,0 );
		glPrint ( 320,440,2,false, true, "MISSION FAILED" );
	}
	glPrint ( 120, 400, 1, false, false, "Turns Taken..........%1.1i", c.getClock() );
	glPrint ( 120, 380, 1, false, false, "Units Built..........%1.1i", c.getStats ( MILITIABUILT ) + c.getStats ( SOLDIERSBUILT ) + c.getStats ( SLAYERSBUILT ) );
	glPrint ( 120, 360, 1, false, false, "    Militia Built....%1.1i", c.getStats ( MILITIABUILT ) );
	glPrint ( 120, 340, 1, false, false, "    Soldiers Built...%1.1i", c.getStats ( SOLDIERSBUILT ) );
	glPrint ( 120, 320, 1, false, false, "    Slayers Built....%1.1i", c.getStats ( SLAYERSBUILT ) );
	glPrint ( 120, 300, 1, false, false, "Units Lost...........%1.1i", c.getStats ( MILITIADIED ) + c.getStats ( SOLDIERSDIED ) + c.getStats ( SLAYERSDIED ) );
	glPrint ( 120, 280, 1, false, false, "    Militia Lost.....%1.1i", c.getStats ( MILITIADIED ) );
	glPrint ( 120, 260, 1, false, false, "    Soldiers Lost....%1.1i", c.getStats ( SOLDIERSDIED ) );
	glPrint ( 120, 240, 1, false, false, "    Slayers Lost.....%1.1i", c.getStats ( SLAYERSDIED ) );
	glPrint ( 120, 220, 1, false, false, "Zombies Dispatched...%1.1i", c.getStats ( ZOMBIEKILLS ) );
	glPrint ( 120, 200, 1, false, false, "Energy Generated.....%1.1i", c.getStats ( ENERGYGET ) );
	glPrint ( 120, 180, 1, false, false, "Energy Spent.........%1.1i", c.getStats ( ENERGYSPENT ) );
	glPrint ( 120, 160, 1, false, false, "Undos Taken..........%1.1i", c.getStats ( UNDOS ) );
}


/*!
 * Print quick help to the screen, i.e. keyboard settings.
 */
void Display::drawHelp()
{
	glEnable ( GL_BLEND );
	glEnable ( GL_TEXTURE_2D );
	glDisable ( GL_LIGHTING );

	glColor4f( 0.4, 0.4, 0.4, 0.4 );
	glRectf( 100, 450, 540, 200 );

	glColor3f ( 1, 1, 1 );
	glPrint ( 320, 440, 2, false, true, "Help" );

	glPrint ( 120, 400, 1, false, false, "WSAD...........Move Camera" );
	glPrint ( 120, 380, 1, false, false, "Arrow Keys.....Move Camera" );
	glPrint ( 120, 360, 1, false, false, "Control+Mouse..Rotate Camera" );
	glPrint ( 120, 340, 1, false, false, "Mouse Wheel....Zoom Camera In/Out" );
	glPrint ( 120, 320, 1, false, false, "Backspace......Reset Camera Tilt/Zoom" );
	glPrint ( 120, 300, 1, false, false, "Left Click.....Select/Order Unit" );
	glPrint ( 120, 280, 1, false, false, "Shift+L.Click..Chart path" );
	glPrint ( 120, 260, 1, false, false, "Right Click....Deselect Unit" );
	glPrint ( 120, 240, 1, false, false, "G..............Set soldier to Guard mode" );
	glPrint ( 120, 220, 1, false, false, "Tab............Select Next Unused Unit" );
	glPrint ( 120, 200, 1, false, false, "Z..............Undo Previous Move" );
	glPrint ( 120, 180, 1, false, false, "123............Build New Units" );
	glPrint ( 120, 160, 1, false, false, "Enter..........End Turn" );
}
