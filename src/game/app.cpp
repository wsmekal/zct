
/**************************************
*                                     *
*   Jeff Molofee's Basecode Example   *
*   SDL porting by Fabio Franchello   *
*          nehe.gamedev.net           *
*                2001                 *
*                                     *
**************************************/

// Includes
#ifdef WIN32													// If We're Under MSVC
	#include <windows.h>											// Include The Windows Header
#else															// Otherwise
	#include <cstdio>												// Include The Standar IO Header
	#include <cstdlib>												// And The Standard Lib (for exit())
#endif															// Then...

#include <cmath>												// We Require The Math Lib For Sin and Cos
#include <assert.h>
#include <fstream>		// File read/write
#include <string>
using namespace std;

#include "SDL_opengl.h"
#include "SDL.h"												// Finally: The SDL Header!
#include "SDL_mixer.h"

#include "main.h"												// We're Including theHeader Where Defs And Prototypes Are
#include "controller.h"

extern S_AppStatus AppStatus;									// We're Using This Struct As A Repository For The Application State (Visible, Focus, ecc)

// User Defined Variables
bool keyPressed[256];
bool clicked;
float oldMousex;
float oldMousey;

//Game-related variables
Controller control;
Controller undo;
Display display;
Map map;
Organizer org;
int scene = S_MENU;

/* Mix_Music actually holds the music information.  */
Mix_Music *music = NULL;
Mix_Music* music_menu = NULL;
Mix_Music* music_stats = NULL;
Mix_Music* zombie_groan[3] = { NULL, NULL, NULL };


bool InitGL(SDL_Surface *S)										// Any OpenGL Initialization Code Goes Here
{
	return display.InitGL(S);
}

bool Initialize(void)											// Any Application & User Initialization Code Goes Here
{
	AppStatus.Visible = true;								// At The Beginning, Our App Is Visible
	AppStatus.MouseFocus = true;								// And Have Both Mouse
	AppStatus.KeyboardFocus = true;								// And Input Focus
	SDL_ShowCursor( false );							// cursor is not shown

	// Start Of User Initialization
	control = Controller( &display );
	control.generate(formatString("%s/data/maps/map1.dat", dataPath.c_str()));
	undo.duplicate( &control );
	control.setUndo( &undo );
	org = Organizer();
	org.load();

	return true;												// Return TRUE (Initialization Successful)
}


void Deinitialize(void)											// Any User Deinitialization Goes Here
{
	return;														// We Have Nothing To Deinit Now
}

char getLetter( Uint8 *keys )
{
	for(int i = int('a'); i <= int('z'); ++i) {
		if(keys) {
			if(keys[i]) {
				if(!keyPressed[i]) {
					keyPressed[i] = true;
					return char(i);
				}
			} else {
				keyPressed[i] = false;
			}
		}
	}
	return ' ';
}


void Update(Uint32 Milliseconds, Uint8 *Keys, int mousex, int mousey, bool lclick, bool rclick, bool wheelup, bool wheeldown)	// Perform Motion Updates Here
{
	switch(scene) {
	case S_PLAY:
		if(Keys) {													// If We're Sent A Key Event With The Update
			if(Keys[SDLK_UP] || Keys['w']) {
				display.moveCamera( CAMERA_SPEED, CAMERA_SPEED);
			}
			if(Keys[SDLK_DOWN] || Keys['s']) {
				display.moveCamera(-CAMERA_SPEED,-CAMERA_SPEED);
			}
			if(Keys[SDLK_LEFT] || Keys['a']) {
				display.moveCamera(-CAMERA_SPEED, CAMERA_SPEED);
			}
			if(Keys[SDLK_RIGHT] || Keys['d']) {
				display.moveCamera( CAMERA_SPEED,-CAMERA_SPEED);
			}
			if(Keys['n']) {
				display.zoomInOut(-ZOOM_SPEED);
			}
			if(Keys['m']) {
				display.zoomInOut(ZOOM_SPEED);
			}
			if(Keys[SDLK_ESCAPE]) {
				if(!keyPressed[SDLK_ESCAPE]) {
					keyPressed[SDLK_ESCAPE] = true;
					if(control.getOutcome() > 0) {
						//We won! Clear the map
						if(org.getTimeRecord(org.getSelect()) == 0 || org.getTimeRecord(org.getSelect()) > control.getClock()) {
							org.setTimeRecord(org.getSelect(), control.getClock());
						}
						org.save();
					}
					if(control.getClock() == 1 && control.getOutcome() == 0) {
						scene = S_MENU;
					} else {
						scene = S_STATS;
					}
				}
			} else {
				keyPressed[SDLK_ESCAPE] = false;
			}
			if(Keys[SDLK_RETURN]) {
				if(!keyPressed[SDLK_RETURN]) {
					keyPressed[SDLK_RETURN] = true;
					if(control.getOutcome() != 0) {
						if(control.getOutcome() > 0) {
							//We won! Clear the map
							if(org.getTimeRecord(org.getSelect()) == 0 || org.getTimeRecord(org.getSelect()) > control.getClock()) {
								org.setTimeRecord(org.getSelect(), control.getClock());
							}
							org.save();
						}
						scene = S_STATS;
					} else {
						control.endPlayerTurn();
					}
				}
			} else {
				keyPressed[SDLK_RETURN] = false;
			}
			if(Keys[SDLK_TAB] && !Keys[SDLK_LALT]) {
				if(!keyPressed[SDLK_TAB]) {
					keyPressed[SDLK_TAB] = true;
					control.selectNextUnit();
				}
			} else {
				keyPressed[SDLK_TAB] = false;
			}
			if(Keys['z']) {
				if(!keyPressed[size_t('z')]) {
					keyPressed[size_t('z')] = true;
					control.revert();
				}
			} else {
				keyPressed[size_t('z')] = false;
			}
		if( Keys['h'] ) {
			if( !keyPressed[int('h')] ) {
				keyPressed[int('h')] = true;
				display.toggleDisplayHelp();
			}
		} else {
			keyPressed[int('h')] = false;
		}
			if(Keys['1']) {
				control.requestNewUnit(1);
			}
			if(Keys['2']) {
				control.requestNewUnit(2);
			}
			if(Keys['3']) {
				control.requestNewUnit(3);
			}
		}

		if(Keys && (Keys[SDLK_LCTRL] || Keys[SDLK_RCTRL])) {
			display.pivotCamera(mousex - oldMousex);
			display.tiltCamera(mousey - oldMousey);
		}
		if(lclick)
			control.issueOrder();
		if(rclick)
			control.cancelOrder();
		if(wheelup)
			display.zoomInOut(-ZOOM_SPEED);
		if(wheeldown)
			display.zoomInOut(ZOOM_SPEED);

		control.moveCursor(mousex, mousey);
		control.process();
		break;
	case S_MENU:
    if(music == NULL) {
      music = Mix_LoadMUS(formatString("%s/data/audio/1178_nicStage_guitar_string_ominous.ogg", dataPath.c_str()));
      Mix_PlayMusic(music, -1);
    }
		if(Keys) {													// If We're Sent A Key Event With The Update
			if(Keys[SDLK_ESCAPE]) {
				if(!keyPressed[SDLK_ESCAPE]) {
					keyPressed[SDLK_ESCAPE] = true;
          if( music ) {
            Mix_HaltMusic();
            Mix_FreeMusic( music );
            music=NULL;
          }
					TerminateApplication();
				}
			} else {
				keyPressed[SDLK_ESCAPE] = false;
			}
		}
		if(lclick) {
			if(org.getSelect() > -1 && (org.toUnlock(org.getSelect()) <= 0 || (Keys && Keys['j']))) {
				control.generate(formatString("%s/data/maps/map%1.1i.dat", dataPath.c_str(), org.getSelect()+1));
				undo.duplicate(&control);
				control.setUndo(&undo);
        if( music ) {
          Mix_HaltMusic();
          Mix_FreeMusic( music );
          music=NULL;
        }
				scene = S_PLAY;
			}
		}
		org.moveCursor(mousex, mousey);
		break;
	case S_STATS:
    if(music == NULL) {
      music = Mix_LoadMUS(formatString("%s/data/audio/9360_lancelottjones_industrial.ogg", dataPath.c_str()));
      Mix_PlayMusic(music, -1);
    }
		if(Keys[SDLK_ESCAPE]) {
			if(!keyPressed[SDLK_ESCAPE]) {
				keyPressed[SDLK_ESCAPE] = true;
        if( music ) {
          Mix_HaltMusic();
          Mix_FreeMusic( music );
          music=NULL;
        }
				scene = S_MENU;
			}
		} else {
			keyPressed[SDLK_ESCAPE] = false;
		}
		if(Keys[SDLK_RETURN]) {
			if(!keyPressed[SDLK_RETURN]) {
				keyPressed[SDLK_RETURN] = true;
        if( music ) {
          Mix_HaltMusic();
          Mix_FreeMusic( music );
          music=NULL;
        }
				scene = S_MENU;
			}
		} else {
			keyPressed[SDLK_RETURN] = false;
		}
		if(lclick) {
      if( music ) {
        Mix_HaltMusic();
        Mix_FreeMusic( music );
        music=NULL;
      }
			scene = S_MENU;
    }
		break;
	}

	oldMousex = mousex;
	oldMousey = mousey;
	return;
}

/*!
 * Draw either the game graphics (and help), menu or statistics screen.
 * 
 * @param Screen (SDL_surface*)
 */
void Draw(SDL_Surface *Screen)									// Our Drawing Code
{
	assert( Screen );												// We won't go on if the Screen pointer is invalid

	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );		// Clear Screen And Depth Buffer

	switch( scene ) 
	{
	case S_PLAY:
		display.draw( control );
		if( display.getDisplayHelp() ) {
			Log( "Help is displayed\n" );
			display.drawHelp();
		}
		break;
	case S_MENU:
		display.drawMenu(org);
		break;
	case S_STATS:
		display.drawStats(control);
		break;
	}

	glFlush();													// Flush The GL Rendering Pipelines

	return;
}
