ZOMBIE CITY TACTICS
Created by Alan Gordon, August 2006
v1.11

OVERVIEW
Zombie City Tactics is a turn-based strategy game that takes place in the eternally unfortunate Zombie City. The objective of the game is generally to expand your territory and capture zombie generators, until there are no zombies left on the map.

CONTROLS
WSAD...........Move Camera
Arrow Keys.....Move Camera
Control+Mouse..Rotate Camera
Mouse Wheel....Zoom Camera In/Out
Backspace......Reset Camera Tilt/Zoom
Left Click.....Select/Order Unit
Shift+L.Click..Chart path
Right Click....Deselect Unit
G..............Set soldier to Guard mode
Tab............Select Next Unused Unit
Z..............Undo Previous Move
123............Build New Units
Enter..........End Turn

BASICS
When a game of Zombie City Tactics begins, it will be the player's turn. A white arrow will appear over every player-owned unit that hasn't moved yet this turn. The player then moves the units at his disgression, claiming territory as he moves. 
The enemies are zombies, which are usually green. When the player ends his turn, all zombies will move towards the human forces, or, if they are adjacent to a human unit, they will eat it. Fortunately, the humans can retaliate - to attack a zombie with a unit, simply try to move onto the same square as the zombie. Usually, this will destroy it, but some special zombie types have to be attacked multiple times before being destroyed.
If you select a unit and hold shift, you can click on a series of adjacent squares to chart a long-term path for that unit. Until it is given another order, it will follow the path on its own.

GENERATORS
Scattered across most maps are a number of tall towers, called Generators. Every turn, a new zombie will come out of each Generator, making them very dangerous objects. The player can and should disable these Generators by capturing them - simply try to move a unit onto the Generator, similarly to attacking a zombie. Be careful, though - if a zombie is adjacent to a Generator, it can reactivate it. Generators come in various types, each creating a different type of zombie. Some of the more powerful zombie types will only be generated every second turn.

ENERGY
In most situations, the human forces will be greatly outnumbered by the zombie forces. Fortunately, the humans have the ability to create new units, using energy. Depending on the amount of territory and Generators controlled by the player, a certain amount of energy will be added to their reserve each turn (1% for every 2 tiles of territory and 5% for every generator). By spending this energy, new units can be placed anywhere within player territory. Building a Militia costs 20% of your energy, a Soldier costs 30%, and a Slayer costs 40%. The energy is maxed out at 100%, although this can be exceeded by capturing Generators.

HUMAN UNITS
Militia
Appearance: Blue
Cost: 20%
The militia is the most basic human unit. It moves one space per turn, and has no special attributes.

Soldier
Appearance: Grayish Blue
Cost: 30%
The soldier is slightly more powerful than the militia, and is very well-suited to defense. When this unit destroys a zombie, it does not move onto the square the zombie occupied. If a soldier is set to Guard mode, then they will automatically attack the strongest adjacent zombie each turn.

Slayer
Appearance: Black
Cost: 40%
These skilled warriors are the elite forces of the human army. They possess great speed, moving two spaces in one turn.

V.I.P
Appearance: Red
Cost: Cannot be built
V.I.Ps only appear on certain maps. They are civilians, and cannot destroy zombies. The player's task is to protect them - if a V.I.P is eaten by a zombie, the map immediately ends in defeat.

ZOMBIES
Zombie
Appearance: Green
This is the most common type of zombie. They move blindly towards the nearest human, at a rate of one space per turn. They possess no special attributes.

Smart Zombie
Appearance: Light Green
This zombie has retained enough of its brain to possess basic problem-solving abilities. Unalike the common zombie, it can easily navigate complex terrain to find its prey.

Fast Zombie
Appearance: Small, Light Green
These zombies hunt their prey with incredible speed, moving two spaces in one turn.

Big Zombie
Appearance: Large, Dark Green
These massive zombies possess great strength, giving them the ability to knock down walls. In addition, they must be attacked three times before they are destroyed.

Reaper
Appearance: Purple
Reapers are zombies with unnatural speed and strength. They move two spaces in one turn, must be attacked twice before they are destroyed, and can climb over walls with ease.

Stalker
Appearance: Dark Purple
Stalkers are zombies that have developed adaptive camoflauge abilities. They cannot be seen until they come within 2 spaces of a human unit.

Zombie Lord
Appearance: Large, Yellow
This is undoubtedly the most powerful type of zombie, although little is known of its abilities. There are rumors that it possesses the power to rebuild itself even when destroyed. How to deal with it is for you to figure out.

MAP TYPES
Rout: The most basic map type. The map ends when all zombies have been destroyed and all Generators have been captured. Satisfying this condition will yield victory on any other map type as well.
Seize: On this map type, there will be one or more tiles that are flashing. Bring any human unit to one of these tiles to end the map in victory.
Escort: This type is very similar to Seize, but with one modification: Only a V.I.P can be used to capture the marked destination.
Survive: After a certain number of turns, this map will end in victory for the player as long as at least one human unit is still standing.
Rout, Seize, and Escor maps can be timed. If the time limit is exceeded on such maps, the game ends in defeat.

TIPS
-The zombies behave in very predictable ways - with some careful observation, you should be able to learn what their priorities are, and what they do to achieve their goals.
-In larger maps, it is frequently a good idea to build a militia or two to move through areas behind the battlefront and capture territory to increase your energy income.
-For most types of zombie Generators, one well-placed soldier can hold off the influx of zombies while you deal with other threats.
-Fighting zombies in a tight corridor is frequently slow and dangerous. If possible, bring the battle out into open air.
-When fighting Big Zombies or Reapers, it is usually better to kill a small number of zombies than to injure a lot of them.

FUTURE IMPROVEMENTS
-Packaged level editor
-More post-map statistics

VERSION HISTORY
v0.8b
-Initial Public Release.

v1.0
-Doubled the number of maps.
-Increased the maximum draw distance and decreased the maximum camera angle, preventing a bug where distant terrain wouldn't draw properly.
-Added basic mouseover information on beings.
-Changed the indicator for unmoved units from black to white, making it easier to see on Slayers.
-Slight graphical tweak to the menu.
-Various possible text popups can't overlap anymore.
-Fixed the AI lag on large maps.
-Adjusted Zombie Lord AI to fix a possible abuse.
-Added post-map statistics.

v1.1
-Improved the camera controls.
-Added the kill counter to the being info overlay.
-Added the ability to chart paths.
-Added Guard mode.
-Toned down the difficulty on the map 'Over the Walls'.
-Renamed the map '99 Zombies' to 'Overrun', on account of there are actually 180 zombies, not 99.
-Fixed multiple problems with the map 'Chess'.

v1.11
-Added path charting.
-Added guard mode for soldiers.

THANKS TO...
Preston Whited
Eric Gordon
MrBusiness
GrimSweeper
samsamsamb
insertcredit.com

ACKNOWLEDGEMENTS
Zombie City Tactics owes a great amount of inspiration to Lost Souls, an excellent mac-only strategy game by Richard White of Spiderweb Games. You can find it at: http://www.spiderwebsoftware.com/lostsouls/index.html
The Zombie City series was, at its roots, based on the Zombie Infection simulator, created by Kevan Davis. You can find it at: http://kevan.org/proce55ing/zombies/

Questions? Comments? Suggestions? Bug Reports? Contact the creator at kobuscrispi@gmail.com.